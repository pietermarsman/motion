# coding=utf-8
"""
Detection of rigid moving objects based on the trajectories of points
"""
import math

import numpy as np
from scipy import optimize
import sklearn
from sklearn.mixture import GMM

__author__ = 'Pieter Marsman'


def energy_sort(matrix):
    """
    Greedy sorting of a matrix based on Costeira1998

    :param matrix: The matrix that is sorted
    :type matrix: ndarray
    :return: The sorted matrix and the indexes of the rows/columns in the original matrix
    :rtype: (ndarray, ndarray)
    """
    assert matrix.shape[0] == matrix.shape[1]
    matrix = np.copy(matrix)
    sorted_idx = np.arange(0, matrix.shape[0])
    for k in range(0, matrix.shape[0]):
        energy = np.sum(matrix[:k + 1, k:] ** 2, axis=0)
        ind = np.argmax(energy)
        matrix = swap_row(matrix, k, k + ind)
        matrix = swap_column(matrix, k, k + ind)
        temp = sorted_idx[k]
        sorted_idx[k] = sorted_idx[k + ind]
        sorted_idx[k + ind] = temp
    return matrix, sorted_idx


def swap_row(matrix, ind0, ind1):
    """
    Swap two rows.

    :param matrix: The matrix in which the swapping should be done.
    :type matrix: ndarray
    :param ind0: The index of the first row
    :type ind0: int
    :param ind1: The index of the second row
    :type ind1: int
    :return: The new matrix with the swapped rows
    :rtype: ndarray
    """
    if ind0 is ind1:
        return matrix
    temp = np.copy(matrix[ind0, :])
    matrix[ind0, :] = matrix[ind1, :]
    matrix[ind1, :] = temp
    return matrix


def swap_column(matrix, ind0, ind1):
    """
    Swap two columns.

    :param matrix: The matrix in which the swapping should be done.
    :type matrix: ndarray
    :param ind0: The index of the first columns
    :type ind0: int
    :param ind1: The index of the second columns
    :type ind1: int
    :return: The new matrix with the swapped columns
    :rtype: ndarray
    """
    if ind0 is ind1:
        return matrix
    temp = np.copy(matrix[:, ind0])
    matrix[:, ind0] = matrix[:, ind1]
    matrix[:, ind1] = temp
    return matrix


class SSC(object):
    """
    Sparse subspace clustering (SSC) of a large matrix recovers a clustering of points using a sparse representation.

    :param p_lambda: The weighting of the regularization of the minimization
    :type p_lambda: float
    :param method: The mineralization method to use. Can be either 'lasso', 'L-BFGS-B', 'BFGS' or 'derivative'. The
        'L-BFGS-B' method works the best.
    :type method: str
    :param minimize_max_iter: The number of iterations to use for minimization
    :type minimize_max_iter: int
    :param minimize_tolerance: The tolerance level at which to stop the minimization.
    :type minimize_tolerance: float
    :param gmm_restart: The number of restarts for the GMM.
    :type gmm_restart: int
    :param gmm_iter: The number of iterations to perform during each GMM.
    :type gmm_iter: int
    """

    def __init__(self, p_lambda, method, minimize_max_iter, minimize_tolerance, gmm_restart, gmm_iter):
        self.p_lambda = p_lambda
        self.big_y = None
        self.mask = None
        self.Y_hat = None
        self.y = None
        self.result = None
        self.method = method
        self.min_toll = minimize_tolerance
        self.gmm_restart = gmm_restart
        self.gmm_iter = gmm_iter
        self.options = {'maxiter': minimize_max_iter}
        if self.method in ['lasso']:
            self.lasso = sklearn.linear_model.Lasso(alpha=p_lambda, fit_intercept=False)

    def set_current_data_point(self, i):
        """
        Sets the current data point for minimization

        :param i: The index in the data matrix
        :return: The list with indexes in the data matrix that doesn't contain the current data point
        :rtype: ndarray
        """
        indices = range(0, self.big_y.shape[1])
        indices.remove(i)
        rows = np.where(self.mask[:, i] == 1)[0]
        self.Y_hat = self.big_y[rows, :]
        self.Y_hat = self.big_y[:, indices]
        self.y = self.big_y[self.mask[:, i] == 1, i]
        return indices

    def set_data(self, big_y, mask):
        """
        Set a new matrix as the data. A mask indicates which values are available and missing.

        :param big_y: The new data matrix. Each row is a data point and each column is a feature.
        :type big_y: ndarray
        :param mask: A mask for the data matrix. Indicates available values (1) and missing (0).
        :type mask: ndarray
        """
        self.big_y = np.copy(big_y)
        self.big_y[mask != 1] = 0.0
        self.mask = mask

    def objective(self, c):
        """
        The objective to maximize. Depends on the current data matrix and selected point.

        :param c: The coefficients
        :type c: ndarray
        :return: The objective for LASSO minimization
        :rtype: float
        """
        l1 = np.linalg.norm(c, 1)
        l2 = math.sqrt(np.sum((self.y - self.Y_hat.dot(c)) ** 2))
        return l1 + self.p_lambda * l2

    def objective_deriv(self, c):
        """
        The derivative of the LASSO minimization objective.

        :param c: The coefficients
        :type c: ndarray
        :return: The derivative of the objective for LASSO minimization at point c.
        :rtype: ndarray
        """
        # noinspection PyUnresolvedReferences
        l1_deriv = c / np.abs(c)
        l1_deriv[c == 0.0] = 0.0
        diff = self.y - self.Y_hat.dot(c)
        # noinspection PyUnresolvedReferences
        l2_deriv = -self.p_lambda * (diff / np.linalg.norm(diff, 2)).dot(self.Y_hat)
        return l1_deriv + l2_deriv

    def find_coefficient(self, c0=None):
        """
        Finds the coefficients that minimize the loss ||c|| with regulizer ||y - cY|| where c_ii = 0

        :param c0: The initial coefficients to use
        :type c0: ndarray
        :return: The optimal values for the coefficients c
        :rtype: ndarray
        """
        if c0 is None:
            c0 = np.random.random(self.big_y.shape[1] - 1) / self.big_y.shape[1]
        if self.method in ['L-BFGS-B', 'BFGS']:
            self.result = optimize.minimize(self.objective, c0, method=self.method, jac=self.objective_deriv,
                                            options=self.options, tol=self.min_toll)
            return self.result.x
        elif self.method in ['lasso']:
            self.lasso.fit(self.Y_hat, self.y)
            res = self.lasso.coef_
            if np.sum(res) == 0.0:
                res = np.ones(res.shape) / self.big_y.shape[1]
            return res
        elif self.method in ['derivative']:
            deriv = self.objective_deriv(c0)
            return deriv
        else:
            return None

    def find_all_coefficients(self, data, mask=None, coefficients0=None):
        """
        Finds all the coefficients that minimize the loss ||C|| with regulizer ||Y - CY|| where C_ii = 0

        :param data: The data matrix. Rows are data points and columns are features.
        :type data: ndarray
        :param mask: The mask for the data matrix. Indicates which values are available (1) and missing(0)
        :type mask: ndarray
        :param coefficients0: The initial coefficients.
        :type coefficients0: ndarray
        :return: The minimized coefficients and their objective value.
        :rtype: (ndarray, ndarray)
        """
        if mask is None:
            mask = np.ones(data.shape)
        self.set_data(data, mask)
        coefficients = np.zeros((self.big_y.shape[1], self.big_y.shape[1]))
        performance = np.zeros(self.big_y.shape[1])
        for i in range(0, self.big_y.shape[1]):
            indices = self.set_current_data_point(i)
            if coefficients0 is None:
                coef = self.find_coefficient()
            else:
                coef = self.find_coefficient(coefficients0[i, indices])
            performance[i] = self.objective(coef)
            coefficients[i, indices] = coef
        return coefficients, performance

    def compute_laplacian_decomposition(self, coefficients, dimensions, variant='normalized_sym'):
        """
        The graph laplacian decomposition of a affinity matrix can be done in several ways.

        :param coefficients: The affinity  matrix
        :type coefficients: ndarray
        :param dimensions: The number of dimensions to use from the eigenvector decomposition
        :type dimensions: list of int
        :param variant: The variant of laplacian graph to use. Can be either 'un_normalized', 'normalized_sym' or
            'normalized_rw'.
        :type variant: str
        :return: The graph laplacian and the eigenvector decomposition of the graph laplacian
        :rtype: (ndarray, ndarray)
        :raise AssertionError: When the variant is not supported
        """
        # noinspection PyUnresolvedReferences
        adjacency = np.abs(coefficients) + np.abs(coefficients.T)
        big_d_diag = np.sum(adjacency, 0)
        if variant == 'un_normalized':
            laplacian = np.diag(big_d_diag) - adjacency
        elif variant == 'normalized_sym':
            big_d_inv_sqrt = np.diag(1.0 / np.sqrt(big_d_diag))
            eye = np.eye(coefficients.shape[0], coefficients.shape[0])
            laplacian = eye - big_d_inv_sqrt.dot(adjacency).dot(big_d_inv_sqrt)
        elif variant == 'normalized_rw':
            big_d_inv = np.diag(1.0 / big_d_diag)
            eye = np.eye(coefficients.shape[0], coefficients.shape[0])
            laplacian = eye - big_d_inv.dot(adjacency)
        else:
            text = "Type should be one of 'un_normalized', 'normalized_sym', 'normalized_rw'. Instead '" + variant + \
                   "' is given."
            raise AssertionError(text)

        return laplacian, self._decompose_laplacian(laplacian, dimensions)

    @staticmethod
    def _decompose_laplacian(laplacian, dimensions):
        values, vectors = np.linalg.eigh(laplacian)
        # noinspection PyUnresolvedReferences
        vectors = vectors / np.abs(vectors).sum(axis=1)[:, np.newaxis]
        return vectors[:, :dimensions]

    def apply_gmm(self, laplacian_decomposition, ks):
        """
        Fits a Gaussian Mixture Model on the laplacian decomposition.

        :param laplacian_decomposition: The decomposition to fit the GMM to
         :type laplacian_decomposition: ndarray
        :param ks: The number of clusters (k) to try. The Bayesian information criterion (bic) is used to test if an
            increase in k is useful. When the bic increases the best clustering is returned.
        :return: The labels, probability, best k and best bic
        :rtype: (ndarray, ndarray, int, float)
        """
        laplacian_decomposition /= np.mean(laplacian_decomposition)
        best_bic = float('inf')
        best_labels = None
        best_k = None
        best_prob = None
        for k in ks:
            g = GMM(n_components=k, covariance_type='diag', n_iter=self.gmm_iter, n_init=self.gmm_restart, params='mc',
                    init_params='mc')
            g = g.fit(laplacian_decomposition)
            bic = g.bic(laplacian_decomposition)
            if bic < best_bic:
                best_bic = bic
                best_labels = g.predict(laplacian_decomposition)
                best_k = k
                best_prob = g.predict_proba(laplacian_decomposition)
            else:
                break
        return best_labels, best_prob, best_k, best_bic