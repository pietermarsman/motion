# coding=utf-8
"""
Reading of the KITTI ground truth data
"""
import os
import re

import scipy.io
from sklearn.metrics import roc_curve
from matplotlib import pyplot as plt

from miscellaneous import ind2vec
from projections import *


__author__ = 'Pieter Marsman'


class Kitti(object):
    """
    Reading of the KITTI ground truth data from a particular source
    :param source: The source of the images. Should be the actual folder where the images are located. The /poses/
    folder is supposed to be on the relative location ..\poses\
    :type source: str
    """
    FILE_PATH = '/poses/'
    TIME_STEP = 0.09

    def __init__(self, source, start_frame=0):
        if type(source) is str:
            self._file_parser = re.compile('([0-9.e-]+) ')
            source = source.split('/')
            folder = '/'.join(source[:-4])
            self.source = folder + Kitti.FILE_PATH + source[-3] + '.txt'
            self.data = None
            self.size = None
            self.states = None
            self.available = True
            self.step = 0
            self.start_frame = start_frame
            self.first_state = None
            self.reset()
        else:
            self.available = False

    def __str__(self):
        ret = '=== ' + self.__class__.__name__ + ' ==='
        if self.available:
            ret += '\nSource: ' + self.source
        else:
            ret += '\nNo parameters'
        return ret

    def get_world_rotation(self):
        """
        The rotation of the vehicle in the current state compared to the first frame
        :return: array with the rotation
        :rtype: ndarray
        """
        return self.states[-1][:, :-1]

    def get_location(self):
        """
        The location of the vehicle in the current state compared to the first frame
        :return: array with the location
        :rtype: ndarray
        """
        return self.states[-1][:, -1]

    def do_step(self):
        """
        Proceed a single step by reading the next pose data
        """
        if self.available:
            if self.first_state is None:
                self.first_state = np.copy(self.data[self.start_frame, :].reshape((3, 4)))
            new_state = np.copy(self.data[(self.step + self.start_frame) % self.size, :].reshape((3, 4)))
            new_state = combine(inverse(self.first_state), new_state)
            if (self.step + self.start_frame + 1) % self.size == 0:
                self.first_state = inverse(np.copy(new_state))
            new_state[:, 3] *= 100.0
            new_state[1, -1] = - new_state[1, -1]
            self.states.append(new_state)
            self.step = (self.step + 1) % self.size

    def retrieve_data(self):
        """
        Read the information from a file and process it into an understandable format.
        """
        try:
            if self.available:
                self.data = np.array(self._parse_file(self.source))
                self.size = self.data.shape[0]
        except AttributeError:
            self.available = False

    @staticmethod
    def _parse_file(file_path):
        """
        Parse the content of a oxtx file from the kitti database
        :param file_path: Path to the file
        :type file_path: str
        :return: Numbers in the file
        :rtype: list of int
        """
        f = open(file_path, 'r')
        lines = []
        for line in f:
            lines.append([float(x) for x in line.split()])
        return lines

    def reset(self):
        """
        Resets the state of the vehicle to the first state.
        """
        self.step = 0
        self.states = [np.eye(3, 4)]


class Hopkins(object):
    """
    Reading of the Hopkins dataset

    :param folder: The folder were the dataset is located. Should be the parent folder of the folders were the .mat
        files are located.
    :type folder: str
    """

    def __init__(self, folder):
        self.folder = folder
        self.sequences = []

    def read_sequences(self):
        """
        Read all the sequences in the child folders. Goes to every folder and searches for the .mat files.
        """
        for sequence in Hopkins.list_sequences(self.folder):
            self.sequences.append(Hopkins.get_data(sequence))

    @staticmethod
    def list_sequences(folder):
        """
        List all the folders that have a .mat file in them. Check's if the folder name contains a 'g', these folders
            contain .mat files.

        :param folder: The parent folder of the folders containing the .mat files
        :type folder: str
        :return: List of folders that contain .mat files
        :rtype: [str]
        """
        child_folders = os.listdir(folder)
        sequences = []
        for f in child_folders:
            directory = os.path.join(folder, f)
            if os.path.isdir(directory):
                ind = f.find('g')
                if ind == -1:
                    sequences.append(directory)
        return sequences

    @staticmethod
    def get_data(folder):
        """
        Get the data from the .mat file in a specific folder

        :param folder: The folder containing the .mat file
        :type folder: str
        :return: Array with the data from the .mat file
        :rtype: ndarray
        """
        for file_name in os.listdir(folder):
            if file_name[-3:] == 'mat':
                mat_file = os.path.join(folder, file_name)
                return scipy.io.loadmat(mat_file)


class Trajectories(object):
    """
    Generates a number of trajectories with specific length, dimension, noise and sparseness on different rigid bodies.

    :param length: The length of the tracks. For each track a random starting location and random starting
    velocity
        is generated. The rest of the track is generated with random accelerations.
    :type length: int
    :param number: The number of tracks to generate.
    :type number: int
    :param subspaces: The number of subspaces (different rigid bodies) were the tracks lie on.
    :type subspaces: int
    :param dimensions: The number of dimensions of each point
    :type dimensions: int
    :param noise: The standard deviation of the noise added to each point
    :type noise: float
    :param sparseness: The ratio of points that are missing
    :type sparseness: float
    :param rigid_body: If rigid bodies should be used.
    :type rigid_body: bool
    """

    def __init__(self, length, number, subspaces, dimensions, noise, sparseness, rigid_body=True):
        self.length = length
        self.number = number
        self.subspaces = subspaces
        self.dimensions = dimensions
        self.noise = noise
        self.sparseness = sparseness
        if rigid_body:
            ret = Trajectories.create_rigid_body_trajectories(length, number, dimensions, subspaces, noise, sparseness)
            self.trajectories, self.ground_truth, self.mask = ret
        else:
            ret = Trajectories.create_trajectories(length, number, dimensions, subspaces, noise, sparseness)
            self.trajectories, self.ground_truth, self.mask = ret

    @staticmethod
    def create_trajectories(length, number, dimensions, subspaces, noise, sparseness, max_acc=0.1, max_velocity=0.1):
        """
        Generates a number of trajectories with specific length, dimension, noise and sparseness on different rigid
            bodies.

        :param length: The length of the tracks
        :type length: int
        :param number: The number of tracks
        :type number: int
        :param dimensions: The dimension of each point generated for the track.
        :type dimensions: int
        :param subspaces: The number of subspaces
        :type subspaces: int
        :param noise: The standard deviation of the noise added to each point
        :type noise: float
        :param sparseness: The ratio of points that are missing
        :type sparseness: float
        :param max_velocity: The maximum velocity
        :type max_velocity: float
        :param max_acc: The maximum acceleration
        :type max_acc: float
        :return: The trajectories, there clustering and the mask for missing values
        :rtype: (ndarray, ndarray, ndarray)
        """
        ret = Trajectories.initialize_trajectories(length, number, dimensions, subspaces, noise, max_velocity, max_acc)
        velocities, start, start_offset, ground_truth, noise, trajectories, subspace_sizes = ret
        for i in range(0, subspaces):
            id_from, id_to = subspace_sizes[i], subspace_sizes[i + 1]
            trajectories[:dimensions, id_from:id_to] = start[i, :, :] + start_offset[id_from:id_to, :, :].T
            ground_truth[id_from:id_to] = i
            for j in range(dimensions, length * dimensions, dimensions):
                vel = velocities[j / dimensions, :, i].reshape((-1, dimensions, 1))
                trajectories[j:j + dimensions, id_from:id_to] = trajectories[j - dimensions:j, id_from:id_to] + vel
        trajectories += noise
        mask = np.random.random((length, number)) > sparseness
        mask = np.repeat(mask[:, :], dimensions, axis=0)
        trajectories[not mask] = 0.0
        return trajectories, ground_truth, mask

    @staticmethod
    def create_rigid_body_trajectories(length, number, dim, subspaces, noise, sparseness, max_d_vel=0.01,
                                       max_velocity=0.1, max_rotation=0.5):
        """
        Generates a number of trajectories with specific length, dimension, noise and sparseness on different rigid
            bodies.

        :param length: The length of the tracks
        :type length: int
        :param number: The number of tracks
        :type number: int
        :param dim: The number of dimensions of each point
        :type dim: int
        :param subspaces: The number of subspaces
        :type subspaces: int
        :param noise: The standard deviation of the noise added to each point
        :type noise: float
        :param sparseness: The ratio of points that are missing
        :type sparseness: float
        :param max_d_vel: The maximum acceleration
        :type max_d_vel: float
        :param max_velocity: The maximum velocity
        :type max_velocity: float
        :param max_rotation: The maximum rotation in euler angles of the rigid body
        :type max_rotation: float
        :return: The trajectories, there clustering and the mask for missing values
        :rtype: (ndarray, ndarray, ndarray)
        """
        ret = Trajectories.initialize_trajectories(length, number, dim, subspaces, noise, max_velocity, max_d_vel)
        vel, center, d_loc, segmentation, noise, tracks, subspace_sizes = ret
        dd_rot = np.random.random((length, dim, subspaces)) * 2 * max_rotation - max_rotation
        rot = np.cumsum(np.cumsum(dd_rot, axis=0), axis=0)
        centers = np.zeros((length, dim, subspaces))
        for i in range(0, subspaces):
            id_from, id_to = subspace_sizes[i], subspace_sizes[i + 1]
            projection, _ = cv2.Rodrigues(rot[0, :, i])
            centers[0, :, i] = center[i, :, 0]
            tracks[:dim, id_from:id_to] = np.repeat(centers[0, :, i][:, np.newaxis], id_to - id_from, axis=1)
            tracks[:dim, id_from:id_to] += projection.dot(d_loc[id_from:id_to, :, :].T.reshape((dim, -1)))
            segmentation[id_from:id_to] = i
            for j in range(dim, length * dim, dim):
                projection_to_new, _ = cv2.Rodrigues(rot[j / dim, :, i])
                location_change = projection_to_new.dot(vel[j / dim, :, i])
                centers[j / dim, :, i] = centers[j / dim - 1, :, i] + location_change
                tracks[j:j + dim, id_from:id_to] = centers[j / dim, :, i][:, np.newaxis].repeat(id_to - id_from, axis=1)
                projected_body_offset = projection_to_new.dot(d_loc[id_from:id_to, :, :]).reshape((dim, -1))
                tracks[j:j + dim, id_from:id_to] += projected_body_offset
        tracks += noise
        mask = np.random.random((length, number)) > sparseness
        mask = np.repeat(mask[:, :], dim, axis=0)
        tracks[not mask] = 0.0
        return tracks, segmentation, mask

    # noinspection PyUnresolvedReferences
    @staticmethod
    def initialize_trajectories(length, number, dimensions, subspaces, noise, max_velocity, max_acceleration):
        """
        Initializes the containers needed to generate the tracks. Also the initial location and speed are defined.

        :param length: The length of the tracks
        :type length: int
        :param number: The number of tracks
        :type number: int
        :param dimensions: The dimension of each point generated for the track.
        :type dimensions: int
        :param subspaces: The number of subspaces
        :type subspaces: int
        :param noise: The standard deviation of the noise added to each point
        :type noise: float
        :param max_velocity: The maximum velocity
        :type max_velocity: float
        :param max_acceleration: The maximum acceleration
        :type max_acceleration: float
        :return: An array with random velocities, random starting locations, random starting velocities,
            a array for the segmentation, random noise, an empty array for the tracks, and an array with the size of
            each subspace.
        :rtype: (ndarray, ndarray, ndarray, ndarray, ndarray, ndarray, ndarray)
        """
        dd_v = np.random.random((length, dimensions, subspaces)) * 2 * max_acceleration - max_acceleration
        v0 = np.random.random((dimensions, subspaces)) * 2 * max_velocity - max_velocity
        v0 = v0[np.newaxis, :, :].repeat(length, axis=0)
        vel = np.maximum(np.minimum(np.cumsum(dd_v, axis=0) + v0, max_velocity), -max_velocity)
        loc0 = np.random.random((subspaces, dimensions, 1))
        d_loc = np.random.random((number, dimensions, 1)) * 0.1
        segmentation = np.zeros(number)
        if noise > 0.0:
            noise = np.random.normal(0.0, noise, (length * dimensions, number))
        else:
            noise = np.zeros((length * dimensions, number))
        tracks = np.zeros((length * dimensions, number))
        subspace_sizes = np.hstack((0, np.random.random(subspaces) + 0.1))
        subspace_sizes = np.asarray(np.cumsum(subspace_sizes / subspace_sizes.sum()) * number, np.int)
        return vel, loc0, d_loc, segmentation, noise, tracks, subspace_sizes

    def roc(self, probabilities):
        """
        Computes the true positives and false positives based on the probabilities.

        :param probabilities: NxM array with probabilities for N points belonging to M subspaces.
        :type probabilities: ndarray
        :return: (N*N)x2 array with the false and true positives of assigning two points to the same subspace
        :rtype: ndarray
        """
        truth = ind2vec(self.ground_truth)
        same_label_truth = truth.dot(truth.T)
        same_label = probabilities.dot(probabilities.T)
        fpr, tpr, _ = roc_curve(same_label_truth.flatten(), same_label.flatten())
        return np.vstack((fpr, tpr)).T


def show_trajectories(data, mask, ax=None, color=None):
    """
    Shows the trajectories in a 3D plot.

    :param data: Nx(3*M) array with N trajectories of size M (with 3 coordinates per point).
    :type data: ndarray
    :param mask: Nx(3*M) mask for the trajectories. It is assumed that the either a whole point (x, y, z) is missing
        or present.
    :type mask: ndarray
    :param ax: The axis to draw on
    :type ax: matplotlib.pyplot.axis
    :param color: The color to draw with
    :type color: str
    """
    if ax is None:
        fig = plt.figure(23235)
        ax = fig.add_subplot(111, projection='3d')
    for track, track_mask in zip(data.T, mask.T):
        start = np.where(track_mask)[0]
        # Assume that xs, ys and zs have the same missing values.
        xs = start[start % 3 == 0]
        ys = start[start % 3 == 1]
        zs = start[start % 3 == 2]
        if color is None:
            ax.plot(track[xs], track[ys], track[zs])
            ax.scatter(track[xs][0], track[ys][0], track[zs][0])
        else:
            ax.plot(track[xs], track[ys], track[zs], color=color)
            ax.scatter(track[xs][0], track[ys][0], track[zs][0], color=color)