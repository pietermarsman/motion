# coding=utf-8
"""
Reading images from folders and camera. Calibrating the camera's using a chessboard. Altering the images with the 
found parameters from the (stereo) calibration. Triangulating points that were found using these camera's.
"""

import os
import time

import numpy as np
import cv2

from miscellaneous import create_directory
from projections import from_homogeneous, compose


__author__ = 'Pieter Marsman'


class Camera(object):
    """
    Single camera that can be calibrated
    """

    KITTI_DATASET_FOLDER = 'C:/Users/PieterM/Videos/dataset/data/odometry/sequences/'
    CALIBRATION_FOLDER = 'calibration/'
    CALIBRATION_CRITERIA = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 100, 1e-5)
    CALIBRATION_FLAGS = (cv2.CALIB_FIX_PRINCIPAL_POINT + cv2.CALIB_ZERO_TANGENT_DIST)
    CHESSBOARD_SQUARE_SIZE = 2.5 # cm

    def __init__(self, video_capture_source, identifier, image_width, image_blur, equalize_histogram_alpha,
                 start_frame=None):
        """
        Initializes criteria and containers

        :param video_capture_source: The input for cv2.VideoCapture.
        :type video_capture_source: int | str
        :param identifier: A unique object that can be converted to string to identify this object. Is used to save
            images and parameters to the hard drive and load them again the next time.
        :type identifier: object
        :param image_width: Width of the camera feed. The image and intrinsic parameters are rescaled using this.
        :type image_width: int
        :param image_blur: Size of the Gaussian blur on the images. Should be uneven. If 0 nothing happens
        :type image_blur: int
        :param equalize_histogram_alpha: The ratio between a equalized histogram and the original histogram. 0.0
            means the not changing the image and using the old histogram and 1.0 means completely using the new
            histogram.
        :type equalize_histogram_alpha: float
        """
        self._original_width = None
        """:type: int"""
        self.image_size = None
        """:type: tuple"""
        self.rms = None
        """:type: float"""
        self._intrinsic = None
        """:type: ndarray"""
        self._new_intrinsic = None
        """:type: ndarray"""
        self.distortion = None
        """:type: ndarray"""
        self.undistortion_map = None
        """:type: ndarray"""
        self.rectification_map = None
        """:type: ndarray"""
        self.roi = None
        """:type: tuple"""
        self._chessboards_found = 0
        self._image_points = []
        self._object_points = []
        self.video_capture_source = video_capture_source
        self.identifier = str(identifier)
        self.image_width = image_width
        self.image_blur = image_blur
        self.equalize_alpha = equalize_histogram_alpha
        self.loop = 0
        # noinspection PyArgumentList
        self._cam = cv2.VideoCapture(self.video_capture_source)
        # Fills the parameters without proceeding to the next image in the stream
        self.get_image(proceed=False)
        if start_frame is not None:
            self._cam.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, start_frame)

    def __str__(self):
        ret = '=== ' + self.__class__.__name__ + ' ==='
        ret += '\nName: ' + str(self.identifier)
        ret += '\nImage width: ' + str(self.image_width)
        ret += '\nImage blur: ' + str(self.image_blur)
        ret += '\nEqualize histogram: ' + str(self.equalize_alpha)
        ret += '\nIntrinsic parameters: ' + str(np.round(self._new_intrinsic.flatten(), 2))
        ret += '\nDistortion parameters: ' + str(np.round(self.distortion, 2))
        ret += '\nRegion of interest: ' + str(self.roi)
        ret += '\nRMS: ' + str(self.rms)
        return ret

    def get_image(self, proceed=True):
        """
        Gets an image from the VideoCapture and post process it. It is cropped (with the roi), rescaled,
        histogram equalized and converted to lab. Eventually a blur is applied to the lab image.

        :param proceed: If the image stream should continue to the next image. 
        :type proceed: bool
        :return: (img, lab) image from the camera in rbg and lab color space
        :rtype: tuple of np.ndarray
        """
        # Get the images
        success, img = self._cam.read()
        if not success:
            self._cam.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, 0)
            success, img = self._cam.read()
            self.loop += 1
        if not success:
            raise IOError("I could not read the images from your hard drive. Are you sure you specified the right "
                          "path to the KITTI dataset in KITTI_DATASET_FOLDER?")
        # If it is an image feed from file and the end is reached, proceed if specified with the first image
        if not proceed and type(self.video_capture_source) is str:
            self._cam.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, self._cam.get(cv2.cv.CV_CAP_PROP_POS_FRAMES) - 1)
        # Set a roi
        if self.roi is not None:
            if self.roi[2] > 0 and self.roi[3] > 0:
                img = img[self.roi[1]:self.roi[3], self.roi[0]:self.roi[2], :]
        # Resize the image
        self._original_width = img.shape[1]
        if self.image_width < self._original_width:
            img = self.resize(img)
        self.image_size = (img.shape[1], img.shape[0])
        # Equalize the image
        if self.equalize_alpha > 0.0:
            img = self.histogram_equalization(img)
        # Rectify the image
        img = self._rectify_image(img)
        # Convert it to lab space
        lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
        # Blur the lab image
        if self.image_blur > 0 and self.image_blur % 2:
            lab = cv2.GaussianBlur(lab, (self.image_blur, self.image_blur), 0)
        return img, lab

    def normalize_keypoints(self, keypoints_left):
        """
        Normalizes the point such that the center of the camera is at origin and the focal distance is 1.0.

        :param keypoints_left: Nx2 array with keypoint in the left camera frame
        :type keypoints_left: ndarray
        :return: The normalized points
        :rtype: ndarray
        """
        intrinsic, distortion = self.get_calibration_parameters()
        points1 = cv2.undistortPoints(keypoints_left.reshape((1, -1, 2)), intrinsic, distortion).reshape((-1, 2))
        return points1

    def calibrate(self, calibrate_from_file=False, number_of_images=20, chessboard_size=(9, 6)):
        """
        Calibrate the camera. Can either read the parameters from a file (from an earlier calibration) or use images
            to calculate the parameters. If images are used, images from earlier calibrations are also used to speed up
            the process of capturing images. If this is not suitable, remove the images from the CALIBRATION_FOLDER.

        :param calibrate_from_file: True if it should use the parameters stored in a file
        :type calibrate_from_file: bool
        :param number_of_images: to use for the calibration
        :type number_of_images: int
        :param chessboard_size: size of the chessboard
        :type chessboard_size: tuple of int
        """
        if calibrate_from_file:
            self._load_parameters()
        else:
            self._find_object_image_correspondence(number_of_images, chessboard_size)
            self._find_calibration_parameters()
            self.save_parameters()

    def _find_object_image_correspondence(self, number_of_images, chessboard_size):
        """
        Finds correspondences between a real chessboard and the projections of it onto the stereo camera's

        :param number_of_images: The amount of images to capture that contain chessboards
        :type number_of_images: int
        :param chessboard_size: Size of the inlier chessboard corners
        :type chessboard_size: tuple of int
        """
        print "Please show me", number_of_images, "chessboards"
        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        object_coordinates = np.zeros((np.prod(chessboard_size), 3), np.float32)
        object_coordinates[:, :2] = np.mgrid[0:chessboard_size[0], 0:chessboard_size[1]].T.reshape(-1, 2)
        object_coordinates *= Camera.CHESSBOARD_SQUARE_SIZE
        # Initialize arrays for saving of object_coordinates and projections of them into the image
        previous_chessboard_time = 0

        image_folder = self.get_save_folder() + 'mono_calibration_images/'
        debug_folder = self.get_save_folder() + 'mono_calibration_debug/'
        create_directory(image_folder)
        create_directory(debug_folder)
        files = os.listdir(image_folder)

        # Get number_of_images images of chessboards
        while self._chessboards_found < number_of_images:

            if len(files) > 0:
                gray = cv2.imread(image_folder + files[0], flags=cv2.CV_LOAD_IMAGE_GRAYSCALE)
                img = gray
                del files[0]
                previous_chessboard_time = 0
            else:
                img, lab = self.get_image()
                # Convert to gray images
                gray = lab[:, :, 0]

            # Find chessboard corners
            ret, corners = cv2.findChessboardCorners(gray, chessboard_size, None)
            use_image = ret and previous_chessboard_time + 1.0 < time.time()

            # If there are corners found in both images and the last time was 1 second ago
            timestamp = str(int(time.time() * 100))
            if use_image:
                print "I found", self._chessboards_found, "chessboards"
                # Set the counters
                self._chessboards_found += 1
                # Refine the measurements of the corners
                cv2.cornerSubPix(gray, corners, (5, 5), (-1, -1), Camera.CALIBRATION_CRITERIA)
                # Append the object location and projection location to the lists
                self._object_points.append(object_coordinates)
                self._image_points.append(corners)
                if previous_chessboard_time > 0:
                    cv2.imwrite(image_folder + timestamp + '.png', gray)
                previous_chessboard_time = time.time()

            # Draw the chessboard corners to show20
            cv2.drawChessboardCorners(img, chessboard_size, corners, ret)
            cv2.imshow('image', img)
            if use_image:
                debug_file = debug_folder + timestamp + '.png'
                cv2.imwrite(debug_file, img)
            cv2.waitKey(1)

        # Destroy all windows at the end
        cv2.destroyAllWindows()

    def _find_calibration_parameters(self):
        """
        Calculates the intrinsic, rotational and translational parameters of the cameras
        """

        print "I am now going to calibrate the camera with these", self._chessboards_found, "images of " \
                                                                                            "chessboards"
        _, img = self.get_image()
        self.image_size = (img.shape[1], img.shape[0])
        self.rms, self._intrinsic, self.distortion, _, _, = cv2.calibrateCamera(self._object_points, self._image_points,
                                                                                self.image_size,
                                                                                flags=Camera.CALIBRATION_FLAGS)
        # Get camera matrix that is optimal
        self._new_intrinsic, _ = cv2.getOptimalNewCameraMatrix(self._intrinsic, self.distortion, self.image_size, 1,
                                                               self.image_size)

    def get_calibration_parameters(self):
        """
        The intrinsic parameters of the camera need to be scaled in order to match to the image width. Nothing
        happens with the distortion.

        :return: intrinsic and distortion parameters corrected for the possible rescaled image
        :rtype: tuple of np.ndarray
        """
        # Compute scaling factor of the images
        scaling_factor = 1.0
        if self._original_width is not None and self.image_width is not None:
            scaling_factor = float(self.image_width) / float(self._original_width)
        # Scale the intrinsic values by the scaling factor
        scaled_intrinsic = self._new_intrinsic * scaling_factor
        scaled_intrinsic[-1, -1] = 1.0
        return scaled_intrinsic, self.distortion

    def set_calibration_parameters(self, intrinsic, distortion, new_intrinsic=None):
        """
        Sets the calibration parameters to new values

        :param intrinsic: 3x3 array of intrinsic camera parameters
        :type intrinsic: numpy.ndarray
        :param distortion: 5x0 array of distortion camera parameters
        :type distortion: numpy.ndarray
        :param new_intrinsic: 3x3 array of new intrinsic camera parameters
        :type new_intrinsic: numpy.ndarray
        """
        self._intrinsic = intrinsic
        self.distortion = distortion
        if new_intrinsic is not None:
            self._new_intrinsic = new_intrinsic

    def save_parameters(self):
        """
        Save parameters to parameters/xxx.npy
        """
        folder = self.get_save_folder() + 'mono_parameters/'
        create_directory(folder)
        np.save(folder + 'intrinsic', self._intrinsic)
        np.save(folder + 'distortion', self.distortion)
        np.save(folder + 'new_intrinsic', self._new_intrinsic)
        np.save(folder + 'roi', self.roi)
        np.save(folder + 'rms', self.rms)

    def _load_parameters(self):
        """
        Load parameters from parameters/xxx.npy
        """
        folder = self.get_save_folder() + 'mono_parameters/'
        self._intrinsic = np.load(folder + 'intrinsic.npy')
        self.distortion = np.load(folder + 'distortion.npy')
        self._new_intrinsic = np.load(folder + 'new_intrinsic.npy')
        self.roi = np.load(folder + 'roi.npy')
        if len(self.roi.shape) > 0:
            self.roi = tuple(self.roi)
        else:
            self.roi = None
        self.rms = np.load(folder + 'rms.npy').item()

    def _rectify_image(self, img):
        """
        Remaps the image to get horizontal lines if the parameters are available

        :param img: The image that should be rectified with the parameters from this camera.
        :type img: ndarray
        """
        # Only perform remap if the camera's are fully calibrated
        if self._new_intrinsic is not None and self.distortion is not None:
            img = cv2.undistort(img, self._new_intrinsic, self.distortion)
        if self.undistortion_map is not None and self.rectification_map is not None:
            img = cv2.remap(img, self.undistortion_map, self.rectification_map, cv2.INTER_NEAREST)
        return img

    def release(self):
        """
        Releases the camera's such that they can be used by another program
        """
        self._cam.release()

    def histogram_equalization(self, image):
        """
        Balances the brightness values of the image. Also called histogram equalization. It uses the parameter 
        histogram_equalization that is specified when the Camera is created.

        :param image: The image
        :type image: ndarray
        :return: The image with balanced brightness values.
        :rtype: ndarray
        """
        histogram, _ = np.histogram(image.flatten(), 256, [0, 256])
        cdf = np.cumsum(histogram).astype(np.float)
        cdf = cdf / cdf.max() * 255

        cdf_equal = np.ma.masked_equal(cdf, 0)
        cdf_equal = (cdf_equal - cdf_equal.min()) * 255 / (cdf_equal.max() - cdf_equal.min())
        cdf_new = self.equalize_alpha * cdf_equal + (1.0 - self.equalize_alpha) * np.array(np.linspace(0, 255, 256))
        cdf_new = np.ma.filled(cdf_new, 0).astype('uint8')
        img2 = cdf_new[image]
        return img2

    def get_save_folder(self):
        """
        Gets a unique folder for this camera based on its identifier.

        :return: the folder in which parameters and images can be saved. The identifier of this camera is used to get
            a unique folder.
        :rtype: str
        """
        return Camera.CALIBRATION_FOLDER + self.identifier + '/'

    @staticmethod
    def get_default_camera(scene, image_width, image_blur, equalize_histogram, start_frame=0):
        """
        Gets a default camera either by using a real camera or reading images from folders.

        :param scene: input to use
        :type scene: int
        :param image_width: width of the input feed
        :type image_width: int
        :param image_blur: blur on the input feed
        :type image_blur: int
        :param equalize_histogram: The weighting between the equalized histogram and the old histogram. With 1.0 only
            the equalized histogram is used. With 0.0 only the original histogram is used.
        :type equalize_histogram: float
        :param start_frame: Specifies the first frame that should be used. Useful for starting the video half way.
        :type start_frame: int
        :return: Camera object that reads from the source
        :rtype: Camera
        """
        # Initialize stereo feed from cameras. Scene 0 is the left camera (source 1)
        if scene == 0:
            source = 1
        elif scene == 1:
            source = 0
        # Initialize stereo feed from file
        elif 1 < scene < 44:
            source = Camera.KITTI_DATASET_FOLDER + "{:0>2d}".format((scene - 2) / 2) + '/image_' + str(
                scene % 2 + 2) + '/%06d.png'
        else:
            raise Exception('Scene should be a number in [0,50]')
        return Camera(source, scene, image_width, image_blur, equalize_histogram, start_frame)

    def resize(self, img):
        """
        Resize image to width specified when creating this camera.

        :param img: input image
        :type img: ndarray
        :return: image with new size
        :rtype: ndarray
        """
        if self.image_width is not None:
            height, width = img.shape[:2]
            ratio = float(self.image_width) / float(width)
            img = cv2.resize(img, dsize=(self.image_width, int(height * ratio)))
        return img

    @staticmethod
    def calibrate_from_string():
        """
        Short method for calibrating a camera with parameters from the internet
        """
        feed = int(raw_input("Feed "))
        cam = Camera.get_default_camera(feed, feed, 640, 0, True)
        intrinsic_string = raw_input("Intrinsic ")
        intrinsic_string = intrinsic_string.split(' ')
        intrinsic_string = map(float, intrinsic_string)
        intrinsic_string = np.array(intrinsic_string).reshape((3, 3))
        cam._intrinsic = intrinsic_string
        cam.distortion = np.zeros(5)
        cam._new_intrinsic = intrinsic_string
        cam.save_parameters()


class StereoCamera(object):
    """
    Two cameras that can be used for depth estimation.
    """

    CALIBRATION_CRITERIA = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 100, 1e-5)
    CALIBRATION_FLAGS = (
        cv2.CALIB_FIX_PRINCIPAL_POINT + cv2.CALIB_FIX_INTRINSIC + cv2.CALIB_USE_INTRINSIC_GUESS +
        cv2.CALIB_ZERO_TANGENT_DIST)

    def __init__(self, left_camera, right_camera):
        """
        :param left_camera: The left camera.
        :type left_camera: Camera
        :param right_camera: The right camera.
        :type right_camera: Camera
        """
        self.translation = None
        """:type: ndarray"""
        self.rotation = None
        """:type: ndarray"""
        self.rms = None
        """:type: float"""
        self._Q = None
        """:type: ndarray"""
        self.E = None
        """:type: ndarray"""
        self.F = None
        self._img_pts = dict(left=[], right=[])
        self._obj_pts = []
        self.cam = dict(left=left_camera, right=right_camera)
        """:type: dict[str, Camera]"""

    def __str__(self):
        ret = '=== ' + self.__class__.__name__ + ' ==='
        ret += '\nTranslation from left to right: ' + str(self.translation.flatten())
        ret += '\nRotation from left to right: \n' + str(self.rotation)
        ret += '\nRMS :' + str(self.rms)
        ret += '\n> With left camera:\n'
        ret += self.cam['left'].__str__()
        ret += '\n> And right camera:\n'
        ret += self.cam['right'].__str__()
        return ret

    def calibrate(self, stereo_calibrate_from_file=False, mono_calibrate_from_file=False, number_of_images=20,
                  chessboard_size=(9, 6)):
        """
        Calibrate a stereo camera. First the two individual camera's are calibrated and then the stereo setup is
        calibrated.

        :param stereo_calibrate_from_file: True if the stereo camera parameters should be read from file
        :type stereo_calibrate_from_file: bool
        :param mono_calibrate_from_file: True if the mono camera parameters should be read from file
        :type mono_calibrate_from_file: bool
        :param number_of_images: The number of images to use
        :type number_of_images: int
        :param chessboard_size: Size of the chessboard
        :type chessboard_size: tuple of int
        """
        # Calibrate the individual cameras
        for side in ['left', 'right']:
            self.cam[side].calibrate(mono_calibrate_from_file, number_of_images, chessboard_size)
        # Either load the parameters
        if stereo_calibrate_from_file:
            self._load_parameters()
        # Or use images from the hard drive and the live feed
        else:
            # Save the width, but reset it because else it would influence the stereo calibration
            width = self.cam['left'].image_width
            self.cam['left'].image_width = None
            self.cam['right'].image_width = None
            # Live calibration. Reset roi to prevent previous roi's to be used.
            for side in ['left', 'right']:
                self.cam[side].roi = None
            # Do the actual calibration
            self._find_object_stereo_image_correspondence(number_of_images, chessboard_size)
            self._find_stereo_calibration_parameters()
            self._find_rectification_parameters()
            # Save the parameters
            self.save_parameters()
            # Reset the width
            self.cam['left'].image_width = width
            self.cam['right'].image_width = width

    def _find_object_stereo_image_correspondence(self, number_of_images, chessboard_size):
        """
        Finds correspondences between a real chessboard and the projections of it onto the stereo camera's

        :param number_of_images: The amount of images to capture from the two stereo camera's with the chessboard
            visible
        :type number_of_images: int
        :param chessboard_size: Size of the inlier corners on the chessboard
        :type chessboard_size: tuple of int
        """

        print "Please show me", number_of_images, "chessboards"
        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        object_coordinates = np.zeros((np.prod(chessboard_size), 3), np.float32)
        object_coordinates[:, :2] = np.mgrid[0:chessboard_size[0], 0:chessboard_size[1]].T.reshape(-1, 2)
        object_coordinates *= Camera.CHESSBOARD_SQUARE_SIZE
        # Initialize arrays for saving of object_coordinates and projections of them into the image
        previous_chessboard_time = 0
        chessboards_found = 0

        image_folder = Camera.CALIBRATION_FOLDER + 'stereo_calibration_images/'
        debug_folder = Camera.CALIBRATION_FOLDER + 'stereo_calibration_debug/'
        create_directory(image_folder)
        create_directory(debug_folder)
        calibration_images_from_file = os.listdir(image_folder)

        # Get number_of_images images of chessboards
        while chessboards_found < number_of_images:

            # Images that are save on the disk are automatically used.
            if len(calibration_images_from_file) > 1:
                gray_l = cv2.imread(image_folder + calibration_images_from_file[0], flags=cv2.CV_LOAD_IMAGE_GRAYSCALE)
                gray_r = cv2.imread(image_folder + calibration_images_from_file[1], flags=cv2.CV_LOAD_IMAGE_GRAYSCALE)
                img_l = gray_l
                img_r = gray_r
                del calibration_images_from_file[0]
                del calibration_images_from_file[0]
                previous_chessboard_time = 0
            # If no images left an live feed is used
            else:
                img_l, lab_l = self.cam['left'].get_image()
                img_r, lab_r = self.cam['right'].get_image()
                # Convert to gray images
                gray_l = lab_l[:, :, 0]
                gray_r = lab_r[:, :, 0]
            # Find chessboard corners
            ret_l, corners_l = cv2.findChessboardCorners(gray_l, chessboard_size, None)
            ret_r, corners_r = cv2.findChessboardCorners(gray_r, chessboard_size, None)

            use_images = ret_l and ret_r and previous_chessboard_time + 1.0 < time.time()

            # If there are corners found in both images and the last time was 1 second ago
            timestamp = str(int(time.time() * 100))
            if use_images:
                print "I found", chessboards_found, "chessboards"
                # Set the counters
                chessboards_found += 1
                # Refine the measurements of the corners
                cv2.cornerSubPix(gray_l, corners_l, (5, 5), (-1, -1), StereoCamera.CALIBRATION_CRITERIA)
                cv2.cornerSubPix(gray_r, corners_r, (5, 5), (-1, -1), StereoCamera.CALIBRATION_CRITERIA)
                # Append the object location and projection location to the lists
                self._obj_pts.append(object_coordinates)
                self._img_pts['left'].append(corners_l)
                self._img_pts['right'].append(corners_r)

                if previous_chessboard_time > 0:
                    cv2.imwrite(image_folder + timestamp + 'l.png', gray_l)
                    cv2.imwrite(image_folder + timestamp + 'r.png', gray_r)
                previous_chessboard_time = time.time()

            # Draw the chessboard corners to show
            cv2.drawChessboardCorners(img_l, chessboard_size, corners_l, ret_l)
            cv2.drawChessboardCorners(img_r, chessboard_size, corners_r, ret_r)
            cv2.imshow('Left image', img_l)
            cv2.imshow('Right image', img_r)
            if use_images:
                cv2.imwrite(debug_folder + timestamp + 'l.png', img_l)
                cv2.imwrite(debug_folder + timestamp + 'r.png', img_r)

            cv2.waitKey(1)

        # Destroy all windows at the end
        cv2.destroyAllWindows()

    def _find_stereo_calibration_parameters(self):
        """
        Calibrates the stereo camera.
        """
        print 'I am now going to calibrate your stereo camera with these images'
        self.get_stereo_image()
        in0, dist0 = self.cam['left'].get_calibration_parameters()
        in1, dist1 = self.cam['right'].get_calibration_parameters()
        # noinspection PyPep8
        img_size = self.cam['left'].image_size
        kwargs = dict(criteria=StereoCamera.CALIBRATION_CRITERIA, flags=StereoCamera.CALIBRATION_FLAGS)
        ret = cv2.stereoCalibrate(self._obj_pts, self._img_pts['left'], self._img_pts['right'], img_size, in0, dist1,
                                  in1, dist0, **kwargs)
        self.rms, in0, dist0, in1, dist1, self.rotation, self.translation, self.E, self.F = ret
        self.cam['left'].set_calibration_parameters(in0, dist0)
        self.cam['right'].set_calibration_parameters(in1, dist1)

    def _find_rectification_parameters(self):
        """
        Finds the mappings from the images to the translated and undistorted images
        """
        # Compute the rotation and projection matrices between the stereo cameras
        in0, dist0 = self.cam['left'].get_calibration_parameters()
        in1, dist1 = self.cam['right'].get_calibration_parameters()
        kwargs = dict(alpha=0.0, flags=cv2.CALIB_ZERO_DISPARITY)
        img_size = self.cam['left'].image_size
        ret = cv2.stereoRectify(in0, dist0, in1, dist1, img_size, self.rotation, self.translation, **kwargs)
        rot = dict()
        rot['left'], rot['right'], _, _, self._Q, self.cam['left'].roi, self.cam['right'].roi = ret
        # Create a mapping to un-distort the images based on the translation and rotation between the cameras
        for side in ['left', 'right']:
            param0, param1 = self.cam[side].get_calibration_parameters()
            ret = cv2.initUndistortRectifyMap(param0, param1, rot[side], param0, img_size, cv2.CV_32FC1)
            self.cam[side].undistortion_map, self.cam[side].rectification_map = ret

    def get_stereo_image(self):
        """
        Gets a stereo image from the camera.

        :return: ((img0, lab0), (img1, lab1)) two rbg images and two lab images
        :rtype: tuple of tuple of ndarray
        """
        return self.cam['left'].get_image(), self.cam['right'].get_image()

    def triangulate_disparity(self, disparity):
        """
        Triangulates points from the disparity image, and thus assuming that pixels only differ in their y location

        :param disparity: 2D disparity array that indicates the shift of pixels from the left to the right frame
        :type disparity: ndarray
        :return: 3D array that indicates a 3D position for each of the pixels in the disparity image
        :rtype: ndarray
        """
        points_3d = cv2.reprojectImageTo3D(disparity, self._Q)
        return points_3d

    def triangulate_matches(self, points1, points2):
        """
        Triangulate the point correspondences with the calibrated camera

        :param points1: Nx2 points from left camera
        :type points1: ndarray
        :param points2: Nx2 points from right camera
        :type points2: ndarray
        :return: Nx3 points relative to the first camera
        :rtype: ndarray
        """
        p1, p2 = self.get_extrinsic()
        # Undistort points
        points1 = self.cam['left'].normalize_keypoints(points1)
        points2 = self.cam['right'].normalize_keypoints(points2)
        # Get the 4D points
        points_3d = from_homogeneous(cv2.triangulatePoints(p1, p2, points1.T, points2.T).T)
        return points_3d, points1, points2

    def get_extrinsic(self):
        """
        The projections for the left and right camera. The left camera always has an identity projection. The
        projection from the right camera is based on the rotation and translation between the left and right camera.

        :return: The left and right projection
        :rtype: (ndarray, ndarray)
        """
        # Create the projection matrices
        p1 = np.array(np.eye(3, 4), np.float)
        p2 = compose(self.rotation, self.translation)
        return p1, p2

    def release(self):
        """
        Releases the camera's such that they can be used for another program
        """
        for side in ['left', 'right']:
            self.cam[side].release()

    def save_parameters(self):
        """
        Save parameters to parameters/xxx.npy
        """
        for side in ['left', 'right']:
            self.cam[side].save_parameters()
        folder = self.cam['left'].get_save_folder() + 'stereo_parameters/'
        create_directory(folder)
        np.save(folder + 'camera_rotation', self.rotation)
        np.save(folder + 'camera_translation', self.translation)
        np.save(folder + 'rms_stereo', self.rms)
        np.save(folder + 'Q', self._Q)

    def _load_parameters(self):
        """
        Load parameters from parameters/xxx.npy
        """
        folder = self.cam['left'].get_save_folder() + 'stereo_parameters/'
        self.rotation = np.load(folder + 'camera_rotation.npy')
        self.translation = np.load(folder + 'camera_translation.npy')
        self.rms = np.load(folder + 'rms_stereo.npy')
        if len(self.rms.shape) is 0:
            self.rms = None
        self._Q = np.load(folder + 'Q.npy')

    # noinspection PyProtectedMember
    @staticmethod
    def calibrate_from_string(feed=None):
        """
        Method to calibrate camera with strings from the internet

        :param feed: The feed that is calibrated with a string. If not provided the user is asked for the feed id.
        :type feed: int | None
        """
        if feed is None:
            feed = int(raw_input("Feed "))
        else:
            print 'Feed', feed
        cam_l = Camera.get_default_camera(feed, 500, 0, 0.5, 0)
        cam_r = Camera.get_default_camera(feed + 1, 500, 0, 0.5, 0)
        cam = StereoCamera(cam_l, cam_r)
        values = dict()
        # img, lab = cam.cam['left'].get_image()
        # values['size'] = img.shape
        for k, s in dict(left="P1 ", right="P2 ", size="Size ").iteritems():
            raw = raw_input(s)
            raw = raw.split(' ')
            raw = map(float, raw)
            values[k] = np.array(raw).reshape((3, -1))
        rotation = dict(left=None, right=None)
        translation = dict(left=None, right=None)
        values['size'] = tuple(values['size'].reshape(-1).astype(np.int))
        for side in ['left', 'right']:
            cam.cam[side]._intrinsic, rotation[side], translation[side], _, _, _, _ = cv2.decomposeProjectionMatrix(
                values[side])
            translation[side] = translation[side][:3] / translation[side][3]
            cam.cam[side].distortion = np.zeros(5)
            cam.cam[side]._new_intrinsic = cam.cam[side]._intrinsic
            cam.cam[side].image_size = values['size'][:2]
            cam.cam[side]._original_width = values['size'][0]
        cam.rotation = rotation['right'].dot(rotation['left'].T)
        # Convert to centimeters
        cam.translation = (translation['left'] - cam.rotation.dot(translation['right'])) * 100.0
        cam._find_rectification_parameters()
        cam.save_parameters()