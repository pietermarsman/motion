# coding=utf-8
"""
Keeping track of the location and rotation of a vehicle
"""
from warnings import warn

from numpy.linalg import LinAlgError
from pykalman import KalmanFilter
from matplotlib import pyplot as plt
from filterpy.kalman import UKF

from projections import *


__author__ = 'Pieter Marsman'


class EgomotionTracker(object):
    """
    Keeps track of rotational and translational pose and velocity using multiple measurements of the same time. These
    measurements correspond to the egomotions since the t-1 frame, the t-2 frame, etc.
    """

    # TODO use the time between measurements

    def __init__(self, trans_cov_multiplier, obs_cov_multiplier):
        """
        Initializes a Kalman filter that uses multiple measurements of egomotion between the current frame and frames
        in the past
        """
        # Define sizes of matrices
        self.rotation_observation_size = 6
        self.translation_size = 3
        self.single_observation_size = self.rotation_observation_size + self.translation_size
        self.total_observation_size = self.single_observation_size
        self.single_state_size = self.rotation_observation_size + self.translation_size
        self.total_state_size = self.single_state_size * 2
        # Define transition and observation matrices
        self.transition_matrix, self.transition_covariance = self.create_transition_matrix(trans_cov_multiplier)
        self.observation_matrix, self.observation_covariance = self.create_observation_matrix(obs_cov_multiplier)
        # Define initial state
        self._mean, self.cov = self.create_initial_state()
        # Initialize Kalman filter
        self.kf = KalmanFilter(transition_matrices=self.transition_matrix, observation_matrices=self.observation_matrix,
                               transition_covariance=self.transition_covariance,
                               observation_covariance=self.observation_covariance, initial_state_mean=self._mean,
                               initial_state_covariance=self.cov)
        # Define state
        self._means = self._mean.reshape((1, -1))
        self.observations = np.zeros((1, self.total_observation_size))

    def __str__(self):
        ret = '=== ' + self.__class__.__name__ + ' ==='
        ret += '\nState size: ' + str(self.total_state_size)
        ret += '\nObservation size: ' + str(self.total_observation_size)
        return ret

    def update(self, rotation, translation, inlier_count):
        """
        Converts the dictionary of rotations and translations to an observation for the Kalman filter. Predicts the
        current state and uses the observations to correct it.
        :param rotation: Measurements of rotation between current frame and the world frame
        :type rotation: ndarray | None
        :param translation: Measurements of translation between current frame and the world frame
        :type translation: ndarray | None
        :param inlier_count: the number of inlier features
        :type inlier_count: int | None
        """
        observation = self.convert_to_observation(rotation, translation)
        observation_cov = self.convert_to_observation_covariance(inlier_count)
        self._mean, self.cov = self.kf.filter_update(self._mean, self.cov, observation,
                                                     observation_covariance=observation_cov)
        self._means = np.append(self._means, self._mean.reshape((1, -1)), axis=0)
        self.observations = np.append(self.observations, observation.reshape((1, -1)), axis=0)
        return self._means, self.cov

    def convert_to_observation(self, rotation, translation):
        """
        Changes the dictionaries of egomotion measurements in the past to observation vectors for the Kalman filter
        :param rotation: Measurements of rotation between current frame and the world frame
        :type rotation: ndarray
        :param translation: Measurements of translation between current frame and the world frame
        :type translation: ndarray
        """
        if rotation is not None and translation is not None:
            rotation = rodrigues_to_filterable(rotation)
            observation = np.zeros(self.total_observation_size)
            observation[:3] = translation
            observation[3:] = rotation
            return observation
        else:
            arr = np.ma.zeros(self.total_observation_size)
            arr[:] = np.ma.masked
            return arr

    def convert_to_observation_covariance(self, inlier_count):
        """
        Convert the number of inlier features to a measure of uncertainty for the observation
        :param inlier_count: The number of inlier features during the egomotion computation between two frames
        :type inlier_count: int
        :return: The observation covariance
        :rtype: ndarray
        """
        if inlier_count is not None:
            observation_covariance = self.observation_covariance * 20.0 / inlier_count
            return observation_covariance
        else:
            arr = np.ma.zeros((self.total_observation_size, self.total_observation_size))
            arr[:, :] = np.ma.masked
            return arr

    def create_transition_matrix(self, trans_cov_multiplier):
        """
        Creates a transition matrix for a Tracker with multiple measurements per time of the rotation and translation

        :param trans_cov_multiplier: Multiplication scalar to scale the identity matrix to create the covariance
        :type trans_cov_multiplier: float
        :return: The transition matrix
        :rtype: ndarray
        """
        transition_matrix = np.zeros((self.total_state_size, self.total_state_size))
        size = self.translation_size
        # location/velocity<-location/velocity
        transition_matrix[:6, :6] = np.eye(size * 2)
        # location<-velocity
        transition_matrix[:3, 3:6] = np.eye(size)
        # angles<-angles
        transition_matrix[6:, 6:] = np.eye(self.rotation_observation_size * 2)
        # angles<-velocity angles
        transition_matrix[6:12, 12:] = np.eye(self.rotation_observation_size)
        # Covariance: everything<-everything
        transition_covariance = np.eye(self.total_state_size)
        # Covariance: location<-location (every prediction (0.1 sec) adds 5.0 cm std noise)
        transition_covariance[:3, :3] *= 5.0
        # Covariance: location<-velocity (every prediction (0.1 sec) adds.0 cm std noise due to motion prediction)
        transition_covariance[:3, 3:6] = np.eye(3) * 5.0
        # Covariance: velocity<-velocity (every prediction (0.1 sec) adds 5.0 cm std noise)
        transition_covariance[3:6, 3:6] *= 5.0
        # Covariance: angles<-angles (every prediction (0.1 sec) adds 2% std noise)
        transition_covariance[6:12, 6:12] *= 0.04
        # Covariance: angles<-velocity angles (every prediction (0.1 sec) add 2% std noise)
        transition_covariance[6:12, 12:] = np.eye(6) * 0.04
        # Covariance: velocity angles<-velocity angles (every prediction (0.1 sec) adds 2% std noise)
        transition_covariance[12:, 12:] *= 0.04
        transition_covariance *= trans_cov_multiplier
        return transition_matrix, transition_covariance

    def create_observation_matrix(self, obs_cov_multiplier):
        """
        Creates a observation matrix for a Tracker with multiple measurements per time of the rotation and translation

        :param obs_cov_multiplier: Scalar to multiply the identity matrix with to create the observation covariance
        :type obs_cov_multiplier: float
        :return: The observation matrix
        :rtype: ndarray
        """
        observation_matrix = np.zeros((self.total_observation_size, self.total_state_size))
        size = self.translation_size
        # observation matrix: observe the current location and current angle
        observation_matrix[:3, :3] = np.eye(size)
        observation_matrix[3:, 6:12] = np.eye(self.rotation_observation_size)
        # observation covariance: location error=50.0cm in range[-inf, inf], angle error=0.05 in range [-1, 1]
        observation_covariance = np.eye(self.total_observation_size)
        # Covariance: location (observed with 100.0cm std noise)
        observation_covariance[:3, :3] *= 100.0
        # Covariance: angles (observed with 5% std noise)
        observation_covariance[3:, 3:] *= 0.1
        observation_covariance *= obs_cov_multiplier
        return observation_matrix, observation_covariance

    def create_initial_state(self):
        """
        Creates the initial state for the tracker
        :return: Initial state where translation and rotation are zero
        :rtype: ndarray
        """
        state = np.zeros(self.total_state_size)
        initial_rotation = rodrigues_to_filterable(np.zeros(3))
        state[2 * self.translation_size:] = np.hstack((initial_rotation, np.zeros(initial_rotation.shape)))
        covariance = np.eye(self.total_state_size)
        # Location is certainly (0.0), but speed is uncertain
        covariance[3:6, 3:6] *= 100000.0
        # Angle is known, but angle speed is uncertain
        covariance[12:, 12:] *= 1000.0
        return state, covariance

    def get_current_rotation(self):
        """
        Get the current rotation of the vehicle
        :return: The rotation of the vehicle compared to the world coordinate system
        :rtype: ndarray
        """
        return filterable_to_rodrigues(
            self._mean[self.translation_size * 2:self.translation_size * 2 + self.rotation_observation_size])

    def get_current_translation(self):
        """
        Get the current translation of the vehicle
        :return: The translation of the vehicle compared to the world coordinate system
        :rtype: ndarray
        """
        return self._mean[:3]

    def predict(self):
        """
        Predict the next projection
        :return: The next state
        :rtype: ndarray
        """
        new_state = self.transition_matrix.dot(self._mean)
        rotation = filterable_to_rodrigues(new_state[6:12])
        return to_projection(rotation, new_state[:3])

    def to_projection(self):
        """
        Get the current projection
        :return: 3x4 projection
        :rtype: ndarray
        """
        start = 2 * self.translation_size
        end = start + self.rotation_observation_size
        return to_projection(filterable_to_rodrigues(self._mean[start:end]), self._mean[:3])

    @staticmethod
    def plot_results(means, covariances=None):
        """
        Plot the results of the tracker in a matplotlib window
        :param means: the filtered means
        :type means: ndarray
        :param covariances: the filtered covariances
        :type covariances: ndarray
        """
        plt.ion()
        fig = plt.figure(1)
        plt.clf()
        ax = fig.add_subplot(121, projection='3d')
        ax.plot(means[:, 0], means[:, 2], means[:, 1])
        EgomotionTracker.set_axes(ax, np.min(means), np.max(means))
        if covariances is not None:
            ax = fig.add_subplot(122)
            mapable = ax.imshow(covariances, interpolation='none')
            fig.colorbar(mapable)
        plt.draw()
        plt.pause(0.001)

    @staticmethod
    def set_axes(ax, minimum, maximum):
        """
        Set all the axis to the minimum and maximum value to get a square view
        :param ax: the ax object to manipulate
        :type ax: Axes
        :param minimum: the minimum value of the ax
        :type minimum: float
        :param maximum: the maximum value of the ax
        :type maximum: float
        """
        ax.set_xlim([minimum, maximum])
        ax.set_ylim([minimum, maximum])
        # noinspection PyUnresolvedReferences
        ax.set_zlim([minimum, maximum])
        ax.set_xlabel('x')
        ax.set_ylabel('z')
        # noinspection PyUnresolvedReferences
        ax.set_zlabel('y')


class PointTracker(object):
    """
    Unscented kalman filter to track the location of points using observations from the image frame
    :param transition_cov: The scaling for the transition covariance
    :type transition_cov: float
    :param observation_cov: The scaling for the observation covariance
    :type observation_cov: float
    :param triangulation_var: The uncertainty of the triangulation in general
    :type triangulation_var: float
    :param depth_var: The uncertainty of the triangulation in the direction of the observation
    :type depth_var: float
    """

    # todo init velocity with low covariance: almost all points do not move

    def __init__(self, transition_cov, observation_cov, speed_cov_multiplier, triangulation_var=25.0, depth_var=6.0):
        self.triangulation_var = triangulation_var
        self.depth_var = depth_var
        self.size = 6
        self.transition_cov = np.eye(self.size) * transition_cov
        self.transition_cov[3:, 3:] *= speed_cov_multiplier
        self.transition_cov[:3, 3:] = np.eye(3) * transition_cov * speed_cov_multiplier
        self.observation_cov2 = np.eye(4) * observation_cov
        self.kf = UKF.UnscentedKalmanFilter(6, 4, 0.1, self.observation2, self.transformation)
        self.kf.R = self.observation_cov2
        self.kf.Q = self.transition_cov

    def update(self, state, cov, obs, d_time, extrinsic, extrinsic2):
        """
        Updates the state of the point using an observation in a single or in a stereo frame
        :param state: The current state
        :param cov: The current covariance
        :param obs: The observation. Can be either a 2-array or 4-array.
        :param d_time: The time that has gone by since the previous state.
        :param extrinsic: The projection of the left camera to the world
        :param extrinsic2: Only when using stereo observation. The projection of the right camera to the left.
        :return: The new state and covariance
        :rtype: (ndarray, ndarray)
        """
        return self._update_stereo(state, cov, obs, d_time, extrinsic, extrinsic2)

    def _update_stereo(self, mean, cov, obs, d_time, world_projection, extrinsic_right):

        self.kf.x = mean
        self.kf.P = cov

        if mean is None:
            rot_w, t_w = decompose(world_projection)
            # Initialize the mean with the triangulated point of the observations in the two frames
            p1 = obs[:2].reshape((-1, 2))
            p2 = obs[2:].reshape((-1, 2))
            mean = from_homogeneous(cv2.triangulatePoints(np.eye(3, 4), extrinsic_right, p1.T, p2.T).T[0])
            mean = world_projection.dot(to_homogeneous(mean))
            mean = np.hstack((mean, np.zeros(3)))
            # Initialize the covariance as somewhat larger in the direction of the observation and somewhat smaller
            # in the directions perpendicular to that. The depth axis is more uncertain.
            rotation = PointTracker.observation_to_rotation(to_homogeneous(obs[:2]))
            rotation = rot_w.T.dot(rotation)
            scale = np.eye(3) * self.triangulation_var
            """:type scale: ndarray"""
            scale[2, 2] *= self.depth_var
            half_cov = rotation.T.dot(scale.dot(scale.dot(rotation)))
            cov = np.zeros((self.size, self.size))
            cov[:3, :3] = half_cov
            cov[3:, 3:] = half_cov * 0.5
            return mean, cov
        elif obs is None:
            try:
                self.kf.predict()
            except LinAlgError:
                warn('LinAlgError')
                print 'ERROR'
            return self.kf.x, self.kf.P
        else:
            self.world_projection = inverse(world_projection)
            self.world_projection_right = combine(self.world_projection, extrinsic_right)
            try:
                self.kf.predict(dt=d_time)
            except LinAlgError:
                warn('LinAlgError catched while predicting the next state for the PointTracker. Not changing the '
                     'state.')
            self.kf.update(obs)
            mean = np.copy(self.kf.x)
            cov = np.copy(self.kf.P)
            return mean, cov

    def observation2(self, state):
        """
        Observation function for an observation in a stereo image frame
        :param state: The state of the filter. [3-array pos, 3-array velocity, 3-array rotation, 3-array rotational
        velocity]
        :return: 4-array with the predicted observation in the camera image frames.
        :rtype: ndarray
        """
        point = to_homogeneous(state[:3])
        obs1 = from_homogeneous(self.world_projection.dot(point))
        obs2 = from_homogeneous(self.world_projection_right.dot(point))
        return np.hstack((obs1, obs2))

    @staticmethod
    def transformation(state, d_time):
        """
        Transformation function that predicts the next state.
        :param d_time: Time that has elapsed.
        :type d_time: float
        :param state: The current state.
        :type state: ndarray
        :return: The predicted state
        :rtype: ndarray
        """
        new_state = np.copy(state)
        new_state[:3] += new_state[3:] * d_time
        new_state[3:] *= 0.9
        # new_state += np.random.random(new_state.shape[0]) * PointTracker.EPS
        return new_state

    @staticmethod
    def observation_to_rotation(observation):
        """
        Converts an observation in undistorted image frame to a rotation: the direction in which it is observed.
        :param observation: The undistorted keypoint
        :type observation: ndarray
        :return: A rotation matrix
        :rtype: ndarray
        """
        x, y, z = observation[0], observation[1], observation[2]
        rot_y = math.atan2(-x, z)
        rot_x = math.atan2(y, z)
        rot_x = np.array([[1, 0, 0], [0, math.cos(rot_x), -math.sin(rot_x)], [0, math.sin(rot_x), math.cos(rot_x)]])
        rot_y = np.array([[math.cos(rot_y), 0, math.sin(rot_y)], [0, 1, 0], [-math.sin(rot_y), 0, math.cos(rot_y)]])
        return rot_x.dot(rot_y)