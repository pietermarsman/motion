# coding=utf-8
"""
Basic computations with 3x4 camera projections
"""
import math

import cv2
import numpy as np


__author__ = 'Pieter Marsman'


def to_projection(euler, translation):
    """
    Converts euler angles and translation to projection

    :param euler: 3-array angles
    :type euler: ndarray
    :param translation: array with 3 elements that are translation
    :type translation: ndarray
    :return: 3x4 array projection
    :rtype: ndarray
    """
    rot_mat = cv2.Rodrigues(euler)[0]
    return np.hstack((rot_mat, translation.reshape((-1, 1))))


def from_projection(projection):
    """
    Convert projection to euler angles and translation

    :param projection: 4x3 array projection
    :type projection: ndarray
    :return: 3-array euler angles and 3x1 translation
    :rtype: (ndarray, ndarray)
    """
    euler = cv2.Rodrigues(projection[:, :-1])[0].T[0]
    return euler, projection[:, -1]


def inverse(projection):
    """
    Computes the inverse projection

    :param projection: 3x4 projection from a to b
    :type projection: ndarray
    :return: 3x4 projection from b to a
    :rtype: ndarray
    """
    rot = projection[:, :-1].T
    trans = - rot.dot(projection[:, -1])
    return np.hstack((rot, trans.reshape((-1, 1))))


def decompose(projection):
    """
    Decomposes a projection into the rotation matrix and translation

    :param projection: 3x4 projection
    :return: 3x3 rotation and 3-array translation
    :rtype: (ndarray, ndarray)
    """
    return projection[:, :-1], projection[:, -1].reshape((-1, 1))


def compose(rotation, translation):
    """
    Composes the projection from a rotation matrix and translation

    :param rotation: 3x3 rotation matrix
    :type rotation: ndarray
    :param translation: Array with 3 elements of translation
    :return: 3x4 projection
    :rtype: ndarray
    """
    return np.hstack((rotation, translation.reshape((-1, 1))))


def combine(projection_b_to_a, projection_c_to_a):
    """
    Combining two projections

    :param projection_b_to_a: Projection from point b to a
    :type projection_b_to_a: ndarray
    :param projection_c_to_a: Projection from point c to a
    :type projection_c_to_a: ndarray
    :return: The projection from d to a. First applying projection_c_to_a and then projection_b_to_a on d results in a.
    :rtype: ndarray
    """
    rot_ba, t_ba = decompose(projection_b_to_a)
    rot_ca, t_ca = decompose(projection_c_to_a)
    rot_da = rot_ba.dot(rot_ca)
    t_da = rot_ba.dot(t_ca) + t_ba
    return compose(rot_da, t_da)


def translation_difference(projection0, projection1):
    """
    Computes the difference in translation between two projections in the reference frame of the first projection

    :param projection0: The first projection
    :type projection0: ndarray
    :param projection1: The second projection
    :type projection1: ndarray
    :return: The difference in location in the reference frame of the first projection
    :rtype: ndarray
    """
    t0 = projection0[:, :-1].dot(projection0[:, -1])
    t1 = projection0[:, :-1].dot(projection1[:, -1])
    return t0 - t1


def rotation_difference(projection0, projection1):
    """
    Computes the difference in rotation between projections in euler angles. The angles go from -pi to pi.

    :param projection0: The first projection
    :param projection1: The second projection
    :return: The difference in rotation in euler angles
    :rtype: ndarray
    """
    euler0 = cv2.Rodrigues(projection0[:, :-1])[0]
    euler1 = cv2.Rodrigues(projection1[:, :-1])[0]
    diff = euler0 - euler1
    diff[diff > math.pi] -= 2.0 * math.pi
    diff[diff < -math.pi] += 2.0 * math.pi
    return diff


def to_homogeneous(points):
    """
    Transforms a array of points to homogeneous points. Homogeneous points express a line crossing the origin and the
        original point.

    :param points: NxM array with points.
    :type points: ndarray
    :return: Nx(M+1) array with homogeneous points
    :rtype: ndarray
    """
    if len(points.shape) is 1:
        return np.hstack((points, 1))
    if len(points.shape) is 2:
        return np.hstack((points, np.ones((points.shape[0], 1))))


def from_homogeneous(points):
    """
    Transforms an array of homogeneous points to normal points. All the values of a point are divided by its last value.

    :param points: NxM array with homogeneous points.
    :type points: ndarray
    :return: Nx(M-1) array with usual points
    :rtype: ndarray
    """
    if len(points.shape) is 1:
        return points[:-1] / points[-1]
    if len(points.shape) is 2:
        return points[:, :-1] / points[:, -1][:, np.newaxis]


def rodrigues_to_filterable(rodrigues):
    """
    Transforms a rodrigues angle to a continuous representation using vectors.

    :param rodrigues: NxM array with the rodrigues angles.
    :type rodrigues: ndarray | float
    :return: Nx(2*M) array with the filterable vector representation of an angle.
    :rtype: ndarray
    """
    return np.hstack((np.sin(rodrigues), np.cos(rodrigues)))


def filterable_to_rodrigues(filterable):
    """
    Transforms the filterable vector representation angles to rodrigues angles.

    :param filterable: NxM array with the filterable representation of an angle.
    :type filterable: ndarray
    :return: Nx(M/2) array with the rodrigues representation of an angle
    :rtype: ndarray
    """
    half = filterable.shape[0] / 2
    return np.arctan2(filterable[:half], filterable[half:])