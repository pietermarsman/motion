# coding=utf-8
"""
Some miscellaneous functions
"""
import os

import numpy as np
from matplotlib import pyplot as plt


__author__ = 'Pieter Marsman'


def create_directory(directory):
    """
    Creates a directory if it not already exists

    :param directory: The path to the directory
    :type directory: str
    """
    if not os.path.exists(directory):
        os.makedirs(directory)


def plot_line(array0, array1):
    """
    Plots a line in the current plot

    :param array0: Nx2 from array
    :type array0: ndarray
    :param array1: Nx2 to array
    :type array1: ndarray
    """
    if len(array0.shape) == 1:
        plt.scatter(array0[0], array0[1], color='b', marker='x')
        plt.plot([array0[0], array1[0]], [array0[1], array1[1]], color='b')
        plt.scatter(array1[0], array1[1], color='g')
    elif len(array0.shape) == 2:
        for pt0, pt1 in zip(array0, array1):
            plot_line(pt0, pt1)


def k_median(data, k, f_cluster, f_cluster_2):
    """
    The k-median algorithm searches for k median in the data. Two functions are used, one to assign data to a
    cluster and the other to vote for data points to be a median..

    :param data: NxM array with data.
    :type data: ndarray
    :param k: The number of medians to find: k < N.
    :type k: int
    :param f_cluster: Function x, y -> assignment. Assigns each data point to a centroid/median.
    :type f_cluster: function
    :param f_cluster_2: Function x, y -> votes. All the data points in y should vote which data point in x is closest.
    :type f_cluster_2: function
    :return: KxM array with medians and N-array with assignments
    :rtype: (ndarray, ndarray)
    """
    centroids = data[:k, :]
    initial_idx = np.random.permutation(np.arange(0, data.shape[0]) % k)
    assignments = np.vstack((initial_idx, np.ones(data.shape[0]) * 1e10)).T
    error, prev_error = None, None
    while error is None or prev_error is None or error < prev_error:
        prev_error = error
        for i in range(0, k):
            if np.sum(assignments[:, 0] == i) > 1:
                segment = data[assignments[:, 0] == i, :]
                if segment.shape[0] > 0:
                    votes = f_cluster_2(segment, segment)
                    best = max(np.unique(votes[:, 0]).tolist(), key=lambda x: np.sum(votes == x))
                    centroids[i, :] = segment[int(best), :]

        assignments = f_cluster(data, centroids)
        error = np.sum(assignments[:, 1]) / data.shape[0]
    return centroids, assignments[:, 0]


def shape(array, dim=0):
    """
    The shape of a array in a particular dimension.

    :param array: The array, could be anything.
    :type array: ndarray | object
    :param dim: The dimension on which the length should be measured.
    :type dim: int
    :return: The length of the dimension. If either the array is not a numpy array or the dimensionality is smaller
        than dim, 0 is returned.
    :rtype: int
    """
    if array is None:
        return 0
    if type(array) is np.ndarray:
        if len(array.shape) < dim:
            return 0
        else:
            return array.shape[dim]
    elif type(array) is list:
        return len(array)


def ind2vec(indices):
    """
    Transforms indices to a array where the i 'th element is 1 and the others are 0

    :param indices: Nx1 array with indexes, should be non-negative integers.
    :type indices: ndarray
    :return: NxM array with M-sized vectors. M is the maximum index value.
    :rtype: ndarray
    """
    vec = np.zeros((indices.shape[0], np.unique(indices).shape[0]))

    for i, index in enumerate(np.asarray(indices, np.int)):
        vec[i, index] = 1
    return vec