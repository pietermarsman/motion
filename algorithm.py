# coding=utf-8
"""
Algorithm for visual egomotion estimation.
"""
import threading
import time

from PyQt4 import QtCore
import scipy

from egomotion import Egomotion3D2D
from dataset import Kitti
# noinspection PyUnresolvedReferences
from following import KnnMatcher, VocabularyMatcher, BFMatcher, track, Matcher
from miscellaneous import shape
from sparse_features import ChessboardDetector, FeatureDetector
from camera import StereoCamera, Camera
from tracking import *
from projections import *


__author__ = 'Pieter Marsman'


class AlgorithmProperty(object):
    """
    Property that can be used in the algorithm and can be changed in the gui. Can be either a ranges of int or float
    or choices from a list of str.
    """
    # Calibration
    CHESSBOARD_SIZE = (9, 6)
    CALIBRATION_IMAGES = 40
    # Egomotion estimation
    EGOMOTION_ERROR = 0.002  # %
    EGOMOTION_MAX_DISTANCE = 500.0  # cm
    EGOMOTION_MAX_ROTATION = 0.25
    # Vocabulary
    VOCABULARY_FILTER_MAX_SPEED = 5.0
    VOCABULARY_BRANCH_FACTOR = 3
    # Tracking
    TRACKING_WINDOW = 21
    # Features
    GOOD_FEATURES_QUALITY = 0.005
    GOOD_FEATURES_DISTANCE = 5  # used during feature detection with goodFeaturesToTrack
    FEATURE_DISTANCE = 7  # used during adding features to the vocabulary
    # Point filtering
    VELOCITY_DECAY = 0.9
    MAX_STEREO_VERTICAL_DIFFERENCE = 0.01

    def __init__(self, name, value, min_value, max_value, step_size):
        """
        Initializes the value, bounds, step size and name for this property. If the type of step_size is int or
        float, the same type is saved and returned. The type of step_size can also be [str], in this case the user
        can choose from the different options and a str is returned.

        :param value: initial value
        :type value: float | int | str
        :param min_value: minimum value
        :type min_value: float | int | None
        :param max_value: maximum value
        :type max_value: float | int | None
        :param step_size: step size
        :type step_size: float | int | [str]
        """
        self.step = step_size
        self._value = value
        self.bounds = [min_value, max_value]
        self.name = name
        self.callbacks = []

    def __str__(self):
        return self.name + ': ' + str(self.value)

    def get_value(self):
        """
        :return value of the property. Is the same type as step_size
        :rtype int
        """
        if type(self.step) is int:
            return int(self._value)
        elif type(self.step) is float:
            return float(self._value)
        else:
            return self._value

    def set_value(self, value):
        """
        Sets the new value. The boundaries are checked and the callbacks are notified.

        :param value: The new value.
        :type value: int | float | str
        """
        if type(value) is int or type(value) is float:
            self._value = max(min(value, self.bounds[1]), self.bounds[0])
            self._value = round(self._value / self.step) * self.step
        elif type(value) is str:
            self._value = value
        for callback in self.callbacks:
            callback(self)

    def set_value_with_spinbox(self, spinbox):
        """
        Sets the new value using a spinbox. Used for setting numbers.

        :param spinbox: A spinbox from the pyqt window
        :type spinbox: pyqtgraph.SpinBox
        """
        self.value = spinbox.value()

    def set_value_with_combobox(self, combobox):
        """
        Sets the new value using a combobox. Used from choosing between different strings.

        :param combobox: A combobox from the pyqt window
        :type combobox: pyqtgraph.ComboBox
        """
        self.value = self.step[combobox]

    value = property(get_value, set_value)

    def subscribe(self, callback):
        """
        Subscribe to changes in this property. The callback function is called with the AlgorithmProperty as first
            argument when it changes.
        :param callback: A function that takes an AlgorithmProperty as first argument.
        :type callback: function
        """
        self.callbacks.append(callback)

    @staticmethod
    def get_properties(scene=6, width=640, blur=3, feed_start=0, alpha=0.5, stereo_match_quality=0.7,
                       time_match_quality=0.6, match_duration=0.1, max_projection_error=0.2, number_of_features=500,
                       vocabulary_size=500, point_trans_cov=5.0, point_obs_cov=0.1, pt_trans_speed=5.0,
                       output_points='not', track_max_error=10.0, track_min_eig=0.005, ego_trans_cov_multiplier=5.0,
                       ego_obs_cov_multiplier=1.0, feature_type='freak'):
        """
        :param ego_obs_cov_multiplier: The multiplication factor for the covariance of the egomotion observation
        :param ego_trans_cov_multiplier: The multiplication factor for the covariance of the egomotion prediction
        :param track_min_eig: The minimum eigenvalues of the optical flow field of the tracking window
        :param track_max_error: The maximum average difference between the pixels in the tracking window
        :param output_points: If the 'updated' or 'all' points should be displayed in the GUI
        :param point_obs_cov: The multiplication factor for the covariance in the observation of a keypoint in the
            image (range: [-1, 1])
        :param pt_trans_speed: The multiplication factor for the covariance of the speed of a point when predicting
            the next speed.
        :param point_trans_cov: The multiplication factor for the covariance of predicting the next point location
            using its velocity
        :param vocabulary_size: The maximum number of leafs in the vocabulary
        :param time_match_quality: The minimum difference in distance between the first and second match  when
            matching keypoints to the vocabulary
        :param stereo_match_quality: The minimum difference in distance between the first and second match when
            stereo matching
        :param scene: The scene that is used as input for the algorithm. 0 or 1 means the first two cameras,
            >1 means a scene from the kitti dataset.
        :param width: The width of the scene in pixels.
        :param blur: The size of the Gaussian blur applied to the lab image.
        :param feed_start: The number of the first frame when reading from file.
        :param alpha: The mixing coefficient for the weighted histogram equalization
        :param match_duration: The duration to keep points in the vocabulary. As long as they are in the vocabulary
            they can be matched. They can only be tracked if they were tracked in the last frame.
        :param max_projection_error: The maximum projection error of 3d point on an image.
        :param number_of_features: The number of features to detect in each frame.
        :param feature_type: The type of feature to use. Can be either 'freak', 'sift', 'surf', 'orb' or 'chessboard'
        :return: The default properties of the algorithm.
        :rtype: dict[str,AlgorithmProperty]
        """
        properties = dict()
        """:type: dict[str, AlgorithmProperty]"""
        properties['feed scene'] = AlgorithmProperty('feed scene', scene, 0, 22, 2)
        properties['feed width'] = AlgorithmProperty('feed width', width, 50, 1500, 50)
        properties['feed blur'] = AlgorithmProperty('feed blur', blur, 0, 31, 1)
        properties['feed start frame'] = AlgorithmProperty('feed start frame', feed_start, 0, 1000, 1)
        properties['feed equalize alpha'] = AlgorithmProperty('feed equalize alpha', alpha, 0.0, 1.0, 0.1)
        properties['time match quality'] = AlgorithmProperty('time match quality', time_match_quality, 0.1, 1.0, 0.1)
        properties['time match duration'] = AlgorithmProperty('time match duration', match_duration, 0.1, 1.0, 0.1)
        properties['stereo match quality'] = AlgorithmProperty('stereo match quality', stereo_match_quality, 0.1, 1.0,
                                                               0.1)
        properties['max projection error'] = AlgorithmProperty('max projection error', max_projection_error, 0.01, 0.5,
                                                               0.01)
        properties['number of features'] = AlgorithmProperty('number of features', number_of_features, 100, 1000, 10)
        properties['vocabulary size'] = AlgorithmProperty('vocabulary size', vocabulary_size, 1, 10000, 10)
        properties['pt obs cov'] = AlgorithmProperty('pt obs cov', point_obs_cov, 1e-10, 1e10, 0.00001)
        properties['pt trans cov'] = AlgorithmProperty('pt trans cov', point_trans_cov, 1e-10, 1e10, 0.1)
        properties['pt trans speed cov'] = AlgorithmProperty('pt trans speed cov', pt_trans_speed, 1e-10, 1e10, 0.1)
        output_options = ['all', 'updated', 'not']
        properties['output points'] = AlgorithmProperty('output points', output_points, None, None, output_options)
        properties['track max error'] = AlgorithmProperty('track max error', track_max_error, 0.0, 1e10, 0.5)
        properties['track min eig'] = AlgorithmProperty('track min eig', track_min_eig, 0.0, 100, 0.001)
        properties['egomotion trans cov'] = AlgorithmProperty('egomotion *trans cov', ego_trans_cov_multiplier, 1e-10,
                                                              1e10, 0.1)
        properties['egomotion obs cov'] = AlgorithmProperty('egomotion *obs cov', ego_obs_cov_multiplier, 1e-10, 1e10,
                                                            0.1)
        properties['feature type'] = AlgorithmProperty('feature type', feature_type, None, None,
                                                       ['freak', 'sift', 'surf', 'orb', 'chessboard'])
        return properties


class Algorithm(QtCore.QThread):
    """
    Continuous sequential execution of blocks of code. The algorithm follows keypoints in the image based on matching
        and tracking. The followed keypoints are triangulated to 3D space, and their velocity is inferred.
    """

    def __init__(self, properties):
        """
        Initializes a set of AlgorithmProperty, a StereoCamera, two SparseFeatureDetectors, a Egomotion detector,
        a Timer, a FeatureFollower and the Gui.
        :param properties: A set of properties.
        :type properties: dict[str, AlgorithmProperty]
        """
        super(Algorithm, self).__init__()
        # Containers set to None
        self.stereo = None
        """:type: StereoCamera"""
        self.features = None
        """:type: FeatureDetector"""
        self.time_matcher = None
        """:type: VocabularyMatcher"""
        self.egomotion = None
        """:type: Egomotion"""
        self.egomotion_tracker = None
        """:type: EgomotionTracker"""
        self.kitti = None
        """:type: Kitti"""
        self.pt_tracker = None
        """:type: PointTracker"""
        self.stereo_matcher = None
        """:type: Matcher"""

        # Listen to changes in properties
        self.properties = properties
        for prop in self.properties.values():
            prop.subscribe(self.property_changed)
        self.changed_properties = set()
        self.updating = False

        # Some containers for information that can be showed in the GUI
        self.prev_lab_l, self.prev_lab_r, self.prev_img_l, self.prev_img_r = None, None, None, None
        self.world_proj = np.eye(3, 4)
        self.intrinsic = None
        """:type: ndarray"""
        self.ex_r = None
        """:type: ndarray"""
        self.track_l1_error = []
        """:type: ndarray"""
        self.previous_data_save = 0
        self.track = []
        self.now = 0.0

        # Variable to stop the thread smoothly and wait until the updating is complete
        self.running = False

        # Initializes the classes that the algorithm needs
        self.init_algorithm()

    def property_changed(self, prop):
        """
        Saves the changed properties in a set.

        :param prop: The property that has been changed.
        :type prop: AlgorithmProperty
        """
        # Wait until the algorithm is no longer updating
        while self.updating:
            time.sleep(0.01)
        # Add the property to the set
        self.changed_properties.add(prop)

    def update_properties(self):
        """
        Updates the algorithm for each changed property
        """
        self.updating = True
        for prop in self.changed_properties:
            self.emit(QtCore.SIGNAL('show_text'), str(prop))
            self.init_algorithm(prop)
        self.changed_properties = set()
        self.updating = False

    def init_algorithm(self, changed_property=None):
        """
        Initializes the algorithm

        :param changed_property: Property that is changed by the gui. None means the algorithm is initialized for the
            first time, else it is the AlgorithmProperty that has changed and the algorithm can be changed accordingly.
        :type changed_property: AlgorithmProperty
        """

        def init_features():
            """
            Initializes the feature detectors and describers

            :raise Exception: If an unsupported stereo matcher is chosen
            """
            if feature_type in ['sift', 'surf', 'freak', 'orb']:
                detector = 'good' if feature_type is 'freak' else feature_type
                self.features = FeatureDetector(detector=detector, descriptor=feature_type,
                                                amount=self.properties['number of features'].value)
            elif feature_type is 'chessboard':
                self.features = ChessboardDetector(AlgorithmProperty.CHESSBOARD_SIZE)
            else:
                raise Exception('Unsupported stereo matcher chosen: ' + self._feature_type)

        def init_time_matching():
            """
            Initializes the time matches. Could be either initialized as a vocabulary tree or knn matcher. The
                distance measure depends on the feature type.

            :raise Exception:
            """
            if feature_type in ['sift', 'surf']:
                self.time_matcher = VocabularyMatcher(2, 'rmse',
                                                      branch_factor=AlgorithmProperty.VOCABULARY_BRANCH_FACTOR,
                                                      match_quality=self.properties['time match quality'].value)
            elif feature_type in ['freak']:
                self.time_matcher = VocabularyMatcher(2, 'hamming',
                                                      branch_factor=AlgorithmProperty.VOCABULARY_BRANCH_FACTOR,
                                                      match_quality=self.properties['time match quality'].value,
                                                      load_tree='model/tree/depth_3/')
            elif feature_type in ['orb']:
                self.time_matcher = VocabularyMatcher(2, 'hamming',
                                                      branch_factor=AlgorithmProperty.VOCABULARY_BRANCH_FACTOR,
                                                      match_quality=self.properties['time match quality'].value)
            elif feature_type is 'chessboard':
                self.time_matcher = VocabularyMatcher(2, 'rmse', match_quality=1.0)
            else:
                raise Exception('Unsupported stereo matcher chosen: ' + feature_type)

        def init_stereo_matching():
            """
            Initializes the stereo matchers. Could be either a knn matcher of brute force matcher.

            :raise Exception:
            """
            if feature_type in ['sift', 'surf']:
                self.stereo_matcher = KnnMatcher(self.properties['stereo match quality'].value)
            elif feature_type in ['freak', 'orb', 'chessboard']:
                self.stereo_matcher = BFMatcher(cv2.NORM_HAMMING, self.properties['stereo match quality'].value)
            else:
                raise Exception('Unsupported stereo matcher chosen: ' + feature_type)

        def init_stereo_camera():
            """
            Initializes the stereo camera feed. The feed could have an origin from real camera's or from the kitti
                dataset on the hard drive. The camera's are calibrated (from file).
            """
            args = (self.properties['feed width'].value, self.properties['feed blur'].value,
                    self.properties['feed equalize alpha'].value, self.properties['feed start frame'].value)
            cam_l = Camera.get_default_camera(self.properties['feed scene'].value, *args)
            cam_r = Camera.get_default_camera(self.properties['feed scene'].value + 1, *args)
            self.stereo = StereoCamera(cam_l, cam_r)
            self.stereo.calibrate(True, True, number_of_images=AlgorithmProperty.CALIBRATION_IMAGES)
            self.intrinsic, _ = self.stereo.cam['left'].get_calibration_parameters()

        feature_type = self.properties['feature type'].value
        # Get the name of the property
        if changed_property is None:
            changed_property_name = None
        else:
            changed_property_name = changed_property.name
        # Only the first time
        if changed_property_name is None:
            self.egomotion = Egomotion3D2D()
        if changed_property in [None, 'feature type']:
            init_features()
        # Change the tracking of points
        if changed_property_name in [None, 'pt trans cov', 'pt obs cov', 'pt trans speed cov']:
            self.pt_tracker = PointTracker(self.properties['pt trans cov'].value, self.properties['pt obs cov'].value,
                                           self.properties['pt trans speed cov'].value)
        # Change in the camera feed. Also initialize the egomotion, tracker and kitti.
        if changed_property_name in [None, 'feed scene', 'feed width', 'feed blur', 'feed equalize alpha',
                                     'feed start frame']:
            if self.stereo is not None:
                self.stereo.release()
            init_stereo_camera()
            self.egomotion.set_intrinsic(self.intrinsic)
            self.egomotion.set_distortion(np.zeros(5))
            self.egomotion_tracker = EgomotionTracker(self.properties['egomotion trans cov'].value,
                                                      self.properties['egomotion obs cov'].value)
            self.prev_lab_l, self.prev_lab_r, self.prev_img_l, self.prev_img_r = None, None, None, None
            start_frame = self.properties['feed start frame'].value
            self.kitti = Kitti(self.stereo.cam['left'].video_capture_source, start_frame=start_frame)
            self.kitti.retrieve_data()
            self.emit(QtCore.SIGNAL('clear track'))
        # Change the feature detector
        if changed_property_name in [None, 'number of features']:
            self.features.number_of_features = self.properties['number of features'].value
        # Change the time matcher
        if changed_property_name in [None, 'branching factor', 'time match quality', 'feed start frame', 'feed scene',
                                     'feed width', 'feature type']:
            init_time_matching()
        # Change the stereo matcher
        if changed_property_name in [None, 'stereo match quality', 'feature type']:
            init_stereo_matching()

    def run(self):
        """
        Continuous reading, processing and showing images.
        """

        # Initialize the projection matrices
        _, self.ex_r = self.stereo.get_extrinsic()

        self.running = True

        # Continuously do this
        while self.running:

            # Apply the new settings
            self.update_properties()

            # Update kitti
            self.kitti.do_step()

            # Get images, keypoints and their descriptions
            (img_l, lab_l), (img_r, lab_r) = self.stereo.get_stereo_image()
            kp_l, desc_l, kp_r, desc_r = self.detect_features(lab_l, lab_r)

            if shape(kp_l, 0) > 0 and shape(kp_r, 0) > 0:
                # Match features with history
                ret = self.match_points(desc_l, desc_r, kp_l, kp_r, self.world_proj, self.intrinsic)
                pts_3d22d, idx_3d22d, pts_3d2d_l, idx_3d2d_l, pts_3d2d_r, idx_3d2d_r, pts_22d, idx_22d = ret

                # Track points
                pts_2d_l, idx_2d_l, pts_2d_r, idx_2d_r = self.track_points(self.prev_lab_l, self.prev_lab_r, lab_l,
                                                                           lab_r)

                # Fuse matcher and tracker information
                pts_3d22d, idx_3d22d, status = self.combine_match_track(pts_3d2d_l, idx_3d2d_l, pts_3d2d_r, idx_3d2d_r,
                                                                        pts_2d_l, idx_2d_l, pts_2d_r, idx_2d_r)

                # Find position
                my_position, my_rotation, num_inliers = self.detect_position(pts_3d22d, self.ex_r)
                self.world_proj = self.update_position(my_rotation, my_position, num_inliers)

                # Update 3D location/information with 3D->3D matches
                self.update_3d3d_matches(pts_3d22d, idx_3d22d, self.world_proj, self.ex_r)

                # Add unmatched 3D to vocabulary tree with initial state
                if idx_22d.shape[0] > 0:
                    self.add_unmatched(pts_22d, desc_l[idx_22d[:, 0]], self.world_proj, self.ex_r)

            # Update moving points
            self.update_states()

            # Remove unwanted nodes from vocabulary tree
            self.time_matcher.remove_old_node(self.now, self.properties['time match duration'].value)

            # Save images for tracking purposes
            self.prev_lab_l, self.prev_lab_r, self.prev_img_l, self.prev_img_r = lab_l, lab_r, img_l, img_r

            # Show output
            self.emit(QtCore.SIGNAL('update'))

            # Update time
            self.now += 0.1

    def detect_features(self, lab0, lab1):
        """
        Detects features in the two L*a*b images and describes them
        :param lab0: First L*a*b image
        :param lab1: Second L*a*b image
        :return: The first keypoints, first descriptions, second keypoints, second descriptions
        :rtype: (ndarray, ndarray, ndarray, ndarray)
        """
        kp_l, desc_l = self.features.detect_and_describe(lab0[:, :, 0])
        kp_r, desc_r = self.features.detect_and_describe(lab1[:, :, 0])
        return kp_l, desc_l, kp_r, desc_r

    def match_points(self, desc_l, desc_r, kp_left, kp_right, world_proj, intrinsic):
        """
        Matches descriptions a history of descriptions saved in a vocabulary tree
        :param desc_l: NxM array with descriptions of keypoints in the left frame
        :type desc_l: ndarray
        :param desc_r: N'xM array with descriptions of keypoints in the right frame
        :type desc_r: ndarray
        :param kp_left: The real locations of keypoints detected in the left frame
        :type kp_left: ndarray
        :param kp_right: The real location of keypoints detected in the right frame
        :type kp_right: ndarray
        :param world_proj: The projection of points from the current frame to the world frame. Used together with the
        intrinsic values to test if 3d points can be seen.
        :type world_proj: ndarray
        :param intrinsic: The intrinsic camera parameters. Used together with the world projection to test if 3d
        points can be seen.
        :type intrinsic: ndarray
        :return: Mapping of indexes for 3d3d correspondences (id_left, id_right, id_history), 3d2d correspondences to
        the left frame (id_left, id_history), 3d2d correspondences to the right frame (id_right, id_history) and 3d
        matches between the left and right frame that do not correspond to descriptions in the history (id_left,
        id_right).
        :rtype: tuple of ndarray
        """
        # Match features with vocabulary tree
        normalize_proj = np.diag([intrinsic[0, 0] / intrinsic[0, -1], intrinsic[1, 1] / intrinsic[1, -1], 1])
        history_filter = Algorithm.vocabulary_space_filter(normalize_proj, inverse(world_proj))
        matches_history_l = self.time_matcher.match(desc_l, None, f=history_filter)
        matches_history_r = self.time_matcher.match(desc_r, None, f=history_filter)

        # Match features with each other
        matches_lr = self.stereo_matcher.match(desc_l, desc_r)

        # Refine matches based on second nearest neighbour
        refined_lr = self.stereo_matcher.refine_match2(matches_lr)
        refined_history_l = self.time_matcher.refine_match2(matches_history_l)
        refined_history_r = self.time_matcher.refine_match2(matches_history_r)

        # Split in unmatched 3D, unmatched 2D, 3D->3D, 3D->2D
        idx_3d2d_l = refined_history_l
        idx_3d2d_r = refined_history_r
        idx_3d3d_l = []
        if shape(refined_history_l) > 0:
            for (id_l, id_r) in refined_lr:
                ind = np.where(refined_history_l[:, 0] == id_l)[0]
                if ind.shape[0] > 0:
                    idx_3d3d_l.append([id_l, id_r, refined_history_l[ind, 1]])
            idx_3d3d_l = np.array(idx_3d3d_l)
            idx_22d_left = np.array([x for x in refined_lr if x[0] not in refined_history_l[:, 0]])
        else:
            idx_3d3d_l = np.array([])
            idx_22d_left = refined_lr

        # Get the points that belong to the indexes
        pts_3d3d, pts_3d2d_l, pts_3d2d_r, pts_22d = None, None, None, None
        idx_3d3d, idx_3d_l, idx_3d_r = None, None, None
        # Get the 3d->3d matches
        if idx_3d3d_l.shape[0] > 0:
            idx_3d3d = idx_3d3d_l[:, -1]
            pts_3d3d = np.zeros((idx_3d3d_l.shape[0], 7))
            pts_3d3d[:, :3] = self.time_matcher.get_information('mean', idx_3d3d)[:, :3]
            pts_3d3d[:, 3:5] = kp_left[idx_3d3d_l[:, 0], :2]
            pts_3d3d[:, 5:7] = kp_right[idx_3d3d_l[:, 1], :2]
        # Get the 3d -> left matches
        if idx_3d2d_l is not None and idx_3d2d_l.shape[0] > 0:
            idx_3d_l = idx_3d2d_l[:, -1]
            pts_3d2d_l = np.zeros((idx_3d2d_l.shape[0], 5))
            pts_3d2d_l[:, :3] = self.time_matcher.get_information('mean', idx_3d_l)[:, :3]
            pts_3d2d_l[:, 3:] = kp_left[idx_3d2d_l[:, 0], :2]
        # Get the 3d -> right matches
        if idx_3d2d_r is not None and idx_3d2d_r.shape[0] > 0:
            idx_3d_r = idx_3d2d_r[:, -1]
            pts_3d2d_r = np.zeros((idx_3d2d_r.shape[0], 5))
            pts_3d2d_r[:, :3] = self.time_matcher.get_information('mean', idx_3d_r)[:, :3]
            pts_3d2d_r[:, 3:] = kp_right[idx_3d2d_r[:, 0], :2]
        # Get the left <-> right matches
        if idx_22d_left is not None and idx_22d_left.shape[0] > 0:
            pts_22d = np.zeros((idx_22d_left.shape[0], 4))
            pts_22d[:, :2] = kp_left[idx_22d_left[:, 0], :2]
            pts_22d[:, 2:] = kp_right[idx_22d_left[:, 1], :2]
        return pts_3d3d, idx_3d3d, pts_3d2d_l, idx_3d_l, pts_3d2d_r, idx_3d_r, pts_22d, idx_22d_left

    def track_points(self, lab_left0, lab_right0, lab_left1, lab_right1):
        """
        Track points from the one image to the next. The location of the keypoints are retrieved from the vocabulary.

        :param lab_left0: The left current image in L*a*b representation
        :type lab_left0: ndarray
        :param lab_right0: The right current image in L*a*b representation
        :type lab_right0: ndarray
        :param lab_left1: The left next image in L*a*b representation
        :type lab_left1: ndarray
        :param lab_right1: The right next image in L*a*b representation
        :type lab_right1: ndarray
        :return: The location of the keypoints in the next frame and their indexes in the vocabulary for both the
        left and right frame: (pts_l, idx_l, pts_r, idx_r).
        :rtype: (ndarray, ndarray, ndarray, ndarray) | (None, None, None, None)
        """
        # Get all the points from the vocabulary
        last_obs, indexes = self.time_matcher.get_information('obs', None)
        if last_obs.shape[0] > 0 and lab_left0 is not None:
            # Only use points that were detected in the previous frame
            last_seen = self.time_matcher.get_information('time', indexes)
            last_updated = self.time_matcher.get_information('updated', indexes)
            trackable = last_seen == last_updated
            if np.any(trackable):
                last_obs = last_obs[trackable, :]
                indexes = np.array(indexes)[trackable]
                # Track the points
                max_error = self.properties['track max error'].value
                min_eig = self.properties['track min eig'].value
                win_size = AlgorithmProperty.TRACKING_WINDOW
                p_2d_left, idx_left, errors_left = track(last_obs[:, :2], lab_left0[:, :, 0], lab_left1[:, :, 0],
                                                         win_size=win_size, max_error=max_error, min_eig=min_eig)
                p_2d_right, idx_right, errors_right = track(last_obs[:, 2:], lab_right0[:, :, 0], lab_right1[:, :, 0],
                                                            win_size=win_size, max_error=max_error, min_eig=min_eig)
                self.track_l1_error.extend(np.hstack((errors_left, errors_right)).tolist())
                return p_2d_left, np.array(indexes)[idx_left], p_2d_right, np.array(indexes)[idx_right]
        return None, None, None, None

    def combine_match_track(self, points_3d2d_left_match, idx_3d2d_left_match, points_3d2d_right_match,
                            idx_3d2d_right_match, points_2d_left_tracked, idx_3d2d_left_track, points_2d_right_tracked,
                            idx_3d2d_right_track):
        """
        Combine the keypoints that were matched and tracked to the current frame. Returns the 3D, 2D-left and
            2D-right location of keypoints that were tracked to both the left and right frame.

        :param points_3d2d_left_match: Nx5 array with the 3D and 2D-left locations of points that were matched.
        :type points_3d2d_left_match: ndarray
        :param idx_3d2d_left_match: N-array with the vocabulary indexes of the matched keypoints to the left
        frame
        :type idx_3d2d_left_match: ndarray
        :param points_3d2d_right_match: Mx5 array with the 3D and 2D-right locations of points that were matched to
        the right frame.
        :type points_3d2d_right_match: ndarray
        :param idx_3d2d_right_match: M-array with vocabulary indexes of the matched keypoints to the right frame
        :type idx_3d2d_right_match: ndarray
        :param points_2d_left_tracked: Kx2 array with the 2D keypoint location in the current left frame
        :type points_2d_left_tracked: ndarray
        :param idx_3d2d_left_track: K-array with the vocabulary indexes of the tracked keypoints
        :type idx_3d2d_left_track: ndarray
        :param points_2d_right_tracked: Lx2 array with the 2D keypoint location in the current right frame
        :type points_2d_right_tracked: ndarray
        :param idx_3d2d_right_track: L-array with the vocabulary indexes of the tracked keypoints
        :type idx_3d2d_right_track: ndarray
        :return: The 3D coordinates and 2D coordinates in the left and right frame. Also the vocabulary indexes of
        these matches. Lastly, the status of the matches, if they are tracked (=1) or matched(=2).
        :rtype: (ndarray, ndarray, ndarray) | (None, None, None)
        """
        idx_left = np.array([])
        idx_right = np.array([])
        if idx_3d2d_left_match is not None:
            idx_left = np.concatenate((idx_left, idx_3d2d_left_match))
        if idx_3d2d_left_track is not None:
            idx_left = np.concatenate((idx_left, idx_3d2d_left_track))
        if idx_3d2d_right_match is not None:
            idx_right = np.concatenate((idx_right, idx_3d2d_right_match))
        if idx_3d2d_right_track is not None:
            idx_right = np.concatenate((idx_right, idx_3d2d_right_track))
        if idx_left.shape[0] > 0 and idx_right.shape[0] > 0:
            idx_3d3d = np.intersect1d(idx_left, idx_right)
            if idx_3d3d.shape[0] > 0:
                points_3d22d = np.zeros((idx_3d3d.shape[0], 7))
                # Add the previous location of the point to the history
                points_3d22d[:, :3] = self.time_matcher.get_information('mean', idx_3d3d)[:, :3]
                status = np.zeros((idx_3d3d.shape[0], 2))
                for i, id_3d3d in enumerate(idx_3d3d):
                    # Add the left keypoints from either the match or the tracker
                    if idx_3d2d_left_match is not None and id_3d3d in idx_3d2d_left_match:
                        keypoint_id = np.where(idx_3d2d_left_match == id_3d3d)[0][0]
                        points_3d22d[i, 3:5] = points_3d2d_left_match[keypoint_id, 3:]
                        status[i, 0] = 2
                    elif idx_3d2d_left_track is not None:
                        keypoint_id = np.where(idx_3d2d_left_track == id_3d3d)[0]
                        points_3d22d[i, 3:5] = points_2d_left_tracked[keypoint_id, :]
                        status[i, 0] = 1
                    # Add the right keypoints from either the match or the tracker
                    if idx_3d2d_right_match is not None and id_3d3d in idx_3d2d_right_match:
                        keypoint_id = np.where(idx_3d2d_right_match == id_3d3d)[0][0]
                        points_3d22d[i, 5:] = points_3d2d_right_match[keypoint_id, 3:]
                        status[i, 1] = 2
                    elif idx_3d2d_right_track is not None:
                        keypoint_id = np.where(idx_3d2d_right_track == id_3d3d)[0]
                        points_3d22d[i, 5:] = points_2d_right_tracked[keypoint_id, :]
                        status[i, 1] = 1
                return points_3d22d, idx_3d3d, status
        return None, None, None

    def detect_position(self, points_3d22d, ex_r):
        """
        Uses both projections to the left and right camera to get a weighted average of the egomotion
        :param points_3d2d_left: Coordinates of a matched 3D point with a 2D point in the left frame
        :type points_3d2d_left: ndarray
        :param points_3d2d_right: Coordinates of a matched 3D point with a 2D point in the right frame
        :type points_3d2d_right: ndarray
        :param ex_r: The extrinsic relation between the right and left camera
        :type ex_r: ndarray
        :return: The translation, rotation and total number of inliers
        :rtype: (ndarray, ndarray, int)
        """
        current_projection = self.world_proj
        if points_3d22d is not None:
            # Find the egomotion based on point correspondences
            reprojection_error = self.properties['feed width'].value * AlgorithmProperty.EGOMOTION_ERROR
            args = (points_3d22d[:, :3], points_3d22d[:, 3:5], reprojection_error, current_projection)
            rot_left, trans_left, inliers_left = self.egomotion.find_egomotion(*args)
            args = (points_3d22d[:, :3], points_3d22d[:, 5:7], reprojection_error, current_projection)
            rot_right, trans_right, inliers_right = self.egomotion.find_egomotion(*args)
            # Give a numerical value to the number of inliers
            inliers_left = inliers_left.shape[0] if inliers_left is not None else 0.0
            inlier_right = inliers_right.shape[0] if inliers_right is not None else 0.0
            # Combine the rotation and translation to get the projections
            projection_left = inverse(compose(rot_left, trans_left))
            projection_right = combine(inverse(compose(rot_right, trans_right)), ex_r)
            # Translate back to euler angles and translations of the left camera
            euler_left, trans_left = from_projection(projection_left)
            euler_right, trans_right = from_projection(projection_right)
            # Create an average based on the number of inliers of both estimations
            sum_inlier = inliers_left + inlier_right
            if sum_inlier > 0:
                euler = (euler_left * inliers_left + euler_right * inlier_right) / sum_inlier
                trans = (trans_left * inliers_left + trans_right * inlier_right) / sum_inlier
                return trans, euler, sum_inlier
        return np.zeros(3), np.zeros(3), 0

    def update_position(self, rotation, translation, inliers):
        pred_euler, pred_translation = from_projection(self.egomotion_tracker.predict())
        if Algorithm.accept_egomotion(rotation, pred_euler, translation, pred_translation, inliers):
            self.egomotion_tracker.update(rotation, translation, inliers)
        else:
            self.egomotion_tracker.update(None, None, None)
        # using np.array() removes some weird bug. Computations using this return value without np.array take about 3
        #  times longer
        return np.array(self.egomotion_tracker.to_projection())

    def update_3d3d_matches(self, points_3d22d, idx_3d22d, projection, ex_r):
        """
        Updates the 3d point from the vocabulary with a match to both left and right frame.
        :param points_3d22d: Nx7 array with the 3D and two 2D coordinates of matched (key)points.
        :type points_3d22d: ndarray
        :param idx_3d22d: N-array with the vocabulary indexes of the matches
        :type idx_3d22d: ndarray
        :param projection: 3x4 array with the projection from the world reference frame to the current reference frame
        :type projection: ndarray
        :param ex_r: 3x4 array with the extrinsic relation between the right and left camera
        :type ex_r: ndarray
        """
        if points_3d22d is not None and points_3d22d.shape[0] > 0:
            normalized_left = self.stereo.cam['left'].normalize_keypoints(points_3d22d[:, 3:5])
            normalized_right = self.stereo.cam['right'].normalize_keypoints(points_3d22d[:, 5:7])
            max_error = self.properties['max projection error'].value
            indexes_used = []
            # Get the projection from the world points to undistorted camera points
            # projection_inv = inverse(projection)
            intrinsic, _ = self.stereo.cam['left'].get_calibration_parameters()
            intrinsic[[0, 1], [0, 1]] /= intrinsic[:2, -1]
            intrinsic[:2, -1] = 0.0
            # For each 3d3d match
            for i, ind in enumerate(idx_3d22d):
                # Get information from the tree
                mean = self.time_matcher.get_information('mean', [ind])[0]
                cov = self.time_matcher.get_information('cov', [ind])[0]
                prev_time = self.time_matcher.get_information('time', [ind])[0]
                # Get information on the keypoints
                normalized_obs = np.hstack((normalized_left[i, :], normalized_right[i, :]))
                # If the new information fits the old information update the point
                if Algorithm.accept_update_point(normalized_obs, mean[:3], inverse(projection), max_error):
                    new_mean, new_cov = self.pt_tracker.update(mean, cov, normalized_obs, self.now - prev_time,
                                                               projection, ex_r)
                    self.time_matcher.vocabulary.information[ind]['mean'] = new_mean
                    self.time_matcher.vocabulary.information[ind]['cov'] = new_cov
                    self.time_matcher.vocabulary.information[ind]['time'] = self.now
                    self.time_matcher.vocabulary.information[ind]['updated'] = self.now
                    self.time_matcher.vocabulary.information[ind]['track'][self.now] = (np.array(new_mean[:3]))
                    obs_y = 0.5 * (points_3d22d[i, 4] + points_3d22d[i, 6])
                    obs = np.hstack((points_3d22d[i, 3], obs_y, points_3d22d[i, 5], obs_y))
                    self.time_matcher.vocabulary.information[ind]['debug'].append(obs)
                    self.time_matcher.vocabulary.information[ind]['obs'] = obs
                    indexes_used.append(i)
            return indexes_used
        return []

    def add_unmatched(self, points_22d, desc_left, projection, ex_r):
        """
        Add keypoints that are not matched to the vocabulary but are matched from left to right to the vocabulary
        :param points_22d: Nx4 array with the 2D coordinates in the left and right frame of a stereo match.
        :type points_22d: ndarray
        :param desc_left: NxM array with the descriptors of the matches
        :type desc_left: ndarray
        :param projection: 3x4 array with the projection from the world reference frame to the current reference frame
        :type projection: ndarray
        :param ex_r: 3x4 array with the projection from the right to the left camera reference frame
        :type ex_r: ndarray
        """
        idx_used = []
        undistorted_22d = np.hstack((self.stereo.cam['left'].normalize_keypoints(points_22d[:, :2]),
                                     self.stereo.cam['right'].normalize_keypoints(points_22d[:, 2:])))
        if undistorted_22d is not None and undistorted_22d.shape[0] > 0:
            idx_left = range(undistorted_22d.shape[0])
            while len(idx_left) > 0 and self.time_matcher.size() < self.properties['vocabulary size'].value:
                prev_obs = self.time_matcher.get_information('obs')[0]
                if len(prev_obs.shape) > 1:
                    # noinspection PyUnresolvedReferences
                    distance = scipy.spatial.distance.cdist(prev_obs[:, :2], points_22d[idx_left, :2])
                    distance = np.min(distance, axis=0)
                    id_max_distance = np.argmax(distance)
                    i = idx_left[id_max_distance]
                    # If the feature is close to already existing features
                    if distance[id_max_distance] < AlgorithmProperty.FEATURE_DISTANCE:
                        break
                else:
                    i = idx_left[0]
                undistorted_obs = undistorted_22d[i, :]
                if Algorithm.accept_new_point(undistorted_obs[:2], undistorted_obs[2:]):
                    mean, cov = self.pt_tracker.update(None, None, undistorted_obs, 0.0, projection, ex_r)
                    tracks = dict()
                    tracks[self.now] = np.array(mean[:3])
                    obs_y = 0.5 * (points_22d[i, 1] + points_22d[i, 3])
                    obs = np.hstack((points_22d[i, 0], obs_y, points_22d[i, 2], obs_y))
                    # noinspection PyUnresolvedReferences
                    color = np.random.randint(0, 255, 3)
                    info = [
                        dict(mean=mean, cov=cov, time=self.now, updated=self.now, track=tracks, obs=obs, debug=[obs],
                             color=color)]
                    desc = desc_left[i, :].reshape((1, -1))
                    self.time_matcher.add_features(info, desc)
                    idx_used.append(i)
                idx_left.remove(i)
        return idx_used

    def update_states(self):
        """
        Update the moving points in the vocabulary using their velocity.
        """
        for key in self.time_matcher.vocabulary.information:
            values = self.time_matcher.vocabulary.information[key]
            if values['time'] < self.now:
                mean, cov = self.pt_tracker.update(values['mean'], values['cov'], None, self.now - values['time'], None,
                                                   1)
                self.time_matcher.vocabulary.information[key]['mean'] = mean
                self.time_matcher.vocabulary.information[key]['cov'] = cov
                self.time_matcher.vocabulary.information[key]['time'] = self.now

    def save_data(self, anyway=False):
        self.track.append(self.world_proj)
        if self.previous_data_save + 60.0 < time.time() or anyway:
            name = "%02i" % (self.properties['feed scene'].value / 2 - 1)
            print 'Feed', name, 'is at', int(round(self.now * 10))
            self.previous_data_save = time.time()
            data = np.array(self.track)
            data[:, :, -1] *= 0.01
            data = data.reshape((-1, 12))
            np.savetxt('data/path/' + name + '.txt', data, '%f', ' ')

    # noinspection PyUnresolvedReferences
    @staticmethod
    def accept_egomotion(euler_obs, euler_pred, position_obs, position_pred, inliers):
        """
         Method that judges if a egomotion observation fits the old egomotion. If the difference is to big it is
         considered as an outlier.
        :param euler_obs: The observed rotation angles of the camera
        :type euler_obs: ndarray
        :param euler_pred: The predicted rotation angles fo the camera
        :type euler_pred: ndarray
        :param position_obs: The observed position of the camera
        :type position_obs: ndarray
        :param position_pred: The predicted position of the camera
        :type position_pred: ndarray
        :return: If the observation is accepted
        :rtype: bool
        """
        distance = np.sqrt(np.sum((position_obs - position_pred) ** 2))
        rotation = np.linalg.norm(rodrigues_to_filterable(euler_obs) - rodrigues_to_filterable(euler_pred), 1)
        accept_distance = distance < AlgorithmProperty.EGOMOTION_MAX_DISTANCE
        accept_rotation = rotation < AlgorithmProperty.EGOMOTION_MAX_ROTATION
        accept_inliers = inliers > 3 if inliers is not None else False
        return accept_distance and accept_rotation and accept_inliers

    @staticmethod
    def accept_update_point(obs_unit_kps, position_3d, projection, max_reprojection_error):
        """
        Judges if the previous points fits the new observation of that point. It fits if the reprojection error is
        smaller than the reprojection error threshold.
        :param obs_unit_kps: The observed location of the feature point in the image frame
        :type obs_unit_kps: ndarray
        :param position_3d: The position of the 3d point in the world frame
        :type position_3d: ndarray
        :param projection: The projection from the world frame to the camera
        :type projection: ndarray
        :param max_reprojection_error: The maximum reprojection error
        :type max_reprojection_error: float
        :return: If the observation fits the point
        :rtype: bool
        """
        predicted_p3 = projection.dot(to_homogeneous(position_3d))
        if predicted_p3[-1] < 0.0:
            return False
        else:
            predicted_p2 = from_homogeneous(predicted_p3)
            projection_distance = np.sqrt(np.sum((obs_unit_kps[:2] - predicted_p2) ** 2))
            # noinspection PyUnresolvedReferences
            vertical_distance = np.abs(obs_unit_kps[1] - obs_unit_kps[3])
            horizontal_distance = obs_unit_kps[0] - obs_unit_kps[2]
            ret = projection_distance < max_reprojection_error and vertical_distance < \
                                                                   AlgorithmProperty.MAX_STEREO_VERTICAL_DIFFERENCE \
                  and horizontal_distance > 0.
            return ret

    @staticmethod
    def accept_new_point(left_image_position, right_image_position, x_diff_threshold=0.0, y_diff_threshold=0.1):
        """
        Judges if a new match is good enough for triangulation. Uses the difference between the x and y coordinates.
        :param left_image_position: Undistorted keypoint in the left camera frame
        :type left_image_position: ndarray
        :param right_image_position: Undistorted keypoint in the right camera frame
        :type right_image_position: ndarray
        :param x_diff_threshold: The minimum difference in the x coordinate
        :type x_diff_threshold: float
        :param y_diff_threshold: The maximum difference in the y coordinate
        :type y_diff_threshold: float
        :return: If the point is suitable from triangulation
        :rtype: bool
        """
        h_dist = left_image_position[0] - right_image_position[0]
        v_dist = left_image_position[1] - right_image_position[1]
        return h_dist > x_diff_threshold and abs(v_dist) < y_diff_threshold

    @staticmethod
    def vocabulary_space_filter(norm_intrinsic, projection):
        """
        A filter for a search in the vocabulary based on it's current projection on the image plane and it's speed.

        :param norm_intrinsic: 3x3 array with the normalized intrinsic values.
        :type norm_intrinsic: ndarray
        :param projection: 3x4 array with the projection from the world reference frame to the current reference frame.
        :type projection: ndarray
        :return: A function that can filter the vocabulary based on its information (projected on the image and low
            speed)
        :rtype: function
        """

        def check_projection_and_speed(info):
            """
            Check if the information of a node is suitable for matching and egomotion estimation. The point should be
                projected inside the image plane and it's speed should be low.

            :param info: Dictionary with information about the point.
            :type info: dict[str, list | ndarray | float]
            :return: If the point is suitable for matching and egomotion estimation
            :rtype: bool
            """
            proj = from_homogeneous(norm_intrinsic.dot(projection.dot(to_homogeneous(info['mean'][:3]))))
            speed = np.linalg.norm(info['mean'][3:], 2)
            # noinspection PyUnresolvedReferences
            accept_projection = np.all(np.abs(proj < 1.0))
            accept_speed = speed < AlgorithmProperty.VOCABULARY_FILTER_MAX_SPEED
            return accept_projection and accept_speed

        return check_projection_and_speed