# coding=utf-8
"""
Testing, performance and random code
"""
import csv
import random
import time

import scipy.stats
import scipy.spatial
import scipy.optimize
from pykalman import AdditiveUnscentedKalmanFilter
from sklearn.metrics import roc_curve, roc_auc_score
from traits.tests.test_trait_cycle import TestCase
from numpy import testing
from matplotlib.artist import setp

from miscellaneous import *
from dataset import Hopkins, Trajectories
from miscellaneous import create_directory, ind2vec
from rigid_motion import SSC
from sparse_features import FeatureDetector
from camera import Camera, StereoCamera
from tracking import PointTracker, EgomotionTracker
from vocabulary_tree import DynamicVocabularyTree, FixedVocabularyTree
from projections import *


__author__ = 'Pieter Marsman'


class TestFeatures(TestCase):
    def setUp(self):
        cam = Camera.get_default_camera(2, 500, 0, 0.5)
        _, lab = cam.get_image()
        self.img = lab[:, :, 0]

    def feature_equal_kp_desc(self, feature_type):
        feature = FeatureDetector(detector=feature_type)
        kp, desc = feature.detect_and_describe(self.img)
        self.assertEqual(kp.shape[0], desc.shape[0])

    def feature_has_features(self, feature_type):
        feature = FeatureDetector(detector=feature_type)
        kp, desc = feature.detect_and_describe(self.img)
        self.assertGreater(kp.shape[0], 0)

    def test_surf_equal_kp_desc(self):
        self.feature_equal_kp_desc('surf')

    def test_surf_has_features(self):
        self.feature_has_features('surf')

    def test_sift_equal_kp_desc(self):
        self.feature_equal_kp_desc('sift')

    def test_sift_has_features(self):
        self.feature_has_features('sift')


# noinspection PyUnresolvedReferences,PyShadowingNames
class TestCamera(TestCase):
    def setUp(self):
        self.cam_l = Camera.get_default_camera(2, 640, 0, 0.5)
        self.cam_r = Camera.get_default_camera(3, 640, 0, 0.5)
        self.cam = StereoCamera(self.cam_l, self.cam_r)
        self.cam.calibrate(True, True)

    def test_get_image(self):
        img, lab = self.cam_l.get_image()
        self.assertTrue(img is not None and lab is not None)
        self.assertTrue(img.shape[0] * img.shape[1] * lab.shape[0] * lab.shape[1] > 0)

    def test_get_extrinsic(self):
        p1, p2 = self.cam.get_extrinsic()
        self.assertTrue(p1 is not None and p2 is not None)
        self.assertTrue(np.prod(p1.shape) * np.prod(p2.shape) > 0)

    def test_undistortion(self):
        left = np.hstack((np.linspace(0.0, 640.0, 100).reshape((-1, 1)), np.zeros((100, 1))))
        ret0 = self.cam.cam['left'].normalize_keypoints(left)
        ret1 = self.cam.cam['right'].normalize_keypoints(left)
        testing.assert_array_equal(ret0, ret1)

    def test_undistortion2(self):
        left = np.hstack((np.linspace(0.0, 640.0, 100).reshape((-1, 1)), np.zeros((100, 1))))
        ret0 = self.cam.cam['left'].normalize_keypoints(left)
        testing.assert_array_equal(ret0[:, 1], np.repeat(ret0[0, 1], 100, axis=0))

    def test_triangulation(self):
        point = np.array([123., 32., 400., 1.])
        p1, p2 = self.cam.get_extrinsic()
        img_point0 = p1.dot(point)
        img_point1 = p2.dot(point)
        intrinsic0, _ = self.cam_l.get_calibration_parameters()
        intrinsic1, _ = self.cam_l.get_calibration_parameters()
        img_point0 = from_homogeneous(intrinsic0.dot(img_point0))
        img_point1 = from_homogeneous(intrinsic1.dot(img_point1))
        p4d, _, _ = self.cam.triangulate_matches(img_point0, img_point1)
        testing.assert_almost_equal(point[:-1], p4d.reshape(-1))


class TestTracker(TestCase):
    def setUp(self):
        self.world_projection = np.eye(3, 4)
        self.extrinsic_right = np.eye(3, 4)
        self.extrinsic_right[0, -1] -= 50.0
        self.world_projection_right = combine(self.world_projection, self.extrinsic_right)
        self.tracker = PointTracker(10.0, 0.001, 2.0)
        self.point = np.random.random(3) * 1000.0
        self.point[:2] -= 500.0
        self.point = np.hstack((self.point, 1))
        self.initial_mean = np.hstack((self.point[:3], np.zeros(3)))
        self.initial_cov = np.eye(6, 6) * 10.0

    def test_transformation_zero_speed(self):
        point = np.array([123.0, 329.2, -231.2, 0.0, 0.0, 0.0])
        new_point = PointTracker.transformation(point, 1.0)
        testing.assert_almost_equal(point, new_point)

    def test_transformation_positive_speed(self):
        point = np.array([123.0, 329.2, -231.2, 11.0, 2.2, 5.1])
        new_point = PointTracker.transformation(point, 1.0)
        expected = np.array([134.0, 331.4, -226.1, 11.0, 2.2, 5.1])
        testing.assert_almost_equal(new_point, expected)

    def test_projection(self):
        world_projection = np.eye(3, 4)
        extrinsic_right = np.eye(3, 4)
        extrinsic_right[:, -1] += np.array([-50, 3.0, 1.2])
        point = np.array([11, 9.8, 102.4, 1])
        left_proj = world_projection.dot(point)
        right_proj = extrinsic_right.dot(point)
        left_proj = left_proj[:-1] / left_proj[-1]
        right_proj = right_proj[:-1] / right_proj[-1]
        obs = np.hstack((left_proj, right_proj))
        mean, cov = self.tracker._update_stereo(None, None, obs, 1.0, extrinsic_right, world_projection)
        testing.assert_almost_equal(point[:-1], mean[:3])

    def test_update_stereo(self):
        obs_l = self.world_projection.dot(self.point)
        obs_r = self.world_projection_right.dot(self.point)
        obs_l = obs_l[:-1] / obs_l[-1]
        obs_r = obs_r[:-1] / obs_r[-1]
        obs = np.hstack((obs_l, obs_r))
        mean, _ = self.tracker._update_stereo(self.initial_mean, self.initial_cov, obs, 1.0, self.extrinsic_right,
                                              self.world_projection)
        testing.assert_almost_equal(self.point[:-1], mean[:3], decimal=0)


# noinspection PyUnresolvedReferences,PyShadowingNames,PyUnusedLocal
class TestFeatureTree(TestCase):
    def setUp(self):
        self.tree = DynamicVocabularyTree(3, 'hamming')
        self.camera = Camera.get_default_camera(2, 640, 0, 0.5, 0)
        self.feat = FeatureDetector('surf', 'freak')

    def test_adding(self):
        feature = np.random.random(128) * 128
        """:type: ndarray"""
        self.tree.add_word(feature)
        testing.assert_almost_equal(self.tree.words[0], feature)
        self.assertEqual(self.tree.children[0], [1])

    def test_str(self):
        features = np.eye(12)
        self.tree.add_words(features)
        ret = str(self.tree)
        self.assertEqual(ret, '0\n|1\n||12\n||13\n|2\n|3\n|4\n|5\n|6\n|7\n|8\n|9\n|10\n|11\n')

    def test_info(self):
        features = np.random.random((25, 128)) * 128
        """:type: ndarray"""
        information = [dict(info=np.random.random(128)) for x in range(0, 25)]
        self.tree.add_words(features, information)
        closest = self.tree.get_closest(features[17, :])
        information2 = self.tree.get_info([closest[0][0]], 'info').reshape(-1)
        testing.assert_almost_equal(information[17]['info'], information2)

    def test_closest(self):
        features = np.random.random((25, 128)) * 128
        """:type: ndarray"""
        self.tree.add_words(features)
        ret = self.tree.get_closest(features[8, :])
        testing.assert_almost_equal(features[8, :], self.tree.words[ret[0][0]])

    def test_closest_2(self):
        features = np.random.random((25, 128)) * 128
        """:type: ndarray"""
        self.tree.add_words(features)
        new_feature = np.copy(features[7, :])
        """:type: ndarray"""
        new_feature[0] += 1
        self.tree.add_word(new_feature)
        ret = self.tree.get_closest(features[7, :], 2)
        testing.assert_almost_equal(new_feature, self.tree.words[ret[1][0]])

    def test_order(self):
        features = self.add_features()
        """:type: ndarray"""
        ret = self.tree.get_closest(features[13, :], 2)
        self.assertLessEqual(ret[0][1], ret[1][1])

    def add_features(self):
        features = np.random.random((500, 128)) * 128
        """:type: ndarray"""
        self.tree.add_words(features)
        return features

    def test_duration(self):
        features = np.random.random((1000, 128)) * 128
        """:type: ndarray"""
        self.tree.add_words(features)
        t = time.time()
        for i in range(0, 1000):
            ret = self.tree.get_closest(features[i, :], 2)
        duration = time.time() - t
        self.assertLess(duration / 1000.0, 0.0005)

    def test_removal(self):
        features = np.random.random((25, 128)) * 128
        """:type: ndarray"""
        information = [dict(info=x) for x in range(0, 25)]
        self.tree.add_words(features, information)
        criteria = lambda x: x['info'] < 12
        self.tree.remove(criteria)
        for node, value in self.tree.information.iteritems():
            if node not in self.tree.children:
                assert value['info'] >= 12

    def test_pruning_to_zero(self):
        features = np.random.randint(0, 255, (1000, 64))
        self.tree.add_words(features)
        self.tree.prune_tree(0)
        self.assertTrue(len(self.tree.children[0]) == 0)

    def test_train(self):
        data = np.random.random((1000, 2))
        bf = cv2.BFMatcher(cv2.NORM_HAMMING)
        f_cluster = lambda data, centroid: np.array(
            [[x.trainIdx, x.distance] for x in bf.match(data.astype(np.uint8), centroid.astype(np.uint8))])
        f_cluster_2 = lambda data0, data1: np.array(
            [[x.trainIdx, x.distance] for _, x in bf.knnMatch(data0.astype(np.uint8), data1.astype(np.uint8), k=2)])
        voc_tree = FixedVocabularyTree.train_vocabulary_tree(data, f_cluster, f_cluster_2, distance_measure='hamming',
                                                             branching_factor=3)
        self.assertTrue(len(voc_tree.words) > data.shape[0])

    def test_load_trained_tree(self):
        folder = 'model/tree/1421780124/'
        voc_tree = FixedVocabularyTree.load(folder)
        self.assertTrue(len(voc_tree.words) > 0)

    def test_loaded_tree_recognition(self):
        folder = 'model/tree/1421780124/'
        voc_tree = FixedVocabularyTree.load(folder)
        """:type: VocabularyTree2"""
        desc = np.random.randint(0, 255, (200, 64))
        voc_tree.add_words(desc)
        near, _ = voc_tree.get_closest(desc[0, :])
        desc2 = voc_tree.words[near[0][0]]
        testing.assert_almost_equal(desc2, desc[0, :])

    def test_loaded_tree_noisy_recognition(self):
        folder = 'model/tree/1421780124/'
        voc_tree = FixedVocabularyTree.load(folder)
        """:type: VocabularyTree2"""
        desc = np.random.randint(0, 255, (2, 64))
        voc_tree.add_words(desc)
        desc_true = desc[0, :]
        desc1 = np.maximum(np.minimum(desc_true + np.random.randint(0, 2, 64), 255), 0)
        near, _ = voc_tree.get_closest(desc1)
        desc2 = voc_tree.words[near[0][0]]
        testing.assert_almost_equal(desc2, desc_true)

    def test_loaded_tree_visually(self):
        global img
        folder = 'model/tree/1421780124/'
        voc_tree = FixedVocabularyTree.load(folder)
        voc_tree.prune_tree(0)
        # First image
        _, lab = self.camera.get_image()
        kps, desc = self.feat.detect_and_describe(lab[:, :, 0])
        info = [dict(obs=row) for row in kps]
        voc_tree.add_words(desc, info)
        # Second image
        for i in range(0, 5):
            img, lab = self.camera.get_image()
            kps, desc = self.feat.detect_and_describe(lab[:, :, 0])
        plt.imshow(img[:, :, [2, 1, 0]])
        t = 0.0
        count = 0
        for kp, feature in zip(kps, desc):
            t0 = time.time()
            near, _ = voc_tree.get_closest(feature, neighbours=2)
            t += time.time() - t0
            if near[0][1] < 0.5 * near[1][1]:
                count += 1
                prev_kp = voc_tree.get_information([near[0][0]], 'obs')
                plot_line(kp[:2], prev_kp[0, :2])
        print 'duration per lookup:', t / desc.shape[0]
        print 'count', count, '/', desc.shape[0], '=', float(count) / desc.shape[0]
        plt.draw()
        plt.pause(10.0)


class TestHammingFeatureTree(TestFeatureTree):
    def setUp(self):
        self.tree = DynamicVocabularyTree(10, 'hamming')


class TestDotFeatureTree(TestFeatureTree):
    def setUp(self):
        self.tree = DynamicVocabularyTree(10, 'dot')

    def test_order(self):
        features = self.add_features()
        ret = self.tree.get_closest(features[13, :], 2)
        print ret
        self.assertGreaterEqual(ret[0][1], ret[1][1])


class TestEgomotionTracker(TestCase):
    def setUp(self):
        self.tracker = EgomotionTracker(5.0, 1.0)

    def test0(self):
        plt.imshow(self.tracker.observation_matrix, interpolation='none')
        plt.colorbar()
        plt.show()


class TestProjection(TestCase):
    def setUp(self):
        euler1 = np.random.random(3) * 2.0 - 1.0
        euler2 = np.random.random(3) * 2.0 - 1.0
        rot1 = cv2.Rodrigues(euler1)[0]
        rot2 = cv2.Rodrigues(euler2)[0]
        t1 = np.random.random((3, 1)) * 500 - 250
        t2 = np.random.random((3, 1)) * 500 - 250
        self.p1 = compose(rot1, t1)
        self.p2 = compose(rot2, t2)
        self.point = np.hstack((np.random.random(3) * 500 + 500, np.array([1])))

    def test_combine(self):
        p3 = combine(self.p1, self.p2)
        proj3 = p3.dot(self.point)
        proj2 = np.hstack((self.p2.dot(self.point), np.array([1.0])))
        proj1 = self.p1.dot(proj2)
        testing.assert_almost_equal(proj3, proj1)

    def test_inverse(self):
        p1_ = inverse(inverse(self.p1))
        testing.assert_almost_equal(p1_, self.p1)

    def test_inverse2(self):
        inv_p1 = inverse(self.p1)
        proj = self.p1.dot(self.point)
        inv_proj = inv_p1.dot(np.hstack((proj, 1)))
        testing.assert_almost_equal(self.point[:-1], inv_proj)

    def test_compose_decompose(self):
        rot, t = decompose(self.p1)
        p1_ = compose(rot, t)
        testing.assert_almost_equal(p1_, self.p1)

    def test_to_from_projection(self):
        euler, t = from_projection(self.p1)
        p1_ = to_projection(euler, t)
        testing.assert_almost_equal(p1_, self.p1)

    def test_filterable_angles0(self):
        angle0 = 0.8 * math.pi
        angle1 = -0.9 * math.pi
        filterable0 = rodrigues_to_filterable(angle0)
        filterable1 = rodrigues_to_filterable(angle1)
        average = filterable_to_rodrigues(filterable0 + filterable1)
        self.assertTrue(average > 0)

    def test_filterable_angles1(self):
        average = filterable_to_rodrigues(
            rodrigues_to_filterable(-0.4 * math.pi) + rodrigues_to_filterable(0.4001 * math.pi))
        self.assertTrue(abs(average) < 0.1)

    def test_filterable_inverse0(self):
        self.assertTrue(filterable_to_rodrigues(rodrigues_to_filterable(0.123)) - 0.123 < 0.01)

    def test_filterable_inverse1(self):
        self.assertTrue(filterable_to_rodrigues(rodrigues_to_filterable(0.)) - 0. < 0.01)


class TestHopkins(TestCase):
    def setUp(self):
        self.hopkins = Hopkins('C:/Users/PieterM/Videos/Hopkins 155')

    def test_folders(self):
        self.hopkins.read_sequences()
        self.assertGreater(len(self.hopkins.sequences), 0)


# noinspection PyUnresolvedReferences
class TestTrajectories(TestCase):
    def setUp(self):
        self.dimension = 3
        self.trajectories = Trajectories(5, 100, 5, self.dimension, 0.05, 0.0)
        self.data = self.trajectories.trajectories
        self.mask = self.trajectories.mask
        self.ssc = SSC(5.0, 'L-BFGS-B', 10, 0.0001, 5, 5)

    def test_ssc_all_coefficients(self):
        coefficients, performance = self.ssc.find_all_coefficients(self.data, self.mask)
        self.assertTrue(np.sum(np.abs(coefficients)) > 0.0)

    def test_ssc_mask(self):
        # noinspection PyTypeChecker
        coefficients, _ = self.ssc.find_all_coefficients(self.data, np.random.random(self.data.shape) > 0.5)
        self.assertTrue(np.sum(np.abs(coefficients)) > 0)


class Performance(TestCase):
    LENGTHS = np.arange(3, 10)
    TRACKS = np.arange(60, 180, 20)
    SUBSPACES = np.arange(2, 9)
    DIMENSIONS = 3
    NOISES = np.arange(0.0, 0.21, 0.03)
    SPARSENESSESS = np.arange(0.0, 0.35, 0.05)
    LAMBDAS = np.array([3.0])
    MINMAX_ITERS = np.array([2])
    TOLERANCE = 0.0001
    GMM_RESTARTS = np.array([1])
    GMM_ITERS = np.array([2])
    TRIALS = 10

    def get_default_parameters(self):
        return 5, 100, 3, 0.1, 0.1, 3.0, 2, 1, 2

    def get_random_parameters(self):
        length = random.choice(Performance.LENGTHS)
        track = random.choice(Performance.TRACKS)
        subspace = random.choice(Performance.SUBSPACES)
        noise = random.choice(Performance.NOISES)
        sparseness = random.choice(Performance.SPARSENESSESS)
        lambda_ = random.choice(Performance.LAMBDAS)
        minmax_iter = random.choice(Performance.MINMAX_ITERS)
        gmm_restart = random.choice(Performance.GMM_RESTARTS)
        gmm_iter = random.choice(Performance.GMM_ITERS)
        return length, track, subspace, noise, sparseness, lambda_, minmax_iter, gmm_restart, gmm_iter

    @staticmethod
    def parameters_to_string(length, track, subspace, noise, sparseness, lambda_, minmax_iter, gmm_restart, gmm_iter):
        ret = ''
        if length is not None:
            ret += "D=" + str(length) + ", "
        if track is not None:
            ret += "N=" + str(track) + ", "
        if subspace is not None:
            ret += 'C=' + str(subspace) + ', '
        if noise is not None:
            ret += '$\sigma$=' + str(noise) + ', '
        if sparseness is not None:
            ret += 'Missing=' + str(round(sparseness * 100)) + '%, '
        if lambda_ is not None:
            ret += '$\lambda$=' + str(lambda_) + ', '
        if minmax_iter is not None:
            ret += 'Mini iter=' + str(minmax_iter) + ', '
        if gmm_restart is not None:
            ret += 'GMM restarts=' + str(gmm_restart) + ', '
        if gmm_iter is not None:
            ret += 'GMM iter=' + str(gmm_iter) + ', '
        return ret[:-2]

    @staticmethod
    def run_ssc(length, track, subspace, noise, sparseness, lambda_, minmax_iter, gmm_restart, gmm_iter, method):
        trajectories = Trajectories(length, track, subspace, Performance.DIMENSIONS, noise, sparseness)
        data = trajectories.trajectories
        mask = trajectories.mask
        ssc = SSC(lambda_, method, minmax_iter, Performance.TOLERANCE, gmm_restart, gmm_iter)
        coefficients, _ = ssc.find_all_coefficients(data, mask)
        laplacian, vectors = ssc.compute_laplacian_decomposition(coefficients, np.max(Performance.SUBSPACES),
                                                                 'normalized_rw')
        labels, probabilities, k, bic = ssc.apply_gmm(vectors[:, 1:], range(2, 30))
        truth = ind2vec(trajectories.ground_truth)
        truth_prob = truth.dot(truth.T).flatten()
        prob = probabilities.dot(probabilities.T).flatten()
        return truth_prob, prob, k

    def test_performance_roc(self):
        create_directory('data/plots/')
        create_directory('data/raw/')
        # Create and save parameters
        iter_values_mask = range(0, 6)
        iter_values = [Performance.LENGTHS, Performance.TRACKS, Performance.SUBSPACES, Performance.NOISES,
                       Performance.SPARSENESSESS, Performance.LAMBDAS, Performance.MINMAX_ITERS,
                       Performance.GMM_RESTARTS, Performance.GMM_ITERS]
        np.save('data/raw/' + str(Performance.TRIALS) + '_parameters', iter_values)
        titles = ['Trajectory length', '#Tracks', '#Subspaces', 'Noise', 'Sparseness', 'Lambdas',
                  'Minimization iterations', 'GMM restarts', 'GMM iterations']
        print 'Testing performance with', Performance.TRIALS, 'trials'
        for i in range(0, len(iter_values)):
            print titles[i], '\t', iter_values[i]
        print ''
        debug_title = ['length', 'tracks', 'subspaces', 'noise', 'sparseness', 'lambdas', 'mini_iter', 'gmm_restarts',
                       'gmm_iter']
        for i in iter_values_mask:
            plt.clf()
            rates = dict()
            performance = []
            values = iter_values[i]
            duration = np.zeros((values.shape[0], Performance.TRIALS))
            # Compute performance
            for value_i, value in enumerate(values):
                print titles[i], value
                label_true = []
                label_est = []
                for trial in range(0, Performance.TRIALS):
                    params = list(self.get_random_parameters())
                    params[i] = value
                    time0 = time.time()
                    truth_prob, prob, _ = Performance.run_ssc(params[0], params[1], params[2], params[3], params[4],
                                                              params[5], params[6], params[7], params[8], 'L-BFGS-B')
                    time1 = time.time()
                    duration[value_i, trial] = time1 - time0
                    label_true.extend(truth_prob.tolist())
                    label_est.extend(prob.tolist())
                fpr, tpr, _ = roc_curve(np.array(label_true), np.array(label_est))
                rates[value] = dict(fpr=fpr, tpr=tpr)
                auc = roc_auc_score(np.array(label_true), np.array(label_est))
                performance.append(auc)
                plt.plot(fpr, tpr)
            # Save ROC data
            plt.legend(values)
            plt.title(titles[i])
            plt.ylabel('True positives')
            plt.xlabel('False positives')
            plt.xlim([0, 1.01])
            plt.ylim([0, 1.01])
            plt.savefig('data/plots/' + str(Performance.TRIALS) + '_' + debug_title[i] + '_roc.png', format='png',
                        transparent=True)
            np.save('data/raw/' + str(Performance.TRIALS) + '_' + debug_title[i] + '_roc', rates)
            # Save AUC data
            plt.clf()
            plt.plot(values, performance)
            plt.xlabel(titles[i])
            plt.ylabel('AUC score')
            plt.title(titles[i])
            plt.ylim([0.5, 1.01])
            plt.savefig('data/plots/' + str(Performance.TRIALS) + '_' + debug_title[i] + '_auc.png', format='png',
                        transparent=True)
            save_perf = np.vstack((values, performance))
            np.save('data/raw/' + str(Performance.TRIALS) + '_' + debug_title[i] + '_auc', save_perf)
            # Save duration data
            plt.clf()
            plt.errorbar(values, np.mean(duration, axis=1), yerr=np.std(duration, axis=1))
            plt.xlabel(titles[i])
            plt.ylabel('Duration')
            plt.title(titles[i] + ' vs. Duration')
            plt.savefig('data/plots/' + str(Performance.TRIALS) + '_' + debug_title[i] + '_duration.png', format='png',
                        transparant=True)
            np.save('data/raw/' + str(Performance.TRIALS) + '_' + debug_title[i] + '_duration', duration)

    def test_performance_clusters(self):
        length, track, subspace, _, sparseness, lambda_, minmax_iter, gmm_restart, gmm_iter = \
            self.get_default_parameters()
        legend = []
        output = np.zeros((Performance.NOISES.shape[0], Performance.TRIALS))
        print 'Test number of clusters performance with', Performance.TRIALS, 'trials'
        for noise_i, noise in enumerate(Performance.NOISES):
            print "Noise", noise
            legend.append(noise)
            for trial_i in range(0, Performance.TRIALS):
                truth_prob, prob, k = Performance.run_ssc(length, track, subspace, noise, sparseness, lambda_,
                                                          minmax_iter, gmm_restart, gmm_iter, 'L-BFGS-B')
                output[noise_i, trial_i] = k
        plt.plot(Performance.NOISES, np.mean(output, axis=1))
        plt.xlabel('Noise')
        plt.ylabel('Average number of clusters detected')
        plt.title('Number of clusters vs. noise')
        plt.suptitle(Performance.parameters_to_string(length, track, subspace, None, sparseness, lambda_, minmax_iter,
                                                      gmm_restart, gmm_iter))
        plt.savefig('data/plots/' + str(Performance.TRIALS) + '_noise_vs_k.png', format='png', transparent=True)
        np.save('data/raw/' + str(Performance.TRIALS) + '_noise_vs_k.png', output)

    # noinspection PyTypeChecker
    def test_performance_initialization(self):
        priors = np.arange(0.0, 1.1, 0.1)
        length, track, subspace, noise, sparseness, lambda_, minmax_iter, _, _ = self.get_default_parameters()
        minmax_iter = 200
        output_normal = np.zeros((priors.shape[0], Performance.TRIALS))
        output_prior = np.zeros((priors.shape[0], Performance.TRIALS))
        print "Test initialization performance with", Performance.TRIALS, 'trials'
        for prior_i, prior in enumerate(priors):
            print "Knowledge", prior
            for trial_i in range(0, Performance.TRIALS):
                trajectories = Trajectories(length, track, subspace, Performance.DIMENSIONS, noise, sparseness)
                data = trajectories.trajectories
                mask = trajectories.mask
                ssc = SSC(lambda_, 'L-BFGS-B', minmax_iter, Performance.TOLERANCE, None, None)
                # Get prior knowledge
                coefficients, _ = ssc.find_all_coefficients(data[:-3, :], mask[:-3, :])
                new_coefficients = (1.0 - prior) * (
                    np.random.random(coefficients.shape) / coefficients.shape[0]) + prior * coefficients
                # Measure without prior knowledge
                ssc = SSC(lambda_, 'L-BFGS-B', minmax_iter, Performance.TOLERANCE, None, None)
                coefficients, _ = ssc.find_all_coefficients(data, mask)
                output_normal[prior_i, trial_i] = ssc.result.nit
                # Measure with prior knowledge
                ssc = SSC(lambda_, 'L-BFGS-B', minmax_iter, Performance.TOLERANCE, None, None)
                coefficients, _ = ssc.find_all_coefficients(data, mask, new_coefficients)
                output_prior[prior_i, trial_i] = ssc.result.nit
        plt.errorbar(priors, np.mean(output_normal, axis=1), yerr=np.std(output_normal, axis=1), color='g')
        plt.errorbar(priors, np.mean(output_prior, axis=1), yerr=np.std(output_normal, axis=1), color='b')
        plt.legend(['Normal', 'With prior'])
        plt.xlabel('Prior knowledge (%)')
        plt.xlim([0, 1.01])
        plt.ylabel('Minimization iterations')
        plt.title('Prior knowledge vs. duration')
        plt.suptitle(
            Performance.parameters_to_string(length, track, subspace, None, sparseness, lambda_, minmax_iter, None,
                                             None))
        plt.savefig('data/plots/' + str(Performance.TRIALS) + '_init_vs_speed.png', format='png', transparent=True)
        np.save('data/raw/' + str(Performance.TRIALS) + '_init_vs_speed_normal', output_normal)
        np.save('data/raw/' + str(Performance.TRIALS) + '_init_vs_speed_prior', output_prior)


# noinspection PyUnresolvedReferences,PyShadowingNames
class RandomCode(TestCase):
    def setUp(self):
        self.p1 = np.hstack((np.eye(3), np.zeros((3, 1))))
        self.p2 = np.hstack((np.eye(3), np.array([[-53], [0], [0]])))
        # self.cam = StereoCamera(2)
        # self.feature = FeatureDetector(feature_type='sift')
        # self.matcher = KnnMatcher(mutual_preference=True, match_quality=0.8)

    def test_triangulation(self):
        points1 = np.array([[0.0, 0.0]]).T
        points2 = np.array([[-0.1, 0.0]]).T
        print points1
        print points2
        points4 = cv2.triangulatePoints(self.p1, self.p2, points1, points2).T
        points3 = points4[:, :3] / np.repeat(points4[:, 3].reshape((-1, 1)), 3, axis=1)
        print points3

    def test_unscented(self):
        kf = AdditiveUnscentedKalmanFilter(initial_state_mean=0.0, initial_state_covariance=0.0)
        mean, cov = kf.filter_update(np.array([0.0]), np.array([[1.0]]), np.array([1.0]))
        print mean, cov
        mean, cov = kf.filter_update(mean, cov, np.array([2.0]))
        print mean, cov
        mean, cov = kf.filter_update(mean, cov, np.array([2.0]))
        print mean, cov
        mean, cov = kf.filter_update(mean, cov, np.array([2.0]))
        print mean, cov
        mean, cov = kf.filter_update(mean, cov, np.array([2.0]))
        print mean, cov
        mean, cov = kf.filter_update(mean, cov, np.array([2.0]))
        print mean, cov
        mean, cov = kf.filter_update(mean, cov, np.array([2.0]))
        print mean, cov
        mean, cov = kf.filter_update(mean, cov, np.array([2.0]))
        print mean, cov

    def test_extrinsic(self):
        point = np.array([0.0, 0.0, 3.0, 1.0])
        extrinsic = np.eye(3, 4)
        for i in range(0, 10):
            projection = extrinsic.dot(point)
            extrinsic[2, -1] -= 0.5
            print projection

    def test_performance(self):
        # performance[ind_t, ind_o, ind_w, ind_d, :] = np.mean(diff[:, 1:], axis=0)
        performance = np.load('data/performance20141020.npy')
        num_transitions = 19
        num_observations = 19
        # noinspection PyTypeChecker
        transition_covs = np.round(10 ** np.linspace(-4, -1, num_transitions), 6)
        # noinspection PyTypeChecker
        observation_covs = np.round(10 ** np.linspace(-4, -1, num_observations), 6)
        cov_widths = np.linspace(0.05, 0.15, 3)
        mean_performance = np.mean(np.mean(performance, axis=4), axis=3)
        min_t, min_o, min_w = np.where(np.min(mean_performance) == mean_performance)
        print "Transition cov", min_t, transition_covs[min_t]
        print "Observation cov", min_o, observation_covs[min_o]
        print "Width cov", min_w, cov_widths[min_w]
        print "Performance", np.min(performance)
        plt.figure(1)
        plt.clf()
        plt.imshow(np.std(np.mean(performance[:, :, min_w, :, :], axis=3), axis=3).T[0, :, :], interpolation='none')
        plt.xlabel('Transition covariance')
        plt.ylabel('Observation covariance')
        plt.xticks(range(0, num_transitions))
        plt.yticks(range(0, num_observations))
        plt.gca().set_yticklabels(observation_covs)
        plt.gca().set_xticklabels(transition_covs)
        setp(plt.axes().get_xticklabels(), rotation=90)
        plt.colorbar()
        plt.figure(2)
        plt.clf()
        plt.imshow(mean_performance[:, :, min_w][:, :, 0], interpolation='none')
        plt.xlabel('Transition covariance')
        plt.ylabel('Observation covariance')
        plt.xticks(range(0, num_transitions))
        plt.yticks(range(0, num_observations))
        plt.gca().set_yticklabels(observation_covs)
        plt.gca().set_xticklabels(transition_covs)
        setp(plt.axes().get_xticklabels(), rotation=90)
        plt.colorbar()
        plt.figure(3)
        plt.imshow(performance[min_t, min_o, min_w, :, :].T[:, :, 0], interpolation='none')
        plt.xlabel('Average distance')
        plt.ylabel('#Observation')
        plt.colorbar()
        plt.draw()
        plt.pause(10000.0)

    def test_bytes(self):
        a = np.array([12, 3, 232, 123, 212, 321, 23, 123], np.uint8)
        print a

    def test_bf_matcher(self):
        bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=True)
        cam = Camera.get_default_camera(0, 640, 0, 0.5)
        img, lab = cam.get_image()
        orb = cv2.ORB()
        kp1, des1 = orb.detectAndCompute(img, None)
        img, lab = cam.get_image()
        kp2, des2 = orb.detectAndCompute(img, None)
        print des1.dtype, type(des1)
        bf.match(des1, des2)

    def test_pyqt(self):
        import pyqtgraph.examples

        pyqtgraph.examples.run()

    def test_projections(self):
        point = np.array([300., 300., 300., 1.])
        rot_w = cv2.Rodrigues(np.array([0.0, -0.0, 0.0]))[0]
        t_w = np.array([0., 0., 0.]).reshape((-1, 1))
        p1 = np.hstack((rot_w, t_w))

        projection = p1.dot(point)
        projection = projection[:-1] / projection[-1]
        print projection

    def test_point_convergence(self):
        tracker = PointTracker(10.0, 0.0005, 2.0)
        point = np.array([-200, 12.2, 333.0]) + np.random.random(3) * 10.0 - 5.0
        speed = np.array([21.3, 0.0, 3.0])
        updates = 20
        mean = None
        cov = None
        plt.scatter([-self.p1[0, -1], -self.p2[0, -1]], [-self.p1[2, -1], -self.p2[2, -1]])
        for i in range(0, updates):
            obs1 = self.p1.dot(np.hstack((point, 1)))
            obs2 = self.p2.dot(np.hstack((point, 1)))
            obs1 = np.round((obs1[:-1] / obs1[-1]) * 100) / 100.0
            obs2 = np.round((obs2[:-1] / obs2[-1]) * 100) / 100.0
            obs = np.hstack((obs1, obs2)) + np.random.random(4) * 0.01 - 0.005
            mean, cov = tracker._update_stereo(mean, cov, obs, 1.0, self.p1, self.p2)
            point += speed

    def test_save_data(self):
        arr = np.load('data/estimation0.0.npy')
        arr[0, :] = np.eye(3, 4)
        with open('data/07.txt', 'wb') as csv_file:
            writer = csv.writer(csv_file, delimiter=' ')
            for row in arr:
                row[:, -1] = -row[:, -1] / 100.0
                writer.writerow(row.flatten())
            row = arr[-1, :, :]
            row[:, -1] = row[:, -1] / 100.0
            writer.writerow(row.flatten())

    def test_show_track(self):
        arr = np.load('data/ground_truth0.0.npy')
        arr[:, :, -1] /= 100.0
        fig1 = plt.figure(1)
        ax1 = fig1.add_subplot(111, projection='3d')
        ax1.scatter(arr[0, 0, -1], arr[0, 2, -1], arr[0, 1, -1], c='g', s=50)
        ax1.scatter(arr[-1, 0, -1], arr[-1, 2, -1], arr[-1, 1, -1], c='r', s=50)
        ax1.plot(arr[:, 0, -1], arr[:, 2, -1], arr[:, 1, -1])
        ax1.set_aspect('equal')
        ax1.set_xlabel('x')
        ax1.set_ylabel('z')
        ax1.set_zlabel('y')
        mini = np.min(arr[:, :, -1])
        maxi = np.max(arr[:, :, -1])

        fig2 = plt.figure(2)
        ax2 = fig2.add_subplot(111)
        ax2.scatter(arr[0, 0, -1], arr[0, 2, -1], c='g', s=50)
        ax2.scatter(arr[-1, 0, -1], arr[-1, 2, -1], c='r', s=50)
        ax2.plot(arr[:, 0, -1], arr[:, 2, -1])
        ax2.set_aspect('equal')
        ax2.set_xlabel('x')
        ax2.set_ylabel('z')

        l = np.arange(0.0, 0.5, 0.1)
        for i in l:
            arr = np.load('data/estimation' + str(i) + '.npy')
            arr[:, :, -1] /= 100.0
            ax1.plot(-arr[:, 0, -1], -arr[:, 2, -1], -arr[:, 1, -1])
            ax2.plot(-arr[:, 0, -1], -arr[:, 2, -1])
            mini = min(mini, np.min(arr[:, :, -1]))
            maxi = max(maxi, np.max(arr[:, :, -1]))

        l2 = ['true']
        l2.extend(l)
        ax1.legend(l2)
        ax2.legend(l2)
        ax1.set_xlim([mini, maxi])
        ax1.set_ylim([mini, maxi])
        ax1.set_zlim([mini, maxi])
        ax2.set_xlim([mini, maxi])
        ax2.set_ylim([mini, maxi])
        plt.show()

    def test_track_performance(self):
        ground_truth = np.load('data/ground_truth0.0.npy')
        ground_truth[:, :, -1] /= 100.0
        ground_truth[0, :, :] = np.eye(3, 4)
        size = ground_truth.shape[0]
        # Compute ground true values
        dist_trans_true = np.zeros((size, 3))
        dist_euler_true = np.zeros((size, 3))
        for i in range(1, size - 1):
            dist_trans_true[i, :] = translation_difference(ground_truth[i, :, :], ground_truth[i - 1, :, :])
            diff_euler = rotation_difference(ground_truth[i - 1, :, :], ground_truth[i, :, :]).reshape(-1)
            dist_euler_true[i, :] = diff_euler / (2 * math.pi) * 180.0
        abs_dist_trans_true = np.cumsum(np.linalg.norm(dist_trans_true, 2, axis=1))
        translation_error = np.zeros((21, 6))
        rotational_error = np.zeros((21, 6))
        for history_ind in range(0, 21):
            history = history_ind / 10.0
            data = np.load('data/estimation' + str(history) + '.npy')
            data[:, :, -1] /= 100.0
            data[:, :, -1] = -data[:, :, -1]
            data[0, :, :] = np.eye(3, 4)
            dist_trans_data = np.zeros((size, 3))
            for i in range(1, size - 1):
                dist_trans_data[i, :] = data[i, :, :-1].dot(data[i, :, -1]) - data[i, :, :-1].dot(data[i - 1, :, -1])
            for dist in range(0, 6):
                start = 0
                last = start
                ind = np.where(abs_dist_trans_true > abs_dist_trans_true[start] + (dist + 1) * 100.0)
                while ind[0].shape[0] > 0:
                    last = ind[0][0]

                    p_true1 = np.eye(4)
                    p_true1[:-1, :] = ground_truth[start, :, :]
                    p_true2 = np.eye(4)
                    p_true2[:-1, :] = ground_truth[last, :, :]
                    p_data1 = np.eye(4)
                    p_data1[:-1, :] = data[start, :, :]
                    p_data2 = np.eye(4)
                    p_data2[:-1, :] = data[last, :, :]
                    p_true = np.linalg.inv(p_true1).dot(p_true2)
                    p_data = np.linalg.inv(p_data1).dot(p_data2)
                    p_error = np.linalg.inv(p_data).dot(p_true)
                    t_diff = np.sqrt(np.sum(p_error[:-1, -1] ** 2))
                    a = p_error[0, 0]
                    b = p_error[1, 1]
                    c = p_error[2, 2]
                    d = 0.5 * (a + b + c - 1.0)
                    r_diff = math.degrees(math.acos(max(min(d, 1.0), -1.0)))

                    translation_error[history_ind, dist] += t_diff
                    rotational_error[history_ind, dist] += r_diff
                    start += 1
                    ind = np.where(abs_dist_trans_true > abs_dist_trans_true[start] + (dist + 1) * 100.0)
                traveled_dist = abs_dist_trans_true[last] - abs_dist_trans_true[start - 1]
                translation_error[history_ind, dist] /= start - 1
                translation_error[history_ind, dist] /= traveled_dist
                translation_error[history_ind, dist] *= 100.0
                rotational_error[history_ind, dist] /= (start - 1)
                rotational_error[history_ind, dist] /= traveled_dist
        labels = np.arange(0.0, 0.5, 0.1)
        plt.figure(1)
        plt.plot(range(100, 700, 100), translation_error[:5, :].T)
        # plt.title()
        plt.xlabel('Path length')
        plt.ylabel('Translational error [%]')
        plt.legend(labels)
        plt.figure(2)
        plt.plot(range(100, 700, 100), rotational_error[:5, :].T)
        plt.xlabel('Path length')
        plt.ylabel('Rotational error [deg/m]')
        plt.legend(labels)
        plt.figure(3)
        plt.bar(np.arange(0.0, 2.1, 0.1), np.mean(translation_error, axis=1), width=0.08)
        plt.xlabel('History length')
        plt.ylabel('Average translational error [%]')
        plt.figure(4)
        plt.bar(np.arange(0.0, 2.1, 0.1), np.mean(rotational_error, axis=1), width=0.08)
        plt.xlabel('History length')
        plt.ylabel('Average rotational error [deg/m]')
        plt.show()

    def test_show_points(self):
        arr = np.load('data/points.npy')
        fig = plt.figure(1)
        ax = fig.add_subplot(111, projection='3d')
        ind = (arr[:, 2] < 10000) * (arr[:, 0] < 1000) * (arr[:, 1] < 1000)
        ax.scatter(arr[ind, 0], arr[ind, 2], arr[ind, 1])
        # ax.set_aspect('equal')
        mini = np.min(arr[ind, :])
        maxi = np.max(arr[ind, :])
        ax.set_xlim([mini, 2000])
        ax.set_ylim([0, maxi])
        ax.set_zlim([mini, 2000])
        ax.set_xlabel('x')
        ax.set_ylabel('z')
        ax.set_zlabel('y')

        plt.show()

    def test_constraint_minimize(self):
        func = lambda x: np.sum(np.abs(x))
        func_deriv = lambda x: x / np.abs(x)
        x0 = np.random.random(10) - 0.5
        y = np.random.random(10) - 0.5
        cons = ({'type': 'eq', 'fun': lambda x: x.dot(y)}, {'type': 'ineq', 'fun': lambda x: np.sum(x) - 1.0})
        # noinspection PyUnusedLocal
        bounds = [(-1000, 1000) for x in range(0, 10)]
        res = scipy.optimize.minimize(func, x0, method='SLSQP', jac=func_deriv, bounds=bounds, constraints=cons,
                                      options={'disp': True})
        print res.x
        print np.sum(res.x) - 1.0

    def test_minmax_angles(self):
        for x in np.linspace(-2 * math.pi, 2 * math.pi, 50):
            print x
            print np.diag(cv2.Rodrigues(np.array([0.0, 0.0, x]))[0])

    def test_filterable(self):
        ret = rodrigues_to_filterable(np.zeros(3))
        print ret
        ret2 = filterable_to_rodrigues(ret)
        print ret2
        # Testing match quality

    def test_prune_trained_tree(self):
        depth = 3
        folder = 'model/tree/1421780124/'
        new_folder = 'model/tree/depth_' + str(depth) + '/'
        voc_tree = FixedVocabularyTree.load(folder)
        voc_tree.prune_tree(depth)
        voc_tree.save(new_folder)

    @staticmethod
    def plot_projection(projection):
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        start = -projection[:, -1]
        end_z = -projection[:, -1] + projection[:, :-1].T.dot(np.array([0., 0., 500.]))
        end_x = -projection[:, -1] + projection[:, :-1].T.dot(np.array([500., 0., 0.]))
        end_y = -projection[:, -1] + projection[:, :-1].T.dot(np.array([0., 500., 0.]))
        ax.plot([start[0], end_z[0]], [start[2], end_z[2]], [start[1], end_z[1]], linewidth=3.0)
        ax.plot([start[0], end_x[0]], [start[2], end_x[2]], [start[1], end_x[1]], linewidth=3.0)
        ax.plot([start[0], end_y[0]], [start[2], end_y[2]], [start[1], end_y[1]], linewidth=3.0)
        ax.scatter(start[0], start[2], start[1])
        minimum = -500
        maximum = 500
        ax.set_xlim([minimum, maximum])
        ax.set_ylim([minimum, maximum])
        ax.set_zlim([minimum, maximum])
        return ax