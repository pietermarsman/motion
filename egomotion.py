# coding=utf-8
"""
Egomotion computation algorithms
"""
import abc

from projections import *


__author__ = 'Pieter Marsman'


class Egomotion(object):
    """
    Egomotion computation
    """

    def __init__(self):
        """
        Initializes containers for egomotion
        """
        self.intrinsic = None
        self.distortion = None

    def __str__(self):
        ret = '=== ' + self.__class__.__name__ + ' ==='
        return ret

    @abc.abstractmethod
    def find_egomotion(self, points0, points1, reprojection_error, guess=None):
        """
        Abstract class for finding egomotion based on 2D or 3D points
        :param points0: points from the reference frame
        :type points0: ndarray
        :param points1: points from the new frame
        :type points1: ndarray
        :param reprojection_error: The maximum reprojection error during RANSAC
        :type reprojection_error: float
        :param guess: Guess for the projection
        :type guess: ndarray
        :return The rotation and translation of the perspective of points1 to points0
        :rtype ndarray
        :raise NotImplementedError: this is an abstract method
        """
        raise NotImplementedError("Should have implemented this")

    def set_intrinsic(self, intrinsic):
        """
        Sets the intrinsic matrix to  use for egomotion computation
        :param intrinsic: intrinsic camera matrix
        :type intrinsic: ndarray
        """
        self.intrinsic = intrinsic

    def set_distortion(self, distortion):
        """
        Sets the distortion parameters for the egomotion computation
        :param distortion: the distortion of the camera
        :type distortion: ndarray
        """
        self.distortion = distortion


class Egomotion2D2D(Egomotion):
    """
    Determine egomotion based on point correspondences. Uses svd of the fundamental/essential matrix to reconstruct
    the rotational matrix and translational vector.
    """

    # Constant matrix used during the svd
    W = np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])

    def __init__(self, algorithm=cv2.FM_RANSAC):
        """
        Initializes the egomotion recovery algorithm.
        :param algorithm: specifies which algorithm is used for cv2.findFundamentalMat. FM_RANSAC or FM_LMEDS is
        recommended.
        :type algorithm: int
        """
        super(Egomotion2D2D, self).__init__()
        self._algorithm = algorithm
        self._points1 = None
        self._points2 = None
        self.F = None
        self.E = None
        self.error = None

    def __str__(self):
        ret = super(Egomotion2D2D, self).__str__()
        ret += "\nalgorithm: " + str(self._algorithm)
        return ret

    def _find_fundamental_matrix(self):
        """
        Finds the fundamental matrix that relates points0 to points 1. Only works if there are more than 7 points.
        """
        if self._points1.shape[0] > 7:
            self.F, mask = cv2.findFundamentalMat(self._points1, self._points2, method=cv2.FM_RANSAC, param1=3, param2=0.99)
            self._estimate_error(mask)

    def _find_essential_matrix(self, intrinsic):
        """
        Retrieves the essential matrix by combining the fundamental matrix and calibration matrix
        :param intrinsic: matrix of the camera
        :type intrinsic: ndarray
        """
        self.E = intrinsic.T.dot(self.F).dot(intrinsic)

    def _find_rotation_translation(self):
        """
        Recovers the rotation and translation from the essential matrix. Ambiguity is resolved by checking if a 3D
        point is in front of each camera.
        This method is described by Nister(2004) in section 3.1
        """
        # Create possible rotations and translations from the essential matrix. There are two possible rotations and
        # two possible translations.
        ret = np.linalg.svd(self.E)
        """:type : tuple of ndarray"""
        u, s, v_t = ret
        r1 = u.dot(Egomotion2D2D.W).dot(v_t)
        r2 = u.dot(Egomotion2D2D.W.T).dot(v_t)
        t1 = u[:, 2]
        t2 = -u[:, 2]
        # Create a projection matrix from origin to [r1|t1]
        projection1 = np.eye(3, 4)
        projection2 = np.append(r1, t1.reshape((3, -1)), axis=1)
        # Triangulate a point
        point_4d = cv2.triangulatePoints(projection1, projection2, self._points1[0, :].T,
                                         self._points2[0, :].T).flatten()
        # Compute depth for the left and right camera of the projected point
        c1 = point_4d[2] * point_4d[3]
        c2 = projection2.dot(point_4d)[2] * point_4d[3]
        # If it is in front of both camera's, [r1|t1] is the right projection matrix
        if c1 > 0 and c2 > 0:
            return r1, t1
        # If it is behind both camera's the translation is wrong and [r1|t2] is the right projection matrix
        elif c1 < 0 and c2 < 0:
            return r1, t2
        # If the projected point is behind one of the camera's the rotation is wrong and a twist is applied
        elif c1 * c2 < 0:
            twist = np.append(np.eye(3, 4), 2 * np.array([[v_t[0, 2], v_t[1, 2], v_t[2, 2], -0.5]]), axis=0)
            twist_point_4d = point_4d[2] * twist.dot(point_4d)[3]
            # Test is if it is before both camera's
            if twist_point_4d > 0:
                return r2, t1
            else:
                return r2, t2

    def find_egomotion(self, points0, points1):
        """
        Method that executes several methods in this class to recover the egomotion from a set of point
        correspondences.
        :param points0: Nx2 set of points. N > 7
        :type points0: ndarray
        :param points1: Nx2 set of points
        :type points1: ndarray
        """
        if self.intrinsic is None:
            raise ValueError("Intrinsic is None. Set the intrinsic parameters of the camera using set_intrinsic()")
        self._points1 = points0
        self._points2 = points1
        self._find_fundamental_matrix()
        self._find_essential_matrix(self.intrinsic)
        self.R, self.t = self._find_rotation_translation()

    def _estimate_error(self, mask):
        """
        Helper function that estimates how good/bad the fundamental matrix is.
        :param mask: Nx1 matrix of booleans to specify which points to use.
        :type mask: ndarray
        """
        mask = mask[:, 0] != 0
        len_mask = np.sum(mask)
        points1 = np.ones((len_mask, 3), np.float32)
        points2 = np.ones((len_mask, 3), np.float32)
        points1[:, :-1] = self._points1[mask, :]
        points2[:, :-1] = self._points2[mask, :]
        errors = [abs(p[1].T.dot(self.F).dot(p[0])) for p in zip(points1, points2)]
        self.error = np.mean(errors)


class Egomotion3D3D(Egomotion):
    """
    Determines the egomotion based on 3D point correspondences. Uses svd to reconstruct the rotation and then
    computes the translation by looking at the difference between the normal point cloud and the rotated second point
    cloud.
    """

    def __str__(self):
        ret = super(Egomotion3D3D, self).__str__()
        ret += '\nno parameters'
        return ret

    def find_egomotion(self, points0, points1):
        """
        Finds the egomotion using two 3D sets of points
        :param points0: Nx3 set of point
        :type points0: ndarray
        :param points1: Nx3 set of points that correspond to points0 at a later time
        :type points1: ndarray
        :return: the rotation and translation
        :rtype: ndarray, ndarray
        """
        shape = points0.shape
        mean0 = np.repeat(np.mean(points0, axis=0).reshape((1, -1)), shape[0], axis=0)
        mean1 = np.repeat(np.mean(points1, axis=0).reshape((1, -1)), shape[0], axis=0)
        # Subtract the mean of the points and compute the singular value decomposition
        ret = np.linalg.svd((points1 - mean1).T.dot((points0 - mean1)))
        """:type : tuple of ndarray"""
        u, _, v_t = ret
        self.R = v_t.dot(u.T)
        self.t = mean0[0, :] - self.R.dot(mean1[0, :])
        return self.R, self.t


class Egomotion3D2D(Egomotion):
    """
    Determines the egomotion based on 3D-to-2D correspondences. This is often called Perspective-n-Point (PnP).
    """

    def __init__(self, method=cv2.EPNP):
        super(Egomotion3D2D, self).__init__()
        self.method = method

    def __str__(self):
        ret = super(Egomotion3D2D, self).__str__()
        ret += '\nMethod: ' + str(self.method)
        return ret

    def find_egomotion(self, points0, points1, reprojection_error, guess=None):
        """
        Finds the egomotion using a 3D and 2D set
        :param points0: Nx3 set of points in the current frame
        :type points0: ndarray
        :param points1: Nx2 set of points, projection of points0 on the perspective plane in the previous frame
        :type points1: ndarray
        :param reprojection_error: The maximum reprojection error during RANSAC
        :type reprojection_error: float
        :param guess: Guess for the projection
        :type guess: ndarray
        :return: The rotation, translation and number of inliers of the perspective of points1 to points0
        :rtype: (ndarray, ndarray, ndarray)
        """
        if self.intrinsic is None or self.distortion is None:
            raise ValueError("Both intrinsic and distortion of the camera should not be None. Initialize those "
                             "variables using set_intrinsic() and set_distortion().")
        euler_rot = None
        translation = None
        if guess is not None:
            euler_rot, translation = from_projection(guess)
            translation = np.array(translation)
        p0, p1 = points0.astype(np.float32), points1.astype(np.float32)
        args = (p0, p1, self.intrinsic, self.distortion, euler_rot, translation, guess is not None)
        rot, translation, inliers = cv2.solvePnPRansac(*args, flags=cv2.P3P, reprojectionError=reprojection_error)
        translation = translation.reshape(-1)
        rot_matrix, _ = cv2.Rodrigues(rot)
        return rot_matrix, translation, inliers