# coding=utf-8
"""
Graphical user interface for showing output images, plots and matrices and changing parameters.
"""
from PyQt4 import QtCore
from PyQt4.QtCore import QObject, SIGNAL
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
from pyqtgraph.dockarea import *
import pyqtgraph.opengl as gl
from matplotlib import pyplot as plt
import time

from following import Matcher
from miscellaneous import shape
from projections import decompose, to_homogeneous, combine, inverse
import timer


__author__ = 'Pieter Marsman'


class Gui(QtGui.QWidget):
    """
    Creates a GUI with PyQtGraph to show images and 3D points
    """

    # noinspection PyUnresolvedReferences
    def __init__(self, properties, algorithm):
        """
        Initializes the window and sub-windows

        :param algorithm: The running algorithm
        :type algorithm: Algorithm
        """
        super(Gui, self).__init__()
        pg.setConfigOptions(useWeave=False)
        self._colors = None
        self._area = DockArea()
        # Create main window
        self._win = QtGui.QMainWindow()
        self._win.setCentralWidget(self._area)
        self._win.showMaximized()
        self._win.setWindowTitle('Obstacle motion')
        self._win.show()
        # Create and set docks
        self.dock_image = Dock("Image")
        self.dock_plot = Dock("Plot")
        self.dock_settings = Dock("Settings")
        self.dock_settings.setMaximumWidth(250)
        self.dock_image.setMaximumHeight(350)
        self._area.addDock(self.dock_image, 'top')
        self._area.addDock(self.dock_settings, 'right', self.dock_image)
        self._area.addDock(self.dock_plot, 'bottom', self.dock_image)
        # Add properties in the settings dock
        for key in sorted(properties):
            p = properties[key]
            """:type: AlgorithmProperty"""
            if type(p.value) is str:
                self._create_menu(self.dock_settings, key, p.value, p.step, p.set_value_with_combobox)
            else:
                self._create_spin(self.dock_settings, key, p.value, p.bounds, p.step, p.set_value_with_spinbox)
        scroller = QtGui.QScrollArea()
        scroller.setWidgetResizable(True)
        self._text_output = QtGui.QLabel("Textual output")
        self._text_output.setAlignment(QtCore.Qt.AlignTop)
        scroller.setWidget(self._text_output)
        self.dock_settings.addWidget(scroller)
        self.text = ''

        # Add widgets to the docks
        self.widget_image = pg.ImageView()
        self.dock_image.addWidget(self.widget_image)
        self.widget_plot = gl.GLViewWidget()
        self.widget_plot.opts['distance'] = 200
        self._plot = dict(a=gl.GLScatterPlotItem(pos=np.array([0, 0, 0])))
        self._line_plot = dict(a=gl.GLLinePlotItem(pos=np.array([[0, 0, 0]])))
        self._axes = gl.GLScatterPlotItem(pos=np.array([[1, 0, 0, ], [0, 1, 0], [0, 0, 1]]),
                                          color=np.array([(1, 0, 0, 1), (0, 1, 0, 1), (0, 0, 1, 1)]))
        self.widget_plot.addItem(self._plot['a'])
        self.widget_plot.addItem(self._line_plot['a'])
        self.widget_plot.addItem(self._axes)
        self.widget_plot.addItem(gl.GLGridItem())
        self.plot_colors = []
        self.plot_range = [0.0, 1.0]
        self.dock_plot.addWidget(self.widget_plot)

        self.max_draw_index = 0
        self.track = []
        self.track_l1_error = []
        self.previous_data_save = 0

        self.algorithm = algorithm
        QObject.connect(self.algorithm, SIGNAL('clear track'), self.clear_track, QtCore.Qt.QueuedConnection)
        QObject.connect(self.algorithm, SIGNAL('update'), self.update, QtCore.Qt.QueuedConnection)
        QObject.connect(self.algorithm, SIGNAL('show_text'), self.show_text, QtCore.Qt.QueuedConnection)

    def __str__(self):
        ret = '=== ' + self.__class__.__name__ + ' ==='
        ret += '\nno parameters'
        return ret

    # noinspection PyUnresolvedReferences
    @staticmethod
    def _create_spin(dock, title, value, bounds, step, connect_function):
        """
        Creates a QLabel and SpinBox and adds it to the dock

        :param dock: Dock to which this spinbox is added
        :type dock: Dock
        :param title: Description of the spinbox
        :type title: str
        :param value: Initial value of the spinbox
        :type value: float or int
        :param bounds: Bounds on the value of the spinbox
        :type bounds: Tuple of float or int
        :param step: Step size with which the value can be changed
        :type step: float or int
        :param connect_function: Function that is called when the spinbox changes. Receives the spinbox as the first
        parameter.
        :type connect_function: function
        """
        label = QtGui.QLabel(' ' + title)
        spin = pg.SpinBox(value=value, bounds=bounds, step=step)
        dock.addWidget(label)
        dock.addWidget(spin)
        spin.sigValueChanged.connect(connect_function)

    # noinspection PyUnresolvedReferences
    @staticmethod
    def _create_menu(dock, title, value, possible_values, connect_function):
        label = QtGui.QLabel(' ' + title)
        cb = QtGui.QComboBox()
        cb.addItems(possible_values)
        cb.setCurrentIndex([i for i, x in enumerate(possible_values) if x == value][0])
        dock.addWidget(label)
        dock.addWidget(cb)
        fun = lambda changed: connect_function(cb.currentIndex())
        cb.currentIndexChanged.connect(fun)

    def clear_track(self):
        """
        Remove the track history. Old projections are no longer displayed.
        """
        self.track = []

    def update(self):
        """
        Updates the GUI to the current state of the algorithm
        """
        track_2d = self.show_point_cloud()
        if track_2d is not None:
            self.show_image(track_2d)
        self.show_projections()
        self.show_tracks()
        self.show_texts()

    def show_texts(self):
        """
        Show text in the gui. Currently only the framerate.
        """
        self.show_text('framerate:\t' + str(round(1.0 / timer.tick('total'), 2)))

    def show_tracks(self):
        """
        Show the track of projections of the camera's in the GUI.
        """
        self.track.append(self.algorithm.world_proj)
        track0 = 0.01 * np.array(self.track)[1:, :, -1]
        """:type: ndarray"""
        self.show_3d_line(track0, (1., 0., 0., 0.8), 'track')
        if self.algorithm.kitti.available:
            track1 = 0.01 * np.array(self.algorithm.kitti.states)[:, :, -1]
            """:type: ndarray"""
            self.show_3d_line(track1, (0., 1., 0., 0.8), 'true')

    def show_image(self, track_2d):
        """
        Show image with the trajectories of the points drawed on it.

        :param track_2d: Nx7 array with the trajectories of the point (x0, y0, x1, y1, r, g, b)
        :type track_2d: ndarray
        """
        img = self.algorithm.prev_img_l
        if len(track_2d.shape) > 1:
            draw_color = track_2d[:, [6, 5, 4]] * 255
            """:type draw_color: ndarray"""
            img = Matcher.draw_lines(img, track_2d[:, :2], track_2d[:, 2:4], draw_color)
        # Show the images
        img = img[:, :, [2, 1, 0]]
        axes = [1, 0]
        if len(img.shape) > 2:
            axes = [1, 0, 2]
        self.widget_image.setImage(np.transpose(img, axes=axes))

    def show_projections(self):
        """
        Show the current left and right projection in the GUI
        """
        projection = self.algorithm.world_proj
        ex_r = self.algorithm.ex_r
        proj0 = (projection, np.array([1., 0., 0., 1.]))
        proj1 = (combine(projection, inverse(ex_r)), np.array([1., 1., 0., 1.]))
        for proj in [proj0, proj1]:
            self.show_projection(proj[0], proj[1])

    def show_projection(self, projection, color):
        """
        Show the projection in the GUI with a specific color

        :param projection: The projection to show in the 3D plot
        :type projection: ndarray
        :param color: The color to display the projection with
        :type color: tuple | ndarray
        """
        rotation, translation = decompose(projection)
        translation = translation.reshape(-1)

        # Show origin
        data = np.array([translation]) / 100.0
        """:type: ndarray"""
        self.show_3d_scatter(data, color, str(color))
        red = np.array([1., 0., 0., 1.])
        green = np.array([0., 1., 0., 1.])
        blue = np.array([0., 0., 1., 1.])
        # Show z axis
        self.show_3d_line(np.array([translation, (translation + rotation.dot(np.array([0., 0., 50.])))]) / 100.0,
                          np.array([green, green]), str(color) + 'z')
        # Show x axis
        self.show_3d_line(np.array([translation, (translation + rotation.dot(np.array([50., 0., 0.])))]) / 100.0,
                          np.array([red, red]), str(color) + 'x')
        # Show y axis
        self.show_3d_line(np.array([translation, (translation + rotation.dot(np.array([0., 50., 0.])))]) / 100.0,
                          np.array([blue, blue]), str(color) + 'y')

    # noinspection PyUnresolvedReferences
    def show_3d_line(self, data, color=None, plot='a'):
        """
        Shows a 3D line in the 3D plot.

        :param data: Nx3 array with the line to display [(x, y, z)].
        :type data: ndarray
        :param color: The color to display the line with. Also specify the alpha as a fourth element of the tuple.
        :type color: (float, float, float, float) | ndarray
        :param plot: The plot to show the 3D line in. By using a plot that already exists the previous data is erased.
        """
        if color is None:
            color = (1., 1., 1., .8)
        data = data[:, [0, 2, 1]]
        if plot not in self._line_plot:
            self._line_plot[plot] = gl.GLLinePlotItem(pos=data, color=color)
            self.widget_plot.addItem(self._line_plot[plot])
        else:
            self._line_plot[plot].setData(pos=data, color=color)

    def show_point_cloud(self):
        """
        Shows the 3D locations of points in the vocabulary.

        :return: The 2D image trajectories of points that are shown in the 3D plot.
        :rtype: ndarray
        """
        if not self.algorithm.properties['output points'].value == 'not':
            projection = self.algorithm.world_proj
            time_matcher = self.algorithm.time_matcher
            props = self.algorithm.properties
            tracks_3d = []
            track_2d = []
            points = []
            keys = time_matcher.vocabulary.information.keys()
            children = time_matcher.vocabulary.children
            for key in keys:
                if key not in children:
                    try:
                        time_updated = time_matcher.get_info('updated', key)
                        time_last = time_matcher.get_info('time', key)
                        is_show_all = props['output points'].value == 'all'
                        is_point_updated = props['output points'].value == 'updated' and time_updated == time_last
                        if is_show_all or is_point_updated:
                            values = time_matcher.vocabulary.information[key]
                            pos = values['mean'][:3] * 0.01
                            relative_height = inverse(projection)[1, :].dot(to_homogeneous(values['mean'][:3])) * 0.01
                            speed = np.linalg.norm(values['mean'][3:], 2)
                            img_track = values['debug']
                            track_keys = values['track'].keys()
                            track_keys.sort()
                            color = np.array([speed / 100.0, (relative_height + 2.0) / 20.0, .5, 1.0])
                            # noinspection PyUnresolvedReferences
                            color = np.maximum(np.minimum(color, 1.0), 0.0)
                            points.append(np.hstack((pos, color)))
                            new_track = []
                            for track_key in track_keys:
                                new_track.append(values['track'][track_key])
                            tracks_3d.append((np.array(new_track), color))
                            for img_track_i in range(1, len(img_track)):
                                track_2d.append(
                                    np.hstack((img_track[img_track_i][:2], img_track[img_track_i - 1][:2], color)))
                    except KeyError:
                        pass
            points = np.array(points)
            track_2d = np.array(track_2d)
            if shape(points, 0) > 0:
                self.show_3d_scatter(points[:, :3], points[:, 3:])

            for i in range(0, self.max_draw_index):
                self.clear_line_plot('line' + str(i))
            self.max_draw_index = 0
            for track_3d, color in tracks_3d:
                self.show_3d_line(np.array(track_3d) * 0.01, tuple(color.tolist()), 'line' + str(self.max_draw_index))
                self.max_draw_index += 1
            return track_2d
        return None

    # noinspection PyUnresolvedReferences
    def show_3d_scatter(self, data, color=None, plot='a'):
        """
        Create a 3D scatter in the 3D plot of the data.

        :param data: Nx3 array with point locations.
        :type data: ndarray
        :param color: The color of the points. Could be either specified once or for all point independently using a
            large Nx4 array.
        :type color: tuple | ndarray
        :param plot: The plot name to show the data in. If the plot already exists the previous data is erased.
        :type plot: str
        """
        if color is None:
            color = (0., 0., 1., .8)
        if plot not in self._plot:
            self._plot[plot] = gl.GLScatterPlotItem(pos=data[:, [0, 2, 1]], color=color)
            self.widget_plot.addItem(self._plot[plot])
        else:
            self._plot[plot].setData(pos=data[:, [0, 2, 1]], color=color)

    def clear_line_plot(self, plot):
        """
        Clears a line plot by setting the data to None

        :param plot: The name of the plot
        :type plot: str
        """
        if plot in self._line_plot:
            self._line_plot[plot].setData(pos=None)

    def show_text(self, text):
        """
        Shows the text in the GUI
        :param text: Texts to output in the gui
        :type text: str
        """
        self.text = text + '\n' + self.text
        self._text_output.setText(self.text)

    def is_visible(self):
        """
        :return: If the GUI window is open
        :rtype: bool
        """
        return self._win.isVisible()

    @staticmethod
    def plt_scatter(data, kwargs):
        """
        Creates a scatter plot with matplotlib using 2D data. Data can only be displayed in the GUI thread.

        :param data: Nx2 array with point data.
        :type data: ndarray
        :param kwargs: Other arguments that could be given to pyplot.scatter(): figure, subplot, color, marker,
            if cla() should be called and if draw() should be called.
        :type kwargs: dict[str, object]
        """
        figure = kwargs['figure'] if 'figure' in kwargs else 0
        sub_plot = kwargs['subplot'] if 'subplot' in kwargs else 111
        color = kwargs['color'] if 'color' in kwargs else 'b'
        marker = kwargs['marker'] if 'marker' in kwargs else 'o'
        cla = kwargs['cla'] if 'cla' in kwargs else False
        draw = kwargs['draw'] if 'draw' in kwargs else False
        plt.figure(figure)
        plt.subplot(sub_plot)
        plt.ion()
        if cla:
            plt.cla()
        plt.scatter(data[:, 0], data[:, 1], color=color, marker=marker)
        if draw:
            plt.draw()
            plt.pause(0.001)

    @staticmethod
    def plt_plot_line(data0, data1, kwargs):
        """
        Creates a line plot with matplotlib using 2D data. Data can only be displayed in the GUI thread.

        :param data0: Nx2 array with the from-locations
        :type data0: ndarray
        :param data1: Nx2 array with the to-locations
        :type data1: ndarray
        :param kwargs: Other arguments that could be given to pyplot.plot(): figure, subplot, color, if cla() should be
            called and if draw() should be called.
        :type kwargs: dict[str, object]
        """
        figure = kwargs['figure'] if 'figure' in kwargs else 0
        sub_plot = kwargs['subplot'] if 'subplot' in kwargs else 111
        color = kwargs['color'] if 'color' in kwargs else 'b'
        cla = kwargs['cla'] if 'cla' in kwargs else False
        draw = kwargs['draw'] if 'draw' in kwargs else False
        plt.figure(figure)
        plt.subplot(sub_plot)
        plt.ion()
        if cla:
            plt.cla()
        plt.plot([data0[:, 0], data1[:, 0]], [data0[:, 1], data1[:, 1]], color=color)
        if draw:
            plt.draw()
            plt.pause(0.001)

    @staticmethod
    def plt_imshow(data, kwargs):
        """
        Creates a image plot with matplotlib using 2D data. Data can only be displayed in the GUI thread.

        :param data: NxM(x3) array with the image data.
        :type data: ndarray
        :param kwargs: Other arguments that could be given to pyplot.plot(): figure, subplot, if cla() should be
            called, if draw() should be called and the cmap to use.
        :type kwargs: dict[str, object]
        """
        figure = kwargs['figure'] if 'figure' in kwargs else 0
        sub_plot = kwargs['subplot'] if 'subplot' in kwargs else 111
        cla = kwargs['cla'] if 'cla' in kwargs else False
        draw = kwargs['draw'] if 'draw' in kwargs else False
        cmap = kwargs['cmap'] if 'cmap' in kwargs else 'gray'
        interpolation = kwargs['interpolation'] if 'interpolation' in kwargs else 'none'
        plt.figure(figure)
        plt.subplot(sub_plot)
        plt.ion()
        if cla:
            plt.cla()
        plt.imshow(data, interpolation=interpolation, cmap=cmap)
        if draw:
            plt.draw()
            plt.pause(0.001)