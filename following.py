# coding=utf-8
"""
Matching of two sets of descriptions
"""
import abc

import cv2
import numpy as np

from projections import to_homogeneous, from_homogeneous
from vocabulary_tree import DynamicVocabularyTree, FixedVocabularyTree


__author__ = 'Pieter Marsman'


class Matcher(object):
    """
    Abstract class for description matching
    """

    def __init__(self, match_quality, max_vertical_difference, max_disparity, min_disparity, buckets):
        """
        Initializes criteria that are necessary to refine matches. Criteria that are None are not used.

        :param match_quality: Number between zero and one. Distance between the first match (feature in one frame and
        feature in other frame) should be at least match_quality times the distance between the second match. Note
        that this can only be used with knn matching.
        :type match_quality: float | None
        :param max_vertical_difference: Maximum vertical difference between the features. If horizontal epipolar
        lines are assumed this could be close to zero.
        :type max_vertical_difference: float | None
        :param max_disparity: Maximum horizontal difference between the features. If objects very close to the camera
        not likely this could be lower.
        :type max_disparity: float | None
        :param min_disparity: Minimum horizontal difference between the features. Depth estimation for disparities
        that are close to zero are not reliable and can therefore be discarded.
        :type min_disparity: float | None
        """
        # Refinement criteria
        self._match_quality = match_quality
        self._max_vertical_difference = max_vertical_difference
        self._max_disparity = max_disparity
        self._min_disparity = min_disparity
        self._buckets = buckets
        self.kept_average = np.zeros(8)
        self.keep_overlap = np.zeros((8, 8))
        self.keep_count = 0

    def __str__(self):
        ret = '=== ' + self.__class__.__name__ + ' ==='
        ret += '\nMatch quality: ' + str(self._match_quality)
        ret += '\nMax vertical difference: ' + str(self._max_vertical_difference)
        ret += '\nMax horizontal disparity: ' + str(self._max_disparity)
        ret += '\nMin horizontal disparity: ' + str(self._min_disparity)
        ret += '\nNumber of buckets: ' + str(self._buckets)
        return ret

    @abc.abstractmethod
    def match(self, descriptions0, descriptions1):
        """
        Matches two sets of descriptions

        :param descriptions0: NxM array of N descriptions of size M
        :type descriptions0: ndarray
        :param descriptions1: N'xM array of N descriptions of size M
        :type descriptions1: ndarray | None
        :raise NotImplementedError: this is an abstract method
        """
        raise NotImplementedError("Should have implemented this")

    @staticmethod
    def draw_keypoints(image, keypoints, color=None):
        """
        Draws the keypoints on the image

        :param image: the image
        :type image: ndarray
        :param keypoints: Nx2 array with keypoints
        :type keypoints: ndarray
        :param color: 3-tuple of 0-255 int's indicating the drawing color
        :type color: (int, int, int)
        :return: The image with the points drawn on them. The image is not copied, so actually the original image is
            changed.
        :rtype: ndarray
        """
        for kp in keypoints:
            if len(kp) > 2:
                size = kp[2]
            else:
                size = 5
            if color is None:
                color = tuple(np.random.randint(0, 255, 3).tolist())
            cv2.circle(image, (int(kp[0]), image.shape[0] - int(kp[1])), int(size), color=color)
        return image

    @staticmethod
    def draw_lines(image, from_location, to_location, color=None):
        """
        Draws lines on the image

        :param image: The image
        :type image: ndarray
        :param from_location: Nx2 array with starting location of the lines (x,y)
        :type from_location: ndarray
        :param to_location: Nx2 array with ending locations of the lines (x,y)
        :type to_location: ndarray
        :param color: Nx3 array with colors
        :type color: ndarray
        :return: The image with lines
        :rtype: ndarray
        """
        img = np.copy(image)
        if color is None:
            color = np.random.randint(0, 255, (from_location.shape[0], 3))
        for p1, p2, draw_color in zip(from_location, to_location, color):
            draw_color = tuple(draw_color.tolist())
            cv2.circle(img, (int(p1[0]), image.shape[0] - int(p1[1])), radius=1, color=draw_color, thickness=-1)
            cv2.circle(img, (int(p2[0]), image.shape[0] - int(p2[1])), radius=1, color=draw_color, thickness=-1)
            # And draw the line
            cv2.line(img, (int(p1[0]), image.shape[0] - int(p1[1])), (int(p2[0]), image.shape[0] - int(p2[1])),
                     draw_color)
        return img

    def refine_match(self, distances, keypoints0, keypoints1, descriptors0, descriptors1):
        """
        Refines the matches based on the chosen method. Assumes that the epipolar lines are horizontal. The results
        can be find in self.good_keypoints. The working depends on the value of match_quality, max_vertical_difference,
        min_disparity, negative_disparity and max_disparity.

        :param distances: List of size K with N-length arrays. The distances between the descriptions of the first K
        matches
        :type distances: list of ndarray
        :param keypoints0: Nx2 array with keypoint locations (x,y) in first image
        :type keypoints0: ndarray
        :param keypoints1: Mx2 array with keypoint locations (u,v) in second frame
        :type keypoints1: ndarray
        :param descriptors0: NxJ array with keypoint descriptors of size J in the first frame
        :type descriptors0: ndarray
        :param descriptors1: MxJ array with keypoint descriptors of size J in the second frame
        :type descriptors1: ndarray
        :return Tuple with as first element an array with the distances between the refined matches. The second
        element is an array with the refined keypoints (x,y,u,v). The third element is an array with the
        descriptors of those keypoints with size (L, 2xJ) where L < N and L < M.
        :rtype tuple of ndarray
        """
        matched_keypoints = []
        matched_descriptions = []
        matched_distances = []
        horizontal_buckets = None
        matches = np.hstack((distances[0].reshape((-1, 1)), distances[1].reshape((-1, 1)), keypoints0, keypoints1))
        # Use filters to select good matches
        keep_count = np.zeros((matches.shape[0], 6))
        for qid, (distance, distance2, kp10, kp11, _, _, kp20, kp21, _, _) in enumerate(matches):
            keep = np.array([True, True, True, True, True, True])
            if np.all(keep) and self._match_quality is not None:
                # Do not keep matches that are not that good compared to the second best match
                keep[0] = distance <= self._match_quality * distance2
            if np.all(keep) and self._max_disparity is not None:
                # Do not keep matches that have a large horizontal difference
                keep[2] = abs(kp10 - kp20) < self._max_disparity
            if np.all(keep) and self._min_disparity is not None:
                # Do not keep matches which disparity is rather small
                keep[3] = -(kp20 - kp10) > self._min_disparity
            if np.all(keep) and self._max_vertical_difference is not None:
                # Do not keep matches that have a large vertical difference
                keep[1] = abs(kp11 - kp21) < self._max_vertical_difference
            if np.all(keep) and self._buckets is not None:
                # Divide the image once into buckets
                if horizontal_buckets is None:
                    minimum = np.min(matches[:, 2:4], axis=0)
                    maximum = np.max(matches[:, 2:4], axis=0)
                    ratio = (maximum[0] - minimum[0]) / (maximum[1] - minimum[1])
                    vertical_buckets = int((self._buckets / ratio) ** 0.5)
                    horizontal_buckets = self._buckets / vertical_buckets
                    horizontal_buckets = np.linspace(minimum[0], maximum[0], horizontal_buckets)
                    vertical_buckets = np.linspace(minimum[1], maximum[1], vertical_buckets)
                # Get the coordinates of the bucket the keypoint is in
                xx_loc = np.where((kp10 <= horizontal_buckets[1:]) * (kp10 >= horizontal_buckets[:-1]))[0][0]
                yy_loc = np.where((kp11 <= vertical_buckets[1:]) * (kp11 >= vertical_buckets[:-1]))[0][0]
                # Get the other matches that are in the bucket
                horizontal_min = matches[:, 2] >= horizontal_buckets[xx_loc]
                horizontal_max = (matches[:, 2] <= horizontal_buckets[xx_loc + 1])
                vertical_min = matches[:, 3] >= vertical_buckets[yy_loc]
                vertical_max = matches[:, 3] <= vertical_buckets[yy_loc + 1]
                others = horizontal_min * horizontal_max * vertical_min * vertical_max
                # Check if the current match is the best
                # others = np.where(others)[0]
                keep[5] = np.all(matches[others, 0] >= distance)
            # Only keep matches that satisfy all criteria
            if np.all(keep):
                # y = 0.5 * (kp11 + kp21)
                matched_keypoints.append([kp10, kp11, kp20, kp21])
                matched_descriptions.append(np.array([descriptors0[qid, :], descriptors1[qid, :]]).reshape(-1))
                matched_distances.append([distance])
            keep_count[qid, :] = keep
        # Transform to array
        matched_keypoints = np.array(matched_keypoints)
        matched_descriptions = np.array(matched_descriptions)
        self.keep_count += 1
        return np.array(matched_distances), matched_keypoints, matched_descriptions

    def refine_match2(self, matches):
        """
        Refines matches based on Lowe's method. Checks if the first distance is at max match_quality of the second
        distance.

        :param matches: List of arrays with matches. The first item is an array with an index and distance. The second
        is the second closest neighbour index and distance.
        :return Tuple with as first element an array with the distances between the refined matches. The second
        element is an array with the refined keypoints (x,y,u,v). The third element is an array with the
        descriptors of those keypoints with size (L, 2xJ) where L < N and L < M.
        :rtype tuple of ndarray
        """
        if matches[0] is None or matches[1] is None:
            return None
        matched_indexes = []
        # Use filters to select good matches
        for qid, (tid0, distance0, tid1, distance1) in enumerate(np.hstack([x for x in matches])):
            keep = distance0 <= self._match_quality * distance1
            if keep:
                matched_indexes.append([qid, tid0])
        return np.array(matched_indexes).astype(np.int32)


class KnnMatcher(Matcher):
    """
    Matches feature descriptions
    """

    def __init__(self, match_quality=None, max_vertical_difference=None, max_disparity=None, min_disparity=None,
                 buckets=None):
        """
        Initializes the flann knn object

        :param match_quality: Number between zero and one. Distance between the first match (feature in one frame and
        feature in other frame) should be at least match_quality times the distance between the second match. Note
        that this can only be used with knn matching.
        :type match_quality: float | None
        :param max_vertical_difference: Maximum vertical difference between the features. If horizontal epipolar
        lines are assumed this could be close to zero.
        :type max_vertical_difference: float | None
        :param max_disparity: Maximum horizontal difference between the features. If objects very close to the camera
        not likely this could be lower.
        :type max_disparity: float | None
        :param min_disparity: Minimum horizontal difference between the features. Depth estimation for disparities
        that are close to zero are not reliable and can therefore be discarded.
        :type min_disparity: float | None
        """
        super(KnnMatcher, self).__init__(match_quality, max_vertical_difference, max_disparity, min_disparity, buckets)
        # Initialize a flann knn
        flann_index_tree = 0
        flann_index_parameters = dict(algorithm=flann_index_tree, trees=5)
        flann_search_params = dict(checks=50)
        self._flann = cv2.FlannBasedMatcher(flann_index_parameters, flann_search_params)

    def match(self, descriptions0, descriptions1):
        """
        Match the feature descriptors using the FLANN descriptor

        :param descriptions0: NxM array of N descriptions of size M
        :type descriptions0: ndarray
        :param descriptions1: N'xM array of N descriptions of size M
        :type descriptions1: ndarray
        :return list of matches. First element are the best matches, second element are the second-best matches.
        :rtype list of ndarray
        """
        matches = self._flann.knnMatch(descriptions0.astype(np.float32), descriptions1.astype(np.float32), k=2)
        best_matches = np.array([[m.trainIdx, m.distance] for (m, _) in matches])
        second_best_matches = np.array([[n.trainIdx, n.distance] for (_, n) in matches])
        return [best_matches, second_best_matches]


class BFMatcher(Matcher):
    """
    Brute force matching of descriptors.

    :param norm_type: The distance measure
    :type norm_type: int
    :param match_quality: Number between zero and one. Distance between the first match (feature in one frame and
    feature in other frame) should be at least match_quality times the distance between the second match. Note
    that this can only be used with knn matching.
    :type match_quality: float | None
    :param max_vertical_difference: Maximum vertical difference between the features. If horizontal epipolar
    lines are assumed this could be close to zero.
    :type max_vertical_difference: float | None
    :param max_disparity: Maximum horizontal difference between the features. If objects very close to the camera
    not likely this could be lower.
    :type max_disparity: float | None
    :param min_disparity: Minimum horizontal difference between the features. Depth estimation for disparities
    that are close to zero are not reliable and can therefore be discarded.
    :type min_disparity: float | None
    """

    def __init__(self, norm_type, match_quality=None, max_vertical_difference=None, max_disparity=None,
                 min_disparity=None, buckets=None):
        """
        Initializes the flann knn object

        :param match_quality: Number between zero and one. Distance between the first match (feature in one frame and
        feature in other frame) should be at least match_quality times the distance between the second match. Note
        that this can only be used with knn matching.
        :type match_quality: float | None
        :param max_vertical_difference: Maximum vertical difference between the features. If horizontal epipolar
        lines are assumed this could be close to zero.
        :type max_vertical_difference: float | None
        :param max_disparity: Maximum horizontal difference between the features. If objects very close to the camera
        not likely this could be lower.
        :type max_disparity: float | None
        :param min_disparity: Minimum horizontal difference between the features. Depth estimation for disparities
        that are close to zero are not reliable and can therefore be discarded.
        :type min_disparity: float | None
        """
        super(BFMatcher, self).__init__(match_quality, max_vertical_difference, max_disparity, min_disparity, buckets)
        self.bf = cv2.BFMatcher(norm_type)

    def match(self, descriptions0, descriptions1):
        """
        Match the feature descriptors using the FLANN descriptor

        :param descriptions0: NxM array of N descriptions of size M
        :type descriptions0: ndarray
        :param descriptions1: N'xM array of N descriptions of size M
        :type descriptions1: ndarray
        :return list of matches. First element are the best matches, second element are the second-best matches.
        :rtype list of ndarray
        """
        matches = self.bf.knnMatch(descriptions0.astype(np.uint8), descriptions1.astype(np.uint8), k=2)
        best_matches = np.array([[m.trainIdx, m.distance] for (m, _) in matches])
        second_best_matches = np.array([[n.trainIdx, n.distance] for (_, n) in matches])
        return [best_matches, second_best_matches]


class VocabularyMatcher(Matcher):
    """
    Matches descriptions using a vocabulary tree

    :param k: The number of nearest neighbours to find
    :type k: int
    :param distance_measure: The distance measure to use for the vocabulary tree. See the documentation of the
    vocabulary tree.
    :type distance_measure: str
    :param match_quality: Number between zero and one. Distance between the first match (feature in one frame and
    feature in other frame) should be at least match_quality times the distance between the second match. Note
    that this can only be used with knn matching.
    :type match_quality: float | None
    :param max_vertical_difference: Maximum vertical difference between the features. If horizontal epipolar
    lines are assumed this could be close to zero.
    :type max_vertical_difference: float | None
    :param max_disparity: Maximum horizontal difference between the features. If objects very close to the camera
    not likely this could be lower.
    :type max_disparity: float | None
    :param min_disparity: Minimum horizontal difference between the features. Depth estimation for disparities
    that are close to zero are not reliable and can therefore be discarded.
    :type min_disparity: float | None
    """

    def __init__(self, k, distance_measure, branch_factor=3, match_quality=None, max_vertical_difference=None,
                 max_disparity=None, min_disparity=None, buckets=None, load_tree=None):
        super(VocabularyMatcher, self).__init__(match_quality, max_vertical_difference, max_disparity, min_disparity,
                                                buckets)
        self.branch_factor = branch_factor
        self.distance_measure = distance_measure
        if load_tree is None:
            self.vocabulary = DynamicVocabularyTree(self.branch_factor, self.distance_measure)
        else:
            self.vocabulary = FixedVocabularyTree.load(load_tree)
        self.k = k

    def add_features(self, information, descriptions):
        """
        Adds keypoints and descriptions to the vocabulary tree

        :param information: Nx4 array with keypoints
        :type information: list of dict[str, ndarray]
        :param descriptions: NxM array with descriptions
        :type descriptions: ndarray
        """
        self.vocabulary.add_words(descriptions, information)

    def clear(self):
        """
        Empties the vocabulary tree
        """
        self.vocabulary = DynamicVocabularyTree(self.branch_factor, self.distance_measure)

    def match(self, descriptors0, descriptors1=None, f=None):
        """
        Match the descriptions to the descriptions in the vocabulary tree

        :param descriptors0: NxM array with the descriptions
        :type descriptors0: ndarray
        :param descriptors1: Not needed in this override method. Should be None.
        :type descriptors1: None
        :param f: Filter on the vocabulary items. Receives a dictionary with information and should return a bool
        :type f: function
        :return: List of the neighbours of the descriptions in the vocabulary tree. First item of the list are the
        nearest neighbours, second item second nearest neighbours etc.
        :rtype: List of ndarray
        """
        neighbours = []
        for i in range(0, self.k):
            neighbours.append([])
        for descriptor in descriptors0:
            near, _ = self.vocabulary.get_closest(descriptor, self.k, f=f)
            if len(near) < 1:
                return [None, None]
            else:
                near.sort(key=lambda x: x[1])
                for i, (n, dist) in enumerate(near):
                    neighbours[i].append([n, dist])
        neighbours = [np.array(n) for n in neighbours]
        if all(map(lambda arr: arr.shape[0] > 0, neighbours)):
            return neighbours
        else:
            return [None, None]

    def get_information(self, information_key, indexes=None):
        """
        Returns the information of the nodes in the tree. If the indexes are not specified the information on the leaf
        nodes is returned.

        :param information_key: The key of the information dictionary
        :param indexes: The nodes
        :return: NxM array with information from N nodes and the indexes of those nodes (in case indexes is given,
        it just returns indexes).
        :rtype: (ndarray, ndarray) | ndarray
        """
        if indexes is None:
            info, keys = self.vocabulary.get_info_leafs(information_key)
            return info, keys
        else:
            return self.vocabulary.get_info(indexes, information_key)

    def get_info(self, information_key, index):
        """
        Single lookup for information with index and information_key in the vocabulary tree. The information is not
        translated to a ndarray.

        :param information_key: The key of the information dictionary.
        :type information_key: object
        :param index: The index of the object to get the information from.
        :type index: int
        :return: Information about the object. Could be anything that is saved in the vocabulary tree as information.
        :rtype: object
        """
        return self.vocabulary.information[index][information_key]

    def remove_old_node(self, now, duration=10.0):
        """
        Remove nodes from the vocabulary that are older than duration

        :param now: The current time
        :type now: float
        :param duration: The duration
        :type duration: float
        """
        criteria = lambda info: info['updated'] + duration < now
        self.vocabulary.remove(criteria)

    def size(self):
        """
        The number of nodes in the dictionary that have information. Does not count the intermediate nodes used for
            clustering.

        :return: The size of the vocabulary.
        :rtype: int
        """
        return len(self.vocabulary.information)


def track_3d(points, previous_gray, current_gray, projection, intrinsic):
    """
    Track a 3d point to the next frame. First the point is projected towards the image, then it is tracked.

    :param points: Nx3 array with points
    :type points: ndarray
    :param previous_gray: The previous gray image from were the points are tracked
    :type points: ndarray
    :param current_gray: The current gray image to were the points are tracked
    :type points: ndarray
    :param projection: 3x4 array with the projection from the 3D space to the current gray image
    :type points: ndarray
    :param intrinsic: 3x3 array with the intrinsic values of the camera
    :type points: ndarray
    :return: The 2D location of the points that could be tracked in the current gray frame, the indexes of these
        points in the original array with 3D points and the tracking errors.
    :rtype: (ndarray, ndarray, ndarray)
    """
    points = projection.dot(to_homogeneous(points).T)
    points_2d_0 = np.asarray(from_homogeneous(intrinsic.dot(points).T).reshape((-1, 1, 2)), np.float32)
    points_2d_1, indexes, errors = track(points_2d_0, previous_gray, current_gray)
    return points_2d_1, indexes, errors


def track(points0, previous_gray, current_gray, win_size=21, max_error=5.0, min_eig=0.001):
    # Calculate optical flow for the points
    """
    Track 2d points from one image to the next. Note: not all points can be tracked and thus the indexes should be
        used to identify which 2D points are tracked to the next frame.

    :param points0: Nx2 array with points
    :param previous_gray: The previous gray image from were the points are tracked
    :param current_gray: The current gray image to were the points are tracked
    :param win_size: The size of the patch window at each pyramidal level.
    :param max_error: Maximum l1 distance between the patches
    :param min_eig: Minimum eigenvalue of the 2x2 optical flow matrix of the patch. Used to discard patches without
        texture beforehand.
    :return: The 2D location of the points that could be tracked in the current gray frame, the indexes of these
        points in the original array with 3D points and the tracking errors.
    :rtype: (ndarray, ndarray, ndarray)
    """
    # todo cleanup
    args = dict(minEigThreshold=min_eig, winSize=(win_size, win_size))
    pts0 = np.copy(points0)
    pts0[:, 1] = previous_gray.shape[0] - pts0[:, 1]
    pts0 = np.asarray(pts0.reshape((-1, 1, 2)), np.float32)
    pts1, tracked_mask, errors = cv2.calcOpticalFlowPyrLK(previous_gray, current_gray, pts0, **args)
    in_image_mask = (pts0[:, :, 0] > 0) * (pts0[:, :, 0] < current_gray.shape[1]) * (pts1[:, :, 1] > 0) * (
        pts1[:, :, 1] < current_gray.shape[0])
    low_error_mask = errors < max_error
    mask = tracked_mask * in_image_mask * low_error_mask

    # Only save the points that were tracked, have a low error and are in the image
    # noinspection PyUnresolvedReferences
    indexes = np.where(mask.reshape(-1))[0]
    pts1 = pts1.reshape((-1, 2))[indexes, :]
    pts1[:, 1] = previous_gray.shape[0] - pts1[:, 1]
    return pts1, indexes, errors.reshape((-1))[indexes]