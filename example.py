# coding=utf-8
"""
Example calls to the algorithm
"""
import time
import sys

from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import SIGNAL
import cv2

from algorithm import Algorithm, AlgorithmProperty
from gui import Gui
from camera import Camera, StereoCamera


__author__ = 'Pieter Marsman'


def example_algorithm():
    """
    Runs the algorithm and shows the results in the GUI
    """
    properties = AlgorithmProperty.get_properties()
    alg = Algorithm(properties)
    # Initialize gui and keep reference to gui
    app = QtGui.QApplication(sys.argv)
    # noinspection PyUnusedLocal
    gui = Gui(properties, alg)
    # Start the algorithm and gui
    alg.start()
    app.exec_()
    # The gui stopped, also stop the algorithm
    alg.running = False
    # Give the algorithm time to shut down
    time.sleep(3)


def example_stereo():
    """
    Calibrates two camera's. Either uses earlier images from the disk or new images of the chessboard.
    """
    cam_l = Camera.get_default_camera(0, 500, 0, 0.5, 0)
    cam_r = Camera.get_default_camera(1, 500, 0, 0.5, 0)
    cam = StereoCamera(cam_l, cam_r)
    cam.calibrate()
    (img_l, lab_l), (img_r, lab_r) = cam.get_stereo_image()
    cv2.imshow('Left', img_l)
    cv2.imshow('Right', img_r)
    cv2.waitKey(0)


example_algorithm()