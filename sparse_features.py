# coding=utf-8
"""
Detection of sparse features
"""
import abc

import cv2
import numpy as np

__author__ = 'Pieter Marsman'


class SparseFeatureDetector(object):
    """
    Parent class of sparse matcher. Provides drawing method
    """

    def __init__(self, description_length, amount):
        """
        Initializes some containers
        :param description_length: The description length of the sparse features
        :type description_length: int
        """
        self.description_length = description_length
        self.number_of_features = amount

    @abc.abstractmethod
    def detect_and_describe(self, image):
        """
        :param image: Detects features in the image
        :raise NotImplementedError: Another class should override this method
        """
        raise NotImplementedError("This should be implemented by another class")


class FeatureDetector(SparseFeatureDetector):
    """
    Feature matcher with:
    - feature detectors: 'sift', 'surf', 'surf2' (using upright keypoints), 'orb', 'good'
    - feature descriptor: 'sift', 'surf', 'orb', 'freak'
    """

    def __init__(self, detector='sift', descriptor=None, amount=500):
        if descriptor is None:
            descriptor = detector
        self.feature_detector_type = detector
        # Initialize the descriptors
        if descriptor is 'sift':
            super(FeatureDetector, self).__init__(128, amount)
            self._feature_descriptor = cv2.SIFT()
        elif descriptor is 'orb':
            super(FeatureDetector, self).__init__(32, amount)
            self._feature_descriptor = cv2.ORB()
        elif descriptor is 'freak':
            super(FeatureDetector, self).__init__(128, amount)
            self._feature_descriptor = cv2.DescriptorExtractor_create('FREAK')
        elif descriptor is 'surf':
            super(FeatureDetector, self).__init__(128, amount)
            self._feature_descriptor = cv2.SURF(hessianThreshold=500, extended=True, upright=False)
        else:
            raise AttributeError('Feature descriptor type ' + str(descriptor) + ' is not supported')
        # Initialize the detectors
        if detector is 'sift':
            self._feature_detector = cv2.SIFT(nfeatures=self.number_of_features)
        elif detector is 'orb':
            self._feature_detector = cv2.ORB(nfeatures=self.number_of_features)
        elif detector is 'surf':
            self._feature_detector = cv2.SURF(hessianThreshold=500, extended=True, upright=False)
        elif detector is 'surf2':
            self._feature_detector = cv2.SURF(hessianThreshold=500, upright=True)
        elif detector is 'good':
            pass
        else:
            raise AttributeError('Feature detector type ' + str(detector) + ' is not supported')

    def __str__(self):
        ret = '=== ' + self.__class__.__name__ + ' ==='
        ret += '\nFeature type: ' + str(self.feature_detector_type)
        ret += '\nNumber of features: ' + str(self.number_of_features)
        return ret

    def detect_and_describe(self, image):
        """
        Detects features in the image
        :param image: The image
        :type image: ndarray
        :return: Nx3 array with keypoints (x,y,size), a NxM array with M sized descriptors
        :rtype: (ndarray, ndarray)
        """
        # Detect the keypoints
        kps_all = []
        if self.feature_detector_type in ['sift', 'orb', 'surf', 'surf2']:
            kps_all = self._feature_detector.detect(image, None)
            # Sometimes there is one keypoint to much
            kps_all = kps_all[:self.number_of_features]
        elif self.feature_detector_type is 'good':
            from algorithm import AlgorithmProperty

            corners = cv2.goodFeaturesToTrack(image, maxCorners=self.number_of_features,
                                              qualityLevel=AlgorithmProperty.GOOD_FEATURES_QUALITY,
                                              minDistance=AlgorithmProperty.GOOD_FEATURES_DISTANCE)
            kps_all = [cv2.KeyPoint(kp[0], kp[1], 5) for kp in corners[:, 0, :]]
        # Describe the keypoints
        kps_described, desc = self._feature_descriptor.compute(image, kps_all)
        # Note: the y-axis is flipped
        kps_described = map(
            lambda keypoint: [keypoint.pt[0], image.shape[0] - keypoint.pt[1], keypoint.size, keypoint.angle],
            kps_described)

        return np.array(kps_described), desc


class ChessboardDetector(SparseFeatureDetector):
    """
    Finds chessboard corners in the image and use these as keypoints
    """

    def __init__(self, chessboard_size):
        """
        Initializes the variables, specifically
        :param chessboard_size: (k,n) number of inlier corners of the chessboard in two directions.
        :type chessboard_size: tuple of int
        """
        super(ChessboardDetector, self).__init__(2, np.prod(chessboard_size))
        self._sub_pixel_criteria = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 100, 1e-5)
        self._chessboard_size = chessboard_size

    def __str__(self):
        ret = super(ChessboardDetector, self).__str__()
        ret += '\nChessboard size: ' + str(self._chessboard_size)
        return ret

    def detect_and_describe(self, image):
        # Find corners in two images
        """
        Detect chessboard corners in the image returns them as keypoint and descriptors
        :param image: the image
        :return: The keypoints and the descriptors
        :rtype: (ndarray, ndarray)
        """
        ret, kp = cv2.findChessboardCorners(image, self._chessboard_size, None)
        # If corners are found in both images enhance their position
        if ret:
            # Find the subpixel precision of the corners
            cv2.cornerSubPix(image, kp, (5, 5), (-1, -1), self._sub_pixel_criteria)
            # Create array of keypoints
            kp = kp.reshape((-1, 2))
            # Generate 'description' of the keypoints: their location on the chessboard
            x = range(0, self._chessboard_size[0])
            y = range(0, self._chessboard_size[1])
            xx, yy = np.meshgrid(x, y)
            xy = np.hstack((xx.reshape((-1, 1)), yy.reshape((-1, 1))))
            desc = np.append(xy, xy, axis=1)
            kp = np.hstack((kp[:, 0].reshape((-1, 1)), image.shape[0] - kp[:, 1].reshape((-1, 1))))
        # If not corners found in both images return empty arrays
        else:
            kp = np.array([])
            desc = np.array([])
        return kp, desc