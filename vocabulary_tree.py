# coding=utf-8
"""
Implementations of vocabulary trees that are build during runtime (dynamic) or in front (fixed)
"""
import time

import cv2
import numpy as np
from matplotlib import pyplot as plt

from miscellaneous import k_median, create_directory


__author__ = 'Pieter Marsman'


class DynamicVocabularyTree(object):
    """
    Implementation of a vocabulary tree that allows O(log n) adding and retrieval of words in a tree of size n.

    :param k: Branching factor
    :type k: int
    """

    def __init__(self, k, distance_measure='rmse', words=None, children=None, parent=None):
        self.k = k
        self.distance_measure = distance_measure
        self.bf = cv2.BFMatcher(cv2.NORM_HAMMING)
        if words is None:
            words = dict()
        self.words = words
        if children is None:
            children = dict()
        self.children = children
        if parent is None:
            parent = dict()
        self.parent = parent
        self.information = dict()
        self.index = max(self.words.keys()) + 1 if len(self.words.keys()) > 0 else 0

    def __str__(self):
        return DynamicVocabularyTree.to_string(self.children)

    @staticmethod
    def to_string(tree_dict, indent=0, children=None):
        """
        Recursively creates a string of a tree dictionary
        :param tree_dict: The dictionary
        :type tree_dict: dict[int, [int]]
        :param indent: The current indent
        :type indent: int
        :param children: The indices of the children that should be added
        :type children: list of int
        :return: A string representation of the tree
        :rtype: str
        """
        if children is None:
            children = [0]
        ret = ''
        for child in children:
            ret += '|' * indent + str(child) + '\n'
            if child in tree_dict:
                ret += DynamicVocabularyTree.to_string(tree_dict, indent + 1, tree_dict[child])
        return ret

    @staticmethod
    def show_2d_tree(tree_dict, words, node=0):
        """
        Show a vocabulary tree that is created with words of 2 dimensions. Can be used for debugging purposes.

        :param tree_dict: The children dictionary of the vocabulary
        :type tree_dict: dict[int, [int]]
        :param words: The words of the vocabulary
        :type words: dict[int, ndarray]
        :param node: The start node. By default it is the root node (0).
        :type node: int
        :return: The height of the added node. Used internally to show different heights of the tree with different
            alphas.
        :rtype: int
        """
        level = 0
        if node in tree_dict:
            parent_data = None
            if node in words:
                parent_data = words[node]
            for child in tree_dict[node]:
                level = max(DynamicVocabularyTree.show_2d_tree(tree_dict, words, child), level)
                data = words[child]
                alpha = min(1.0, level * 0.1)
                color = 'b' if level % 2 == 0 else 'g'
                plt.scatter(data[0], data[1], alpha=alpha, color=color)
                if parent_data is not None:
                    plt.plot([data[0], parent_data[0]], [data[1], parent_data[1]], color=color, alpha=alpha)
        return level + 1

    def add_words(self, words, information=None):
        """
        Adds words to the vocabulary tree
        :param words: NxM array of N words with M letters
        :type words: ndarray
        :param information: NxM array of M sized information for N words
        :type information: list of dict[str, ndarray]
        """
        if information is None:
            information = [None] * words.shape[0]
        assert len(information) == words.shape[0]
        for i, info in enumerate(information):
            self.add_word(words[i, :], info)

    def add_word(self, word, info=None, node=0):
        """
        Adds a single word to the vocabulary
        :param word: A single word
        :type word: ndarray
        :param info: Information dictionary about the word
        :type info: dict[str, ndarray]
        :param node: The node to which it should be added
        :type node: int
        """
        if info is None:
            info = dict()
        # If the node is added beneath a node that already has children
        if node in self.children:
            # Add them if there is room
            if len(self.children[node]) < self.k:
                self.words[self.index] = word
                self.information[self.index] = info
                self.children[node].append(self.index)
                self.parent[self.index] = node
                self.index += 1
            # Else add them to the best child
            else:
                best_child, _ = self.get_best_child(word, node)
                self.add_word(word, info, best_child)
        # If the node does not have children: expand the current node, but keep the indexing intact
        else:
            if self.index is 0:
                self.children[self.index] = []
            else:
                # Remove node from parents
                parent = self.parent[node]
                self.children[parent].remove(node)
                # Create new intermediate node with same content as the to-be-expanded node
                self.children[parent].append(self.index)
                self.words[self.index] = self.words[node]
                self.information[self.index] = self.information[node]
                self.parent[self.index] = parent
                # Add the to-be-expanded node as a child
                self.children[self.index] = [node]
                # Add the new node as parent of the to-be-expanded node
                self.parent[node] = self.index
            self.index += 1
            # Add the new child
            self.add_word(word, info, self.index - 1)

    def get_info(self, indices, information_key):
        """
        Returns the information about a list of nodes
        :param information_key: The information to retrieve from the dictionary
        :param indices: List of node indices
        :type indices: list of int
        :return: NxM array with M-sized information about N nodes
        :rtype: ndarray
        """
        information = []
        for index in indices:
            information.append(self.information[index][information_key])
        return np.array(information)

    def get_info_leafs(self, information_key):
        """
        Get specific information about all the leaf nodes
        :param information_key: The type of specific information. It is assumed that all leaf nodes have this
        information type.
        :type information_key: str
        :return: Array with the information
        :rtype: ndarray
        """
        information = []
        keys = []
        for key, value in self.information.iteritems():
            if key not in self.children:
                information.append(value[information_key])
                keys.append(key)
        return np.array(information), keys

    def get_words(self, indices):
        """
        Get the words at the particular nodes
        :param indices: The nodes
        :return: NxM array with N words with M letters
        :rtype: ndarray
        """
        words = []
        for index in indices:
            words.append(self.words[index])
        return np.array(words)

    def get_closest(self, query_word, neighbours=1, node=0, f=None):
        """
        Returns the closest words to the query word

        :param query_word: The word that is searched for in the tree
        :type query_word: ndarray
        :param neighbours: The number of neighbours that should be found
        :type neighbours: int
        :param node: The node in which the query should be searched
        :type node: int
        :param f: A filter function that receives the information of the node and returns a bool
        :type f: function
        :return: A list with the indices of the closest nodes
        :rtype: (list of [int, float], list of int)
        """
        ret = []
        visited = []
        if f is None:
            f = lambda x: True
        # If the vocabulary is empty
        if len(self.words) < neighbours:
            return [], []
        # If the node has children
        if node in self.children:
            while len(ret) < neighbours:
                best_child, best = self.get_best_child(query_word, node, visited)
                if best_child is None:
                    visited.append(node)
                    return ret, visited
                else:
                    ret2, visited2 = self.get_closest(query_word, neighbours - len(ret), best_child, f=f)
                    ret.extend(ret2)
                    visited.extend(visited2)
            return ret, visited
        # If the node has no children the node is the closest
        else:
            if f(self.information[node]):
                _, measure = self.get_best(query_word.reshape((1, -1)), self.words[node].reshape((1, -1)))
                return [[node, measure]], [node]
            else:
                return [], [node]

    def get_best_child(self, query_word, node, not_consider=None):
        """
        Returns the best child of the node compared to the query word.
        :param query_word: The query word
        :type query_word: ndarray
        :param node: The index of the node which child's should be considered
        :type node: int
        :param not_consider: Particular nodes that should not be considered
        :type not_consider: list of int
        :return: The index of the best child and the distance to that child
        :rtype: (int, float)
        """
        if not not_consider:
            not_consider = []
        childes = []
        descriptions = []
        for child in self.children[node]:
            if child not in not_consider:
                childes.append(child)
                descriptions.append(self.words[child])
        if len(descriptions) > 0:
            index, measure = self.get_best(query_word, np.array(descriptions))
            return childes[index], measure
        else:
            return None, None

    def get_best(self, query_word, descriptions):
        """
        Returns the best description compared to the query word
        :param query_word: N-sized query word
        :type query_word: ndarray
        :param descriptions: NxM array with descriptions
        :type descriptions: ndarray
        :return: The index of the best description and the distance measure to that description
        :rtype: (int, float)
        :raise AttributeError: If a distance measure is not supported
        """
        if self.distance_measure == 'rmse':
            distance = np.sqrt(
                np.mean((np.repeat(query_word.reshape((1, -1)), descriptions.shape[0], axis=0) - descriptions) ** 2,
                        axis=1))
            best = np.argmin(distance)
            return best, distance[best]
        elif self.distance_measure == 'hamming':
            matches = self.bf.match(query_word.reshape((1, -1)).astype(np.uint8), descriptions.astype(np.uint8))
            return matches[0].trainIdx, matches[0].distance
        elif self.distance_measure == 'dot':
            similarity = descriptions.dot(query_word.reshape((-1, 1)))
            best = np.argmax(similarity)
            return best, similarity[best]
        else:
            raise AttributeError('Distance measure ' + str(self.distance_measure) + ' is not supported')

    def remove(self, criteria, node=0):
        """
        Remove all the nodes based on a criteria.
        :param criteria: Function that receives the information of a node and return a boolean.
        :type criteria: function
        :param node: The node where to start. Default is the root of the tree.
        :type node: int
        :return: If the start node has been removed or not
        :rtype: bool
        """
        if node in self.children:
            for child in self.children[node]:
                child_removed = self.remove(criteria, child)
                if child_removed:
                    self.children[node] = [x for x in self.children[node] if x != child]
            if len(self.children[node]) < 1 and node is not 0:
                self.delete_node(node)
                return True
            else:
                return False
        else:
            if node in self.information and criteria(self.information[node]) and node is not 0:
                self.delete_node(node)
                return True
            else:
                return False

    def delete_node(self, node):
        """
        Delete a specific node from the vocabulary. It is assumed that it does not have children.

        :param node: The node to delete
        :type node: int
        :raise Exception: If the root node is tried to be deleted.
        """
        assert len(self.children[node]) == 0 if node in self.children else True
        if node == 0:
            raise Exception('Should not erase root node')
        del self.words[node]
        self.children[self.parent[node]].remove(node)
        del self.parent[node]
        if node in self.information:
            del self.information[node]
        if node in self.children:
            del self.children[node]

    def prune_tree(self, depth, node=0):
        """
        Deletes all nodes that are deeper that a certain depth.

        :param depth: The maximum depth of the tree.
        :type depth: int
        :param node: The start node. Is used internally to iterate through the tree.
        :type node: int
        """
        if node in self.children:
            for child in self.children[node][:]:
                self.prune_tree(depth - 1, child)
        if depth < 0:
            self.delete_node(node)


class FixedVocabularyTree(DynamicVocabularyTree):
    """
    A fixed tree that add new nodes at the leafs, but does not create new intermediate nodes.

    :param k: The branching factor
    :type k: int
    :param words: The words that are in the tree
    :type words: dict[int, ndarray]
    :param children: The children dictionary that create the structure of the tree.
    :type children: dict[int, [int]]
    :param parent: The parent of each node
    :type parent: dict[int, int]
    :param distance_measure: A distance measure to use when comparing words. Can be either 'rmse', 'hamming' or 'dot'
    :type distance_measure: str
    """

    def __init__(self, k, words, children, parent, distance_measure='rmse'):
        super(FixedVocabularyTree, self).__init__(k, distance_measure, words, children, parent)
        self.nodes = dict()
        for key in self.words:
            self.nodes[key] = True
        self.nodes[0] = True

    def add_word(self, word, info=None, node=0):
        """
        Add a word as leaf node to the tree. At each intermediate node add the new word under the child that has the
            smallest distance to the new word.

        :param word: The new word.
        :type word: ndarray
        :param info: The info about this new word.
        :type info: dict[str, object]
        :param node: The node to add this word under. Is used internally to iterate through the tree.
        :type node: int
        """
        if info is None:
            info = dict()
        # If the node is not a leaf, go further down the tree
        best_child, _ = self.get_best_child(word, node)
        if best_child in self.nodes:
            self.add_word(word, info, best_child)
        # If the node does not have children: add the new node as a leaf
        else:
            self.children[node].append(self.index)
            self.words[self.index] = word
            self.information[self.index] = info
            self.parent[self.index] = node
            self.index += 1

    def get_closest(self, query_word, neighbours=1, node=0, f=None, max_visited=50):
        """
        Get the leaf node that is closest to the query word. If neighbours > 1, multiple closest nodes are returned.

        :param query_word: The word that the returned node should be closest to.
         :type query_word: ndarray
        :param neighbours: The number of closest nodes to return
        :type neighbours: int
        :param node: The start node. Used internally to iterate through the tree.
        :type node: int
        :param f: A filter to discard nodes based on their information. Receives the information dictionary and
            returns a bool.
        :type f: function
        :param max_visited: Maximum on the number of visited nodes before returning the result with less nodes than
            neighbours. Prevents searching the whole tree.
        :return: A list with nearest neighbours and the distance to that neighbour & a list with visited nodes (for
        recursive purposes)
        :rtype: (list of [int, int], [int])
        """
        ret = []
        visited = []
        if f is None:
            f = lambda x: True
        # If the vocabulary is empty
        if len(self.words) < neighbours:
            return [], []
        # If it is a node
        if node in self.nodes:
            while len(ret) < neighbours:
                best_child, best = self.get_best_child(query_word, node, visited)
                if best_child is None:
                    visited.append(node)
                    return ret, visited
                else:
                    ret2, visited2 = self.get_closest(query_word, neighbours - len(ret), best_child, f=f)
                    ret.extend(ret2)
                    visited.extend(visited2)
            return ret, visited
        # If it is a data point
        else:
            if f(self.information[node]):
                _, measure = self.get_best(query_word.reshape((1, -1)), self.words[node].reshape((1, -1)))
                return [[node, measure]], [node]
            else:
                return [], [node]

    def remove(self, criteria, node=0):
        """
        Removes nodes from the tree based on a criteria.

        :param criteria: Function that receives the information and should return a bool.
         :type criteria: function
        :param node: The start node. Is used internally to iterate through the tree.
        :type node: int
        """
        if node in self.nodes:
            for child in self.children[node]:
                self.remove(criteria, child)
        else:
            if criteria(self.information[node]) and node is not 0:
                self.delete_node(node)

    def save(self, folder):
        """
        Save this tree to a folder. Later the tree can be loaded again. Useful for creating static trees on
            beforehand.

        :param folder: Path to a folder
         :type folder: str
        """
        create_directory(folder)
        np.save(folder + '/words', self.words)
        np.save(folder + '/children', self.children)
        np.save(folder + '/parent', self.parent)
        np.save(folder + '/param', dict(k=self.k, measure=self.distance_measure))

    @staticmethod
    def load(folder):
        """
        Load a vocabulary tree from a folder.

        :param folder: The path to the folder where the vocabulary tree is saved.
         :type folder: str
        :return: A fixed vocabulary tree that was saved earlier.
        :rtype: FixedVocabularyTree
        """
        words = np.load(folder + '/words.npy').item()
        children = np.load(folder + '/children.npy').item()
        parent = np.load(folder + '/parent.npy').item()
        param = np.load(folder + '/param.npy').item()
        return FixedVocabularyTree(param['k'], words, children, parent, param['measure'])

    @staticmethod
    def train_binary_vocabulary_tree(descriptions):
        """
        Trains a binary fixed vocabulary tree based on the descriptions. A k-medians is used to split the descriptions
            into subsets.

        :param descriptions: NxM array with N words of M features.
         :type descriptions: ndarray
        """
        bf = cv2.BFMatcher(cv2.NORM_HAMMING)
        f_cluster = lambda data, centroid: np.array(
            [[x.trainIdx, x.distance] for x in bf.match(data.astype(np.uint8), centroid.astype(np.uint8))])
        f_cluster_2 = lambda data0, data1: np.array(
            [[x.trainIdx, x.distance] for _, x in bf.knnMatch(data0.astype(np.uint8), data1.astype(np.uint8), k=2)])
        voc_tree = FixedVocabularyTree.train_vocabulary_tree(descriptions, f_cluster, f_cluster_2, 'hamming', 3)
        # Saving and loading tree tree
        voc_tree.save('model/tree/' + str(int(time.time())) + '/')

    @staticmethod
    def train_vocabulary_tree(data, f_cluster, f_cluster_2, distance_measure, branching_factor=3):
        """
        Train an arbitrary vocabulary tree with a specific clustering and voting method.

        :param data: NxM array with N data points of M features to construct the tree
        :type data: ndarray
        :param f_cluster: A function to assign data points to centroid. Receives NxM array with data points and KxM
            array with centroid. Returns N-array indicating to which centroid each data point is assigned.
        :type f_cluster: function
        :param f_cluster_2: A function to vote for new centroid. Receives a NxM array with possible centroids and a
            KxL array with data. The second array votes for centroids in the first array. Returns a N-array with votes
            for each possible centroid.
        :type f_cluster_2: function
        :param distance_measure: The distance measure to use
        :type distance_measure: str
        :param branching_factor: The branching factor to use
        :type branching_factor: int
        :return: :rtype:
        """
        args = (data, f_cluster, f_cluster_2, branching_factor)
        words, children, parent, _ = FixedVocabularyTree._train_vocabulary_tree(*args)
        return FixedVocabularyTree(branching_factor, words, children, parent, distance_measure)

    @staticmethod
    def _train_vocabulary_tree(data, f_cluster, f_cluster_2, branching_factor=3, node=0):
        words = dict()
        children = dict()
        parent = dict()
        parent_node = node
        children[parent_node] = []
        if data.shape[0] > branching_factor:
            print 'processing data with shape', data.shape
            centroid, labels = k_median(data, branching_factor, f_cluster, f_cluster_2)
            unique_labels = np.unique(labels)
            if unique_labels.shape[0] > 1:
                for i, row in enumerate(centroid):
                    node += 1
                    children[parent_node].append(node)
                    words[node] = row
                    parent[node] = parent_node
                    args = (data[labels == i, :], f_cluster, f_cluster_2, branching_factor, node)
                    _words, _children, _parent, _node = FixedVocabularyTree._train_vocabulary_tree(*args)
                    for key in _words.keys():
                        words[key] = _words[key]
                        parent[key] = _parent[key]
                    for key in _children.keys():
                        children[key] = _children[key]
                    node = _node
            else:
                pass
        else:
            for i, row in enumerate(data):
                node += 1
                words[node] = row
                parent[node] = parent_node
                children[parent_node].append(node)
                children[node] = []
        return words, children, parent, node