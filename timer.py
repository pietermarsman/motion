# coding=utf-8
"""
Singleton class with static
"""
import time

__author__ = 'Pieter Marsman'

previous = 0.0
filtered_t = dict()


def tick(key):
    """
    Get the time since the last time this was called

    :param key: Filter the time for this event
    :type key: str
    :return: The time since last time this was called. Does not matter if the key is different.
    :rtype: float
    """
    global previous, filtered_t
    if key not in filtered_t:
        previous = time.time()
        filtered_t[key] = 0.0
        return 1.0
    else:
        diff = time.time() - previous
        filtered_t[key] = filtered_t[key] * 0.9 + 0.1 * diff
        previous = time.time()
        return filtered_t[key]